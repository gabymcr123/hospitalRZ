<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;?>
<div class="page-header" style="padding-top: 0px;margin-top: 0px;">
    <h2 style="margin-top: 0px;padding-top: 0px;" >Hospital Rafael Rodriguez Zambrano <small>Módulo de Agenda</small></h2>
    <?php if(!Yii::app()->user->isGuest) {
        $model = Usuario::model()->find('idusuario ='.Yii::app()->user->Id); 
        print 'Bienvenido(a): <strong>'.$model->nombre.' '.$model->apellido.'</strong>';
    }?>
</div>

<!--<div class="pull-left">  </div>-->

<?php
    $this->widget(
    'booster.widgets.TbCarousel',
    array(
    'items' => array(
    array(
    'image' => 'images/img_hrz7.jpg',
    ),
    array(
    'image' => 'images/img_hrz2.jpg',
    'label' => '',
    'caption' => ''
    ),
    array(
    'image' => 'images/img_hrz3.jpg',
    ),
    array(
    'image' => 'images/img_hrz4.jpg',
    ),
    array(
    'image' => 'images/img_hrz5.jpg',
    ),
    array(
    'image' => 'images/img_hrz6.jpg',
    ),
    array(
    'image' => 'images/img_hrz9.jpg',
    ),
    array(
    'image' => 'images/img_hrz8.jpg',
    ),
    array(
    'image' => 'images/img_hrz1.jpg',
    ),
    ),
'htmlOptions' => array('class' => '','style'=>'width:1080px'),
    )
    );
?>
