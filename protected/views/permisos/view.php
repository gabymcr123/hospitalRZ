<?php
$this->breadcrumbs=array(
	'Permisos'=>array('admin'),
	$model->idpermisos,
);

$this->menu=array(
array('label'=>'Lista de Permisos','url'=>array('index')),
array('label'=>'Actualizar Permiso','url'=>array('update','id'=>$model->idpermisos)),
array('label'=>'Administrar Permisos','url'=>array('admin')),
);
?>
<BR>
<h4>Detalles de permiso a usuario agendadores</h4>
<BR>
<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
                array('name'=>'Tipo de Agenda',
                    'value'=>$model->idtipoagenda1->tipoagenda,
                    'filter'=>CHtml::activeTextField($model, 'tipoagenda',array('class'=>'form-control')),),
                array('name'=>'Profesional de la Salud',
                    'value'=>$model->idmedico1->nombreCompleto,
                    'filter'=>CHtml::activeTextField($model, 'medico',array('class'=>'form-control')),),
                array(
                    'name'=>'autorizacion',
                    'header'=>'Autorización',
                    'filter'=>array('1'=>'Si','0'=>'No'),
                    'value'=>($model->autorizacion=="1")?("Si"):("No")),
),
)); ?>
