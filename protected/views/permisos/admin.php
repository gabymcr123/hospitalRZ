<?php
$this->breadcrumbs=array(
	'Permisos'=>array('admin'),
	'Administrar',
);

$this->menu=array(
array('label'=>'Lista de Permisos','url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('permisos-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h3>Administrar Permisos</h3>


<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'permisos-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
                array(
                    'name'=>'idtipoagenda',
                    //'header'=>'Tipo de Agenda',
                    'sortable'=>false,
                    'value'=>'$data->idtipoagenda1->tipoagenda',
                    'filter'=>CHtml::listData(Tipoagenda::model()->findAll('Estado=true'),'idtipoagenda','tipoagenda'),
                    //'filter'=>CHtml::activeTextField($model, 'tipoagenda',array('class'=>'form-control'))
                    ),
                array('name'=>'Profesional de la Salud',
                    'value'=>'$data->idmedico1->nombreCompleto',
                    'sortable'=>false,
                    //'filter'=>CHtml::listData(Medico::model()->findAll('estado=true'),'idmedico','nombres')),
                    'filter'=>CHtml::activeTextField($model, 'medico',array('class'=>'form-control')),),
                array(
                    'name'=>'autorizacion',
                    'header'=>'Autorización',
                    'filter'=>array('1'=>'Si','0'=>'No'),
                    'sortable'=>false,
                    'value'=>'($data->autorizacion=="1")?("Si"):("No")'),
		/*'creadopor',
		'modificadopor',
		
		'fechacreado',
		'fechamodificado',
		*/
     array(
        'class' => 'booster.widgets.TbToggleColumn',
        'toggleAction' => 'permisos/delete',
        'name' => 'autorizacion',
        'uncheckedButtonLabel'=>'Autorizar',
        'checkedButtonLabel'=>'Denegar',
        'header' => 'Autorizar/Denegar',
         'sortable'=>false,
        //'value'=>'$data->estado ? 1 : 0',
        'filter'=>'',
    ),
),
)); 
?>
<?php echo CHtml::link('Regresar',array('admin')); ?>