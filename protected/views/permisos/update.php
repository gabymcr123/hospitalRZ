<?php
$this->breadcrumbs=array(
	'Permisos'=>array('admin'),
	$model->idpermisos=>array('view','id'=>$model->idpermisos),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Permisos','url'=>array('index')),
	array('label'=>'Ver Permiso','url'=>array('view','id'=>$model->idpermisos)),
	array('label'=>'Administrar Permisos','url'=>array('admin')),
	);
	?>
<BR>
	<h3>MODIFICAR LOS PERMISOS DE <?php echo $model->idmedico1->nombreCompleto; ?> PARA <?php echo $model->idtipoagenda1->tipoagenda; ?></h3>
        <BR>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>