<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'permisos-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Campos con <span class="required">*</span> Son requeridos</p>

<?php echo $form->errorSummary($model); ?>

        <b><?php echo CHtml::encode($model->getAttributeLabel('idtipoagenda')); ?>:</b><br>
        <?php 
        $datos2 = CHtml::listData(Tipoagenda::model()->findAll(''),'idtipoagenda','tipoagenda');
        echo $form->dropDownList($model, 'idtipoagenda',$datos2,array('class'=>'form-control',"disabled"=>"disabled"));
       ?> <br>
       
        <b><?php echo CHtml::encode($model->getAttributeLabel('idmedico')); ?>:</b><br>
        <?php 
        $datos2 = CHtml::listData(Medico::model()->findAll(''),'idmedico','nombreCompleto');
        echo $form->dropDownList($model, 'idmedico',$datos2,array('class'=>'form-control',"disabled"=>"disabled"));?> <br>

        <b><?php echo CHtml::encode($model->getAttributeLabel('autorizacion')); ?>:</b><br>
	<?php echo $form->checkBox($model,'autorizacion'); ?>
        <br><br>

	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
