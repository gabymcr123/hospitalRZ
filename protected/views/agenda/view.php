<?php
/* @var $this AgendaController */
/* @var $model Agenda */

//$this->breadcrumbs=array(
//	'Citas'=>array('admin'),
//	//$model->idagenda,
//);
$user = Usuario::model()->find('idusuario='.Yii::app()->user->Id);

$this->menu=array(
        array('label'=>$user->idrol!=4 ? 'Crear Nueva Cita':'Horario Medico', 'url'=>array('create')),
	array('label'=>'Lista de Citas', 'url'=>array('admin'), 'visible'=>$user->idrol!=2),
	array('label'=>'Actualizar Cita', 'url'=>array('update', 'id'=>$model->idagenda),'visible'=>$user->idrol!=4 and $user->idrol!=2),
	array('label'=>'Eliminar Cita', 'url'=>'#', 'visible'=>$user->idrol!=4,'linkOptions'=>array('submit'=>array('delete','id'=>$model->idagenda),'confirm'=>'¿ Desea elimnar este registro ?')),
	//array('label'=>'Administrar citas', 'url'=>array('admin')),
);
?>

<h2>Cita</h2>
<div style="text-align: right;">
    
<?php 
$this->widget(
'booster.widgets.TbButton',
array(
'buttonType'=>'link',
'label' => 'Agenda al mismo medico',
'visible'=>$user->idrol!=2 and $user->idrol !=4 ? true: false,
'url'=>'index.php?r=agenda/create&mes='.date('m',  strtotime($model->fecha)).'&anio='.date('Y',  strtotime($model->fecha)).'&doctor='.$model->idmedico.'&especialidad='.$model->pkmedico->idespecialidad
)
); echo ' ';
$this->widget(
'booster.widgets.TbButton',
array(
'buttonType'=>'link',
'label' => 'Agendar en el mismo mes',
'visible'=>true,
'url'=>'index.php?r=agenda/create&mes='.date('m',  strtotime($model->fecha)).'&anio='.date('Y',  strtotime($model->fecha))
)
);
if($model->fecha =='2015-02-16'){
    $fech='2015-02-21';
}else if($model->fecha =='2015-02-17'){
    $fech='2015-02-28';
}else{
    $fech=$model->fecha;
}
$arrfecha= explode('-',$fech);
?>
    </div>
<?php 
$diasdesemanas = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'); 
$mesanio = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'); 
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'idagenda',
                array(
                      'name'=>'idtipoagenda',
                      'value'=>$model->pktipoagenda->tipoagenda),
		//array(
                      //'name'=>'idpaciente',
                      //'value'=>$model->pkpaciente->nombre.' '.$model->pkpaciente->apellido ),
		//'idmedico',
                array(
                      'name'=>'idmedico',
                      'value'=>$model->pkmedico->nombreCompleto ),
		//'idsubcentro',
            array(
                'name'=>'Historial Cl�nico',
                'value'=>$model->pkpaciente->historialclinico
                ),
		'nombre',
                array('name'=>'fecha',
                    'value'=>$diasdesemanas[date('N', strtotime($fech))].', '.$arrfecha[2].'-'.$mesanio[(int)$arrfecha[1]].'-'.$arrfecha[0],
                    ),
		'numeroturno',
                'horainicio',
		//'horafin',
		//'estado',
            array(
                'name'=>'idsubcentro',
                'value'=>$model->pksubcentro->subcentro
            ),
                'telefono',
                'celular',
		/*'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',*/
	),
)); ?>
