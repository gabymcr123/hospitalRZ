

<?php
$this->breadcrumbs=array(
	//'Eventos'=>array('index'),
	'Reportes',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<center><h4>Datos de pacientes</h4>
<p> 

<?php //echo CHtml::textField('fecha_inicio', ''); ?>

    <?php
    /*
$this->widget(
        'booster.widgets.TbDatePicker',
        array(
            'name'=> 'fecha_inicial',
            'options' =>  array(
               'language' => 'es',
                'format' => 'dd-mm-yyyy',
        )
        )
        ); 
     *
     */
?>
  
<?php
/*
$this->widget(
        'booster.widgets.TbDatePicker',
        array(
            'name'=> 'fecha_final',
            'options' =>  array(
               'language' => 'es',
                'format' => 'dd-mm-yyyy',
        )
        )
        );
 * 
 */ 
?>

<?php 
if(!isset($model->f3)){
    $model->f3= date_format(date_create(), 'd-m-Y');
}
if(!isset($model->f4)){
    $model->f4= date_format(date_create(), 'd-m-Y');
}
?>
</p>
<?php 
$date = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[f3]',
                                    'value' => $model->f3,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'dd/mm/yy',
                                        'changeMonth' => 'true',
                                        'yearRange'=>'-80:+3',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
   'changeYear'=>'true',
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:35px;width:120px','class'=>'span5 form-control',
                                    ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                                ),true) ;

?>

<?php  
$this->widget('booster.widgets.TbGridView', array(
	'id'=>'agenda-grid',
	'dataProvider'=>$model->busqueda3(),
        'afterAjaxUpdate'=>"function() {
                                           jQuery('#Agenda_f3').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
                                    
   }",
	'filter'=>$model,
        'htmlOptions'=>array('style'=>'word-wrap:break-word;width:950px;'),
	'columns'=>array(
		//'idagenda',
            array(
                    'header' =>'Profesional',
                    'filter'=>'',
                    'value'=>'$data->pkmedico->apellido." ".$data->pkmedico->nombre',
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    ),
                array(
                    'name' =>'Fecha de atención',                           
                    'filter'=> $date,
                    'value'=>'$data->fecha',
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    ),
                //'idmedico',
                
                array(
                    'name'=>'Nombres',
                    'value'=>'$data->nombreCompleto',
                    //'filter'=>CHtml::activeTextField($model, 'doctor',array('class'=>'form-control')),
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                 ),
            
            array(
                    'name'=>'Cedula',
                    'value'=>'$data->pkpaciente->cedula',
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                     ),
		//'idpaciente',
                 
		//'idtipoagenda',
                array(
                    'name'=>'Historial clinico',
                    'value'=>'$data->pkpaciente->historialclinico',
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                     ),
            
                
            
              
            
              
              array(
                    'name'=>'Centro de Salud',
                    'value'=>'$data->pksubcentro->subcentro',
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                     ),
            array(
                    'name'=>'Agendado por',
                    'value'=>'$data->creadopor',
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                     ),
               
		//'idsubcentro',
		//'nombre',
		
		//'numeroturno',
           //array
                  //ame'=>'fecha1', 
                  //alue'=>'$data->fecha'
            //),
            /*
		'fecha',
		'horainicio',
		'horafin',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
		
	),
)); ?>
<br>
<br>
<label class="checkbox-inline"><input id="che" type="checkbox" value="">Agregar celular/télefono al reporte</label>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link('General Pdf',array("generarpdfPacientesAgendados"), array('id'=>'btnpdf')); ?>
</div>
<br>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link('Genera Excel',array("generarExcelPacientesAgendados")); ?>
</div>

<script type="text/javascript">
    var val;
    if($("#che").is(':checked')) {  
              
        } else {  
            
        }  
 $("#btnpdf").click(function (){
if($("#che").is(':checked')) {  
        val = 1;
        } else {  
             val = 0;
        }

     var enlace= 'index.php?r=/agenda/generarpdfPacientesAgendados&val='+val;
var link2 = document.getElementById('btnpdf');
$(link2).attr('href',enlace);
$(link2).attr('target','_blank');


});
</script>