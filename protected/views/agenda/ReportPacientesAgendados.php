<?php $contador=count($model); if ($model !== null):?>

<?php 
$t=$val;
if($t){
    $td1 = '<td width="16.666666666667%">Teléfono</td>';
    $td2='<td width="16.666666666667%">Celular</td>';
    $num=7;
}else{
    $td1 ='';
    $td2 ='';
    $num = 5;
}
$mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
         $mPDF1->useOnlyCoreFonts = true;
         $mPDF1->SetTitle("Reporte");
         $mPDF1->SetAuthor("Hospital");


$html = '<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 10pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
 border-left: 0.1mm solid #000000;
 border-right: 0.1mm solid #000000;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items td.blanktotal {
 background-color: #FFFFFF;
 border: 0mm none #000000;
 border-top: 0.1mm solid #000000;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
 td{
 font-size: 10px;
 }
</style>
</head>
<body>';
$control = 0;
foreach ($model as $row){

    if($control != $row->idmedico){
        if($control != 0){
        $html = $html . '<tr>
        <td class="blanktotal" colspan="'.$num.'" rowspan="6"></td>
        </tr>
        </tbody>
        </table>';
        $mPDF1->WriteHTML($html); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
        $mPDF1->AddPage();
        $html="";
        }
        
        $control = $row->idmedico;
        $html = $html .'<!--mpdf
        <htmlpageheader name="myheader">
        <table width="100%"><tr>
        <td width="50%" style="color:#0000BB;"><span style="font-weight: bold; font-size: 14pt;">Hospital General Rodriguez Zambrano De Manta</span><br />Manta-Ecuador<br /><span style="font-size: 15pt;">&#9742;</span> 593(5)2611849 Ext 5056</td>
        <td width="50%" style="text-align: right;"><b>Pacientes Agendados</b></td>
        </tr></table>
        </htmlpageheader>

       <htmlpagefooter name="myfooter">
        <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
        Página {PAGENO} de {nb}
        </div>
        </htmlpagefooter>

       <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
        <sethtmlpagefooter name="myfooter" value="on" />
        mpdf-->'; 
        $html = $html.'<div style="text-align: right"><b>Fecha: </b>'.date("d/m/Y").'</div>
        '.$html = $html.'<div style="text-align: right"><b>Doctor: </b>'.$row->pkmedico->apellido." ".$row->pkmedico->nombre.'</div>     
'.$html = $html.'<div style="text-align: right"><b>Especialidad: </b>'.$row->pkmedico->idespecialidad1->especialidad.'</div>     
        

        <br>
         <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse;" cellpadding="5">
         <thead>
         <tr>
         <td width="16.666666666667%">Fecha</td>
        
         <td width="26.666666666667%">Nombre del paciente</td>
         <td width="16.666666666667%">N° archivo único</td>
         '.$td1." ".$td2.'
         <td width="16.666666666667%">Hora cita</td>
         <td width="6.666666666667%">Numero de turno</td>
         
         
         </tr>
         </thead>
         <tbody>';
    }
    if($t){
    $html = $html . ' <tr>
    <td align="center">
    '.$row->fecha.'
    </td>
   
  
    <td align="center">
    '.$row->nombreCompleto.'
    </td> 
     <td align="center" style="font-size: 13px;">
    '.$row->pkpaciente->historialclinico.'
    </td>
      <td align="center">
    '.$row->pkpaciente->telefono.'
    </td>
     <td align="center">
    '.$row->pkpaciente->celular.'
    </td> 
    <td align="center">
    '.$row->horainicio.'
    </td> 
    <td align="center">
    '.$row->numeroturno.'
    </td> 
    
    </tr>';}else{
        $html = $html . ' <tr>
    <td align="center">
    '.$row->fecha.'
    </td>
   
  
    <td align="center">
    '.$row->nombreCompleto.'
    </td> 
     <td align="center" style="font-size: 13px;">
    '.$row->pkpaciente->historialclinico.'
    </td>
       
    <td align="center">
    '.$row->horainicio.'
    </td> 
    <td align="center">
    '.$row->numeroturno.'
    </td> 
    
    </tr>';
    }
}
    $html = $html . '<tr>
        <td class="blanktotal" colspan="'.$num.'" rowspan="6"></td>
        </tr>
        </tbody>
        </table>';
    $html = $html .'</body></html>';
     $mPDF1->WriteHTML($html);
     
    //$mPDF1->WriteHTML($html);     
         
         //$mPDF1->SetWatermarkText("Pacientes");
         $mPDF1->showWatermarkText = true;
         $mPDF1->watermark_font = 'DejaVuSansCondensed';
         $mPDF1->watermarkTextAlpha = 0.1;
         $mPDF1->SetDisplayMode('fullpage');
         
         $mPDF1->Output('Reporte_Pacientes'.date('YmdHis'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;

?>
<?php endif; ?>