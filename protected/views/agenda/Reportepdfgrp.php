<html>
<?php 
$contmayora15=0;
$totalmenor=0;
$totalmayor = 0; ?>
<head>
    
<style>
 body {font-family: sans-serif;
 font-size: 10pt;
 }
td { vertical-align: top; }
 .items td {
    border: 0.1mm solid #000000;
    text-align: center;
    font-size: 10px;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0.08mm solid gray;
 border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
</head>
<body>
  
<!--mpdf
 <htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="50%" style="color:#0000BB;"><span style="font-weight: bold; font-size: 14pt;">Hospital General Rodriguez Zambrano De Manta</span><br />Manta-Ecuador<br /><span style="font-size: 15pt;">&#9742;</span> 593(5)2611849 Ext 5056</td>
 <td width="50%" style="text-align: right;"><b>Indicadores de GPR</b></td>
 </tr></table>
 </htmlpageheader>
 
<htmlpagefooter name="myfooter">
 <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
 Página {PAGENO} de {nb}
 </div>
 </htmlpagefooter>
 
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
 
<div style="text-align: right"><b>MES: </b><?php echo $mes.'/'.$anio?> </div>
<h4 style="text-align: center">Distribución según los 15 días límites</h4>
<br>
<div style="text-align:center;">
<table id="tabla" class="items table table-bordered" style="margin: 0 auto;">
    <tr>
    <th style="background: Whitesmoke">Etiquetas de fila</th><th style="background: Whitesmoke">Menor 15 días</th><th style="background: Whitesmoke">Mayor a 15 días</th><th style="background: Whitesmoke">Total general</th></tr>
    <?php foreach($sqlespe as $row):
    $row2 = $sqlespe2[$contmayora15];
    $totalmenor = $row['total'] +$totalmenor;
    $totalmayor = $row2['total']+$totalmayor;
            ?>
    <tr>
    <td><?php print $row['especialidad'];?></td>
    <td><?php print $row['total'];?></td>
    <td><?php print $row2['total'];?></td>
    <td><?php print $row['total']+$row2['total'];?></td>
    </tr>
       <?php $contmayora15++; endforeach; ?>
    <tr style="background: Whitesmoke"> <th>Total General</th><td><?php print $totalmenor;?></td><td><?php print $totalmayor;?></td><td><?php print $totalmenor+$totalmayor;?></td></tr>       
  
</table>
    </div>
</body>
 </html>


