<?php
/* @var $this AgendaController */
/* @var $model Agenda */
$ident = Yii::app()->db->createCommand('select idmedico from agendamiento.medico where idusuario ='.Yii::app()->user->id)->queryScalar();
$sql = 'select rolusuario.rol from agendamiento.usuario inner join agendamiento.rolusuario on usuario.idrol = rolusuario.idrol where idusuario ='.Yii::app()->user->id;
$tipo = Yii::app()->db->createCommand($sql)->queryScalar();
/*
$this->breadcrumbs=array(
	'Citas'=>array('admin'),
	'Crear',
);
$this->widget('zii.widgets.CBreadcrumbs', array(
    'links'=>array(
        'Sample post'=>array('post/view', 'id'=>12),
        'Edit',
    ),
));*/
$this->menu=array(
	//array('label'=>'Lista de Citas', 'url'=>array('admin'),'ident'=>$ident),
	//array('label'=>'Lista de Citas', 'url'=>array('agenda2/admin'),'visble'=>Yii::app()->user->Tipo=='MEDICO',),
        array('label'=> 'Agenda del día', 'url'=>array('agenda2/admin'),'visible'=>$tipo =="MEDICO"),
);
?>

<h4 style="margin-top: 0px;">CREAR NUEVA CITA</h4>
<?php

if($tipo =="MEDICO"){
    $this->renderPartial('_form', array('model'=>$model,'ident'=>$ident,));
}
else
    $this->renderPartial('_form', array('model'=>$model)); ?>