<?php

$this->menu=array(
	array('label'=>'Lista de Citas', 'url'=>array('admin')),
	array('label'=>'Crear Cita', 'url'=>array('create')),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
    'type'=>'horizontal',
	'id'=>'LISTAREAGENDAR2-form',
	'enableAjaxValidation'=>false,
)); ?>
<h3>Lista de Pacientes Reagendados</h3>
<br>
<?php $model=  Agenda::model(); ?>
<?php $datos = CHtml::listData(Especialidadmedica::model()->findAll('estado = true order by especialidad'),'idespecialidad','especialidad'); ?>
<div class="form-group">

    <label class="col-sm-3 control-label required" for="Agenda_idmedico">Especialidad</label>
    <div class="col-sm-5 col-sm-9">
        <?php echo CHtml::dropDownList('especialidad',0,$datos, array('empty'=>'-Seleccione Especialidad-','class'=>'span5 form-control')); ?>
    </div>

</div>
<?php $datos3 = CHtml::listData(Medico::model()->findAll(),'idmedico','nombreCompleto'); 
echo $form->dropDownListGroup($model, 'idmedico',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $datos3))); ?>

    <div class="form-group">
    <div class="col-sm-5">
        <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        <span class="input-group-addon"><?php $this->widget('booster.widgets.TbDatePicker', array('model' => $model,'attribute' =>'fecha' ,'options' => array('format'=>'yyyy/mm/dd','language' => 'es'),'htmlOptions' => array('class'=>'col-sm-4','placeholder'=>'Fecha Inicio','id'=>'combo1'),)); ?></span>
        <span class="input-group-addon"><?php $this->widget('booster.widgets.TbDatePicker', array('model' => $model,'attribute' =>'fecha' ,'options' => array('format'=>'yyyy/mm/dd','language' => 'es'),'htmlOptions' => array('class'=>'col-sm-4','placeholder'=>'Fecha Fin','id'=>'combo2'),)); ?></span>
        </div>
    </div>
</div>

<?php echo CHtml::ajaxLink(
    'Ver Pacientes Reagendados',          // the link body (it will NOT be HTML-encoded.)
    array('agenda/listareagendados'), // the URL for the AJAX request. If empty, it is assumed to be the current URL.
    array(
        'update'=>'#pacientes',
        'data'=> 'js:{"fechainicio":getfechainicio(),"fechafinal":getfechafinal(),"medico":getmedico()}',
    ),array('class'=>'btn btn-default')
); ?>

<hr>
<div id="pacientes">
</div>
<hr>
<?php $this->endWidget(); ?>
<script type="text/javascript" >
    function getfechainicio(){
        return  $("#combo1").val();
    }
     function getfechafinal(){
        return  $("#combo2").val();
    }
    function getmedico(){
        return  $("#Agenda_idmedico").val();
    }
    
    $('#especialidad').change(function (){
	var idespecialidad = this.value; // el "value" de ese <option> seleccionado
	if(idespecialidad == 0) {
            return;
	}
	var action = 'index.php?r=/agenda/obtenermedico&idespecialidad='+idespecialidad;
	$.getJSON(action, function(listaJson) {
	$('#Agenda_idmedico').find('option').each(function(){ $(this).remove(); });
	$.each(listaJson, function(key, medico) {
	$('#Agenda_idmedico').append("<option value='"+medico.idmedico+"'>"+medico.nombre+' '+medico.apellido+"</option>");
	});
	})
    });
    
     $('#combo2').change(function()
{
	var fech1 = $('#combo1').val();
	var fech2 = $('#combo2').val();
	if((Date.parse(fech1)) > (Date.parse(fech2)))
	{
		alert('La fecha inicial no puede ser mayor que la fecha final');
		$('#combo2').val("");
	}
});



 $('#combo2').click(function()
{
    var fecha = $('#combo1').val();
    if(fecha.length < 1)
    {
        alert('Ingrese primero la Fecha Inicio');
        $('#combo2').val("");
    }
});

</script>