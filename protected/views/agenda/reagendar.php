<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'evento-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php
/* @var $this AgendaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Agendas',
);

$this->menu=array(
	array('label'=>'Crear Agenda', 'url'=>array('create')),
	
);

?>
<div id="datos1">
<h2>Reagendamiento de pacientes</h2>
<br>
<center>
<table style="width:60%" >
	<tr>
		<td style="text-align:center;">
	<?php echo CHtml::label('Médico','idmedico');?>			
		</td>
		<td style="text-align:center;">
	<?php echo CHtml::label('Fecha','fecha');?>
		</td>
	</tr>
	<tr>
	<td>
	<?php echo CHtml::hiddenField('idmedico','',array('size'=>10,'maxlength'=>8,'class'=>'form-control'));
	 $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'name'=>'idmedi',
            'source'=>$this->createUrl('Agenda/autocompletar2'),
           'options'=>array
             (
                'showAnim'=>'fold',
                           'select'=>"js:function(event, ui) 
                    {
                    }",
            ),
            'htmlOptions'=>array
            (
     		'style'=>'width:350px;',
     		'placeholder'=>'Buscar Médico..',
     		'title'=>'Indique el Médico',
                'class'=>'form-control'
            ),
            ));
  	?>

        
        
        </td>
	<td>
	  <?php 
	      $this->widget(
		    'booster.widgets.TbDatePicker',
		    array(
                                            'id'=>'date',
		    'name' => 'date',
		    'options' => array(
		    'language' => 'es',
		    'format' => 'dd-mm-yyyy',
                        
		    )
		    )
		    );
	  ?>
	</td>
        
        <td>
        <CENTER><div class="enlaceboton" style="width: 75px">
            <?php echo CHtml::ajaxLink(
    'Enviar',          // the link body (it will NOT be HTML-encoded.)
    array('agenda/obtenerhora'), // the URL for the AJAX request. If empty, it is assumed to be the current URL.
    array(
        'update'=>'#pacientes',
        'data'=> 'js:{"medico":getmedico(), "fecha":getfecha(),}',
        
        
    )
); ?></div></center></td>
	</tr>

</table>
</center>


<hr>
<div id="pacientes">
</div>
<hr>
<legend>Reagendamiento</legend>

    <b>Opciones</b>
    <br>
  
    <br> 

 

    <input value="1" checked = "checked" type="radio" name="fecha" id= "fechaactual"  onclick="toggle(this)">
<label for = "fechaactual"> A partir de fecha actual</label>
<input value = "2"type="radio" name="fecha"  id= "fechaotra"  onclick="toggle(this)">
 <label for = "fechaotra">  Otra Fecha</label>
          <br>
          
    
      <input type = "radio"
                 name = "radSize"
                 id = "mismodoctor"
                 value = "1"
                 onclick="toggle2(this)"
                 checked = "checked" />
          <label for = "mismodoctor">Mismo Profesional de la Salud</label>
          
          <input type = "radio"
                 name = "radSize"
                 id = "otrodoctor"
                 onclick="toggle2(this)"
                 value = "2" />
          <label for = "otrodoctor">Otro Profesional de la Salud</label>
          <br> <br> <br>
   <center>
       <div id="esconder" style="display:none;">
Fecha a reagendar:
<?php 
      $this->widget(
		    'booster.widgets.TbDatePicker',
		    array(
                                            'id'=>'dateR',
		    'name' => 'dateR',
		    'options' => array(
		    'language' => 'es',
		    'format' => 'dd-mm-yyyy',
                        
		    )
		    )
		    );
	  ?>
</div>
       <div id="esconder2" style="display:none;">
Otro doctor
<?php echo CHtml::hiddenField('idmedico','',array('size'=>10,'maxlength'=>8,'class'=>'form-control'));
	 $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'name'=>'idmedi2',
            'source'=>$this->createUrl('Agenda/autocompletar2'),
           'options'=>array
             (
                'showAnim'=>'fold',
                           'select'=>"js:function(event, ui) 
                    {
                    }",
            ),
            'htmlOptions'=>array
            (
     		'style'=>'width:350px;',
     		'placeholder'=>'Buscar Médico..',
     		'title'=>'Indique el Médico',
                'class'=>'form-control'
            ),
            ));
  	?>
</div>
<br>
<CENTER><div class="enlaceboton" style="width: 230px">
<?php echo CHtml::ajaxLink(
    'Reagendar los pacientes',          // the link body (it will NOT be HTML-encoded.)
    array('agenda/enviar'), // the URL for the AJAX request. If empty, it is assumed to be the current URL.
    array(
        'update'=>'#pacientes',
        'data'=> 'js:{"medico":getmedico(),"medico2":getmedico2(), "fecha":getfecha(),"fecha2":getfecha2(),"valor1":getValor(),"valor2":getValor2(),"datos":enviar}',
        "success" => "function(result){
                                        alert(result);
                                    }",
        
    )
); ?>
        </div>
</center>
<?php $this->endWidget(); ?>
</div>
<script type="text/javascript" >
   
   var valor = 0;
   var valor2 = 0;
    function getmedico(){
        return  $("#idmedi").val();
    }
     function getmedico2(){
        return  $("#idmedi2").val();
    }
    function getfecha(){
        return  $("#date").val();
    }
    
    function getfecha2(){
        return  $("#dateR").val();
    }
    
    function getValor(){
        
        return valor;
    }
    function getValor2(){
  
        return valor2;
        
    }
    
    function toggle(elemento) {
        if(elemento.value=="2") {
              document.getElementById("esconder").style.display = "block";
              valor = 1;
             
           }else{
                document.getElementById("esconder").style.display = "none";
              valor = 0;
           }
    }
    
    function toggle2(elemento) {
        if(elemento.value=="2") {
              
              document.getElementById("esconder2").style.display = "block";
              valor2 = 1;
           }else{
           
              document.getElementById("esconder2").style.display = "none";
              valor2 = 0;
           }
    }
    
</script>
<script languaje="javascript">
    var enviar = Array();
    function agregar(obj){
        enviar.push(obj);
    }
    function borrar(obj){
        var a = enviar.indexOf(obj);
        enviar.splice(a);
    }
    function verdadero(){
        $(".chbutton").attr('checked', 'true');
        $(".chbutton").each(function(){
           agregar($(this).val());
        });
    }
    function falso(){
        $(".chbutton").removeAttr('checked');
         $(".chbutton").each(function(){
           borrar($(this).val());
        });
    }
</script>  

<script languaje="javascript">
$('#yt1').click(function() {    
setInterval("location.reload()",1000);
});
</script>



