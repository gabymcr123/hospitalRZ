    <div class="modal-body">
        <label id="fechaetiqueta" style="float:right;"></label> 
            
    <?php 
    //FORMULARIO
    $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'agenda-form',
        'htmlOptions'=>array('name'=>'forma',),
	'type' => 'horizontal',
	'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
	)); ?>
	<p class="help-block">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>
        
		<?php //$tiposAgenda = array('1'=>'Referencia','3'=>'Extra Referencia','Reagendar'=>'Referencia');
                $tiposAgenda = CHtml::listData(Tipoagenda::model()->findAll('estado = true and idtipoagenda='.$model->idtipoagenda.' order by idtipoagenda '),'idtipoagenda','tipoagenda');
		echo $form->dropDownListGroup($model,'idtipoagenda',array('wrapperHtmlOptions' => array('class' => 'col-sm-5','disabled'=>'disabled'),'widgetOptions' => array('data' => $tiposAgenda ,))); ?>
                <?php echo $form->hiddenField($model,'idpaciente','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
                <?php echo $form->hiddenField($model,'numeroturno','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
                <?php echo $form->textFieldGroup($model,'nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50,"autocomplete"=>"off")))); ?>
                
    <?php echo $form->textFieldGroup($model,'telefono',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10,"autocomplete"=>"off")))); ?>		            
    <?php echo $form->textFieldGroup($model,'celular',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10,"autocomplete"=>"off")))); ?>		            
    <div class="form-group">
       
                <?php //echo $form->labelEx($model,'idsubcentro',array('class'=>'col-sm-3 control-label required')); ?> 
               <label class="col-sm-3 control-label required" for="Agenda_idsubcentro">Centro de Salud de procedencia<span class="required">*</span></label>
                <div class="col-sm-9">
                    <span style="font-size: 11px;"><strong>CIUDAD: </strong></span> <strong><span style="font-size: 11px;" id="ciudad"></span></strong>
                    <div class="input-group">
                <?php echo $form->hiddenField($model,'idsubcentro','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>25)))); 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'subcentro',
                'model'=>$model,
                'source'=>$this->createUrl('Agenda/autocompletarcentro'),
                'options'=>array
                (
                    'showAnim'=>'fold',
                    'select'=>"js:function(event, ui)
                    {
                        $('#".CHtml::activeId($model,'idsubcentro')."').val(ui.item.id); 
                        $('#ciudad').html(ui.item.canton);
                    }",
                ),
                'htmlOptions'=>array
                (
                    'size'=>35,
                    'maxlength'=>50,
                    'placeholder'=>'Actualizar centro de salud',
                    'title'=>'Indique el Cetro de Salud',
                    'style'=>'position:initial;',
                    'class'=>'span5 form-control',
                    'onkeyup'=>'this.value=this.value.toUpperCase()',
                    //'value'=>$model->pksubcentro->subcentro,
                ),
                ));
  	?>      <span class="input-group-addon"><a href="#" onclick ="borrarsubcentro()" class="glyphicon glyphicon-trash" title='Borrar Centro de Salud'></a></span>
                </div>
                <?php print '<strong>Centro de salud actual: </strong>'.$model->pksubcentro->subcentro;?>
                </div>
                 </div>
    <?php echo $form->hiddenField($model,'idmedico','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
    <div class="form-group">

    <label class="col-sm-3 control-label" for="Agenda_telefono">Profesional de la Salud</label>
    <div class="col-sm-9">
        <input id="subcentro" class="span5 form-control" type="text" value="<?php print $model->pkmedico->nombre.' '.$model->pkmedico->apellido;?>"  placeholder="Teléfono" autocomplete="off" maxlength="10" disabled="disabled">
    </div>
    </div>
                 <?php echo $form->hiddenField($model,'fecha','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control')); ?>
                 <?php //echo $form->textFieldGroup($model,'fecha',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','onkeypress'=>'return 0;')))); ?>		
     <div class="btn-group" data-toggle="buttons" id='horario'>
    </div>
    <div class="modal-footer">

			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'button',
					'context'=>'primary',
                                'htmlOptions' => array(
                                        'onclick'=>'forma.submit();',
                                ),
					'label'=>$model->isNewRecord ? 'Ingresa' : 'Guardar',
				)); ?>
		<?php $this->endWidget(); 
		//FIN DE FORMULARIO	?>
	</div>
    </div>