<?php

/* @var $this AgendaController */
/* @var $model Agenda */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerCoreScript('jquery');
if (isset($_GET["mes"])) $mes = $_GET["mes"]; else $mes = '';
if (isset($_GET["anio"])) $anio = $_GET["anio"]; else $anio = '';
$medico = Medico::model()->find('idmedico = '.$ident);
?>


<link rel="stylesheet" type="text/css" media="all" href="css/calendario.css" />
        <div class="doctores">
        	<h2><small><?php print $medico->titulo.' '.$medico->nombre.' '.$medico->apellido; ?></small></h2>
                <label>Identificación: </label><?php echo $medico->cedula; ?> <br>
                <label>Especialidad: </label><?php echo $medico->idespecialidad1->especialidad; ?> <br>
                <label>Teléfono: </label><?php echo $medico->telefono; ?> <br>
                <label>Tiempo por Consulta: </label><?php echo $medico->intervalo; ?> minutos<br>
        	<div id="cssmenu"></div>
        </div>
    	<div class="calendario_ajax">
            <div class="cal"></div><div id="mask"></div>
                    
        </div>
        


    <script type="text/javascript">
       //var envio = 'ajax_calendario.php';
       var envio = 'index.php?r=agenda/calendario';
       var valdoctor = "";
       var agenda=$(".cal");
       var nomdoctor = '<?php echo $ident?>'
       var nomespe = '';
       var ID ="";
    function generar_calendario(mes,anio)
	{
		var agenda=$(".cal");
		agenda.html("<img src='images/loading.gif'>");
		$.ajax({
			type: "GET",
			url: envio,
			cache: false,
			data: { mes:mes,anio:anio,accion:"generar_calendario",especialidad:nomespe,doctor:nomdoctor}
		}).done(function( respuesta )
		{
			agenda.html(respuesta);
		});
	}
        function verificarEnvio(){
        var r = true;
        if($('#nombre').val()=='' || $('#Agenda_nombre').val() == '' || $('#subcentro').val() == ''){
            r = false;
            $('#mensaje').attr('class','alert in fade alert-warning');
            $('#mensaje').html('<strong>Datos incompletos.</strong>');
            $('#mensaje').show('show');
            setTimeout(function(){ $('#mensaje').hide('show'); }, 1800);
        }
            
        return r;
        }
        function borrarnombre() {
            $('#nombre').val('');
            $('#Agenda_nombre').val('');
            $('#Agenda_telefono').val('');
            $('#nombre').focus();
        }
        function borrarsubcentro(){
            $('#subcentro').val('');
            $('#Agenda_idsubcentro').val('');
            $('#subcentro').focus();
        }
        function verificarReagenda(){
        var ident = $('#Agenda_idtipoagenda').val();
        if(($('#Agenda_idpaciente').val()!='' && $('#Agenda_idpaciente').val() != null) && (ident == 5 || ident == 6))
        $.ajax({
            url:'index.php?r=/agenda/disponibilidad',
            type:'GET',
            data: {'id':$('#Agenda_idtipoagenda').val(),'fecha':$('#Agenda_fecha').val(),'paciente':$('#Agenda_idpaciente').val(),'especialidad':nomespe},
            success: function (resp) {
                if(resp=='<strong>Atención: </strong>Paciente no tiene cita a reagendar'){
                    $('#mensaje').attr('class','alert in fade alert-warning');
                    $('#yw0').attr('disabled','disabled');
                }else{
                    $('#mensaje').attr('class','alert in fade alert-success');
                    $('#yw0').removeAttr("disabled")
                }
                $('#mensaje').show('show');
                $('#mensaje').html(resp);
            },
        });
    else{
    $('#mensaje').hide('show');
    $('#yw0').removeAttr('disabled');
    }
}

	function formatDate (input) {
		var datePart = input.match(/\d+/g),
		year = datePart[0].substring(2),
		month = datePart[1], day = datePart[2];
		return day+'-'+month+'-'+year;
	}
/*__________________________________________________________________________*/
	//COMENZAR CALENDARIO
		$(document).ready(function()
		{
			/* GENERAMOS CALENDARIO CON FECHA DE HOY */
			generar_calendario("<?php echo $mes ?>","<?php echo $anio ?>");

/*__________________________________________________________________________*/
			/* AGREGAR UN EVENTO */
			$(document).on("click",'a.add',function(e)
			{
                            $('#Agenda_celular').val('');
                            $('#nombre').val('');
                            $('#Agenda_nombre').val('');
                            $('#Agenda_telefono').val('');
                            $('#Agenda_idsubcentro').val('');
                            $('#Agenda_idpaciente').val('');
                            $('#Agenda_celular').val('');
                            $('#subcentro').val('');
                            $('#mensaje').hide();
				e.preventDefault();
				var id = $(this).data('evento');
				var fecha = $(this).attr('rel');
				$('#Agenda_fecha').val(fecha);
                                $('#fechaetiqueta').html(fecha);
				ID = nomdoctor;
				tipo = 'd';
				var par = {'id':ID,'tipo':tipo,'fecha':fecha};
				  $.ajax({
				    url:'index.php?r=/agenda/valoresagenda',
				    type:'GET',
                                    data: par,
				    success: function (response) {
				    	var mat = new Array();
				    	mat = response.split('_');
				    	ID = mat[0];
					$('#Agenda_idmedico').val(ID);
					$('#ini').html(mat[2]);   	
					$('#fin').html(mat[3]);
                                        $('#Agenda_numeroturno').val(parseInt(mat[2])+1);
                                        par = {'idmedico':ID,'fecha':fecha};
                                        $.ajax({
                                              url:'index.php?r=/agenda/obtenerhorario',
                                              type:'GET',
                                              data: par,
                                              success: function (resp) {
                                                  $('#horario').html(resp);
                                        },
                                        });
			      },
			      });
				
			});
/*__________________________________________________________________________*/

/* EVENTO ONCHANGE PARA TIPO DE CONSULTA */
$('#Agenda_idtipoagenda').change(function(){
    var par = {'id':this.value,'idme':ID,'fecha':$('#Agenda_fecha').val(),'pasar':'si'};
    $.ajax({
    url:'index.php?r=/agenda/tipoagenda',
    type:'GET',
    data: par,
    success: function (response) {
   	var mat = new Array();
    	mat = response.split('_');
    	$('#ini').html(mat[0]);
        $('#fin').html(mat[1]);
        $('#Agenda_numeroturno').val(parseInt(mat[0])+1);
      },
     });
});

/*_______________________________________________________________________*/
			/* LISTAR EVENTOS DEL DIA */
			$(document).on("click",'a.modal',function(e)
			{
				e.preventDefault();
				var fecha = $(this).attr('rel');

				$('#mask').fadeIn(1000).html("<div id='nuevo_evento' class='window' rel='"+fecha+"'>Eventos del "+formatDate(fecha)+"</h2><a href='#' class='close' rel='"+fecha+"'>&nbsp;</a><div id='respuesta'></div><div id='respuesta_form'></div></div>");
				$.ajax({
					type: "GET",
					url: envio,
					cache: false,
					data: { fecha:fecha,accion:"listar_evento" }
				}).done(function( respuesta )
				{
					$("#respuesta_form").html(respuesta);
				});

			});

			$(document).on("click",'.close',function (e)
			{
				e.preventDefault();
				$('#mask').fadeOut();
				setTimeout(function()
				{
					var fecha=$(".window").attr("rel");
					var fechacal=fecha.split("-");
					generar_calendario(fechacal[1],fechacal[0]);
				}, 500);
			});

			
			$(document).on("click",".anterior,.siguiente",function(e)
			{
				e.preventDefault();
				var datos=$(this).attr("rel");
				var nueva_fecha=datos.split("-");
				generar_calendario(nueva_fecha[1],nueva_fecha[0]);
			});

		});
// creamos un evento onchange para cuando el usuario cambie su seleccion
// importante: #combo1 hace referencia al ID indicado arriba con: array('id'=>'combo1')
$('#servicio').change(function(){
var opcionSeleccionada = $(this); // el <option> seleccionado
var opcion = opcionSeleccionada.val(); // el "value" de ese <option> seleccionado
if(opcion == 0) {
//$('#siguiente').hide('slow');
return;
}
var action = 'index.php?r=/medico/obtenerespecialidad&opcion='+opcion;
// se pide al action la lista de productos de la categoria seleccionada
//
$('#reportarerror').html("");
$.getJSON(action, function(listaJson) {
//
// el action devuelve los productos en su forma JSON, el iterador "$.each" los separará.
//
// limpiar el combo productos
$('#especialidad').find('option').each(function(){ $(this).remove(); });
$('#especialidad').append("<option value='"+'0'+"'>"
+'-Seleccione Especialidad-'+"</option>");

$.each(listaJson, function(key, valor) {
//
// "producto" es un objeto JSON que representa al modelo Producto
// por tanto una llamada a: alert(producto.nombre) dira: "camisas"
$('#especialidad').append("<option value='"+valor.idespecialidad+"'>"
+valor.especialidad+"</option>");
});

}).error(function(e){ $('#reportarerror').html(e.responseText); });
});

$('#especialidad').change(function(){
    var opcionSeleccionada = $(this); // el <option> seleccionado
var opcion = opcionSeleccionada.val(); // el "value" de ese <option> seleccionado
if(opcion == 0) {
    return;
}
var action = 'index.php?r=/agenda/obtenermedicos&opcion='+opcion;
$.getJSON(action, function(listaJson) {
$('#cssmenu').html("");
var bandera=false;
$.each(listaJson, function(key, valor) {
 if(key==false){
     $('#cssmenu').append('<option class="activo" onclick="calendarespe(this)" value="'+nomespe+'">Todos los Medicos</option>');
 }
$('#cssmenu').append('<option onclick="calendardoc(this)" value="'+valor.idmedico+'">'+valor.nombre+' '+valor.apellido+'</option>');
});});
});

function calendardoc(val){
    $('.activo').removeClass('activo');
    $(val).addClass('activo');
    nomdoctor = val.value;
    var datos=$("#titulocalendario").attr("rel");
    var nueva_fecha=datos.split("-");
    generar_calendario(nueva_fecha[1],nueva_fecha[0]);
}
function calendarespe(val){
    if ($(val).html()=="Todos los Medicos")
    {
        $('.activo').removeClass('activo');
        $(val).addClass('activo');
    }
    if(val.value!=='0'){
        nomdoctor = '';
        nomespe  = val.value;
        var datos=$("#titulocalendario").attr("rel");
        var nueva_fecha=datos.split("-");
        generar_calendario(nueva_fecha[1],nueva_fecha[0]);
    }
}
</script>

    <?php 
    //POPUP
    $this->beginWidget(
    'booster.widgets.TbModal',
    array('id' => 'myModal')
    ); ?>
     
    <div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Ingreso de agenda
    <span id="fin" style="color:blue; float:right"></span>
    <span style="float:right;">&nbsp;de&nbsp;</span> 
    <span id="ini" style="color:green; float:right"></span> 
    </h4>
    </div>
     
    <div class="modal-body">
        <label id="fechaetiqueta" style="float:right;"></label> 
    <?php 
    //FORMULARIO
    $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'agenda-form',
        'htmlOptions'=>array('name'=>'forma',),
	'type' => 'horizontal',
	'enableAjaxValidation'=>TRUE,
        'enableClientValidation'=>TRUE,
    )); ?>

	<p class="help-block">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>
        <div id="mensaje" class="alert in fade alert-danger"></div>
		<?php //$tiposAgenda = array('1'=>'Referencia','3'=>'Extra Referencia','Reagendar'=>'Referencia');
                $tiposAgenda = CHtml::listData(Tipoagenda::model()->findAll('estado = true and idrol = 2 order by idtipoagenda'),'idtipoagenda','tipoagenda');
		echo $form->dropDownListGroup($model,'idtipoagenda',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $tiposAgenda ,))); ?>
                <?php echo $form->hiddenField($model,'idpaciente','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
                <?php echo $form->hiddenField($model,'numeroturno','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
                <div class="form-group">
                <?php //echo $form->label($model,'nombre',array('class'=>'col-sm-3 control-label required')); ?> 
                <label class="col-sm-3 control-label required" for="Agenda_nombre">Nombre Del Paciente<span class="required">*</span></label>
                <?php echo $form->hiddenField($model,'nombre','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>		
               
                <div class="col-sm-9">
                 <div class="input-group">
                <?php $form->hiddenField($model,'nombre','',array('size'=>10,'class'=>'span5 form-control')); 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'nombre',
                'model'=>$model,
                'source'=>$this->createUrl('Agenda/autocompletar'),
                'options'=>array
                (
                    'showAnim'=>'fold',
                    'select'=>"js:function(event, ui) 
                    {
                        $('#".CHtml::activeId($model,'nombre')."').val(ui.item.value); 
                        $('#Agenda_idpaciente').val(ui.item.id);
                        $('#Agenda_telefono').val(ui.item.telefono);
                        verificarReagenda();
                    }",
                ),
                'htmlOptions'=>array
                (
                    //'size'=>30,
                    'maxlength'=>50,
                    'placeholder'=>'Buscar Paciente...',
                    'title'=>'Indique el Paciente',
                    'class'=>'span5 form-control',
                    'style'=>'position:initial;',
                    'onkeyup'=>"this.value=this.value.toUpperCase();$('#".CHtml::activeId($model,'nombre')."').val($('#nombre').val()); "
                ),
                ));
                ?> 
                     <span class="input-group-addon"><a href="#" onclick ="borrarnombre()" class="glyphicon glyphicon-trash" title='Borrar Paciente'></a></span>
                </div>
                </div>
                 </div>
        <?php echo $form->textFieldGroup($model,'telefono',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10,)))); ?>		            
        <?php echo $form->textFieldGroup($model,'celular',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10,)))); ?>		            
  <div class="form-group">
                <?php //echo $form->labelEx($model,'idsubcentro',array('class'=>'col-sm-3 control-label required')); ?> 
               <label class="col-sm-3 control-label required" for="Agenda_idsubcentro">Centro de Salud de procedencia<span class="required">*</span></label>
                <div class="col-sm-9">
                    <div class="input-group">
                <?php echo $form->hiddenField($model,'idsubcentro','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'subcentro',
                'model'=>$model,
                'source'=>$this->createUrl('Agenda/autocompletarcentro'),
                'options'=>array
                (
                    'showAnim'=>'fold',
                    'select'=>"js:function(event, ui) 
                    {
                        $('#".CHtml::activeId($model,'idsubcentro')."').val(ui.item.id); 
                        
                    }",
                ),
                'htmlOptions'=>array
                (
                    'size'=>35,
                    'maxlength'=>50,
                    'placeholder'=>'Buscar Centros de Salud...',
                    'title'=>'Indique el Centro de Salud',
                    'class'=>'span5 form-control',
                    'style'=>'position:initial;',
                     'onkeyup'=>"this.value=this.value.toUpperCase();",
                    //'onkeypress'=>"$('#".CHtml::activeId($model,'nombre')."').val($('#nombre').val()); "
                ),
                ));
  	?><span class="input-group-addon"><a href="#" onclick ="borrarsubcentro()" class="glyphicon glyphicon-trash" title ="Borrar Centro de Salud" ></a></span>
                </div>
                </div>
                 </div>

                <?php //echo $form->textFieldGroup($model,'celular',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>		
                
                 <?php echo $form->hiddenField($model,'fecha','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control')); ?>
                 <?php echo $form->hiddenField($model,'idmedico','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control','value'=>$ident)); ?>
        <div class="btn-group" data-toggle="buttons" id='horario'>
    </div>
    <div class="modal-footer">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'button',
					'context'=>'primary',
                                        'htmlOptions' => array(
                                            'onclick'=>'if(verificarEnvio())forma.submit();',
                                        ),
					'label'=>$model->isNewRecord ? 'Ingresa' : 'Guardar',
				)); ?>
		<?php $this->endWidget(); 
		//FIN DE FORMULARIO	?>
	</div>
    </div>
     
    <?php $this->endWidget(); ?>