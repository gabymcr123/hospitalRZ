<?php
/* @var $this AgendaController */
/* @var $data Agenda */
?>

<div class="view">


	<b><?php echo CHtml::encode($data->getAttributeLabel('idtipoagenda')); ?>:</b>
	<?php echo CHtml::encode($data->pktipoagenda->tipoagenda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idmedico')); ?>:</b>
	<?php echo CHtml::encode($data->pkmedico->nombreCompleto); ?>
	<br />
	
	<b><?php echo CHtml::link(CHtml::encode("Ver Detalles"), array('view', 'id'=>$data->idagenda)); ?></b>
	<?php /* echo CHtml::encode($data->getAttributeLabel('idsubcentro')); ?>:</b>
	<?php echo CHtml::encode($data->pksubcentro->subcentro); ?>
	<br />

	

	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroturno')); ?>:</b>
	<?php echo CHtml::encode($data->numeroturno); ?>
	<br />

	<?php *
	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('horainicio')); ?>:</b>
	<?php echo CHtml::encode($data->horainicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('horafin')); ?>:</b>
	<?php echo CHtml::encode($data->horafin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creadopor')); ?>:</b>
	<?php echo CHtml::encode($data->creadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modificadopor')); ?>:</b>
	<?php echo CHtml::encode($data->modificadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacreado')); ?>:</b>
	<?php echo CHtml::encode($data->fechacreado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechamodificado')); ?>:</b>
	<?php echo CHtml::encode($data->fechamodificado); ?>
	<br />

	*/ ?>

</div>
<hr>
