<?php
$doct= isset($ident) ? true : false;
/* @var $this AgendaController */
/* @var $model Agenda */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerCoreScript('jquery');
if (isset($_GET["mes"])) $mes = $_GET["mes"]; else $mes = '';
if (isset($_GET["anio"])) $anio = $_GET["anio"]; else $anio = '';
if (isset($_GET["doctor"])) $doctor = $_GET["doctor"]; else $doctor = '';
if (isset($_GET["especialidad"])) $especialidad = $_GET["especialidad"]; else $especialidad = '';
?>

<div class="form">
<link rel="stylesheet" type="text/css" media="all" href="css/calendar.css" />
<?php
    if($doct){
        $this->datosdoctor($ident);
    }else{
        $this->datosnodoctor();
    }
?>
    	<div class="calendario_ajax">
            <div class="cal"></div><div id="mask"></div>
                
        </div>
<div id="horarioinformacion" style="display: block;width: 100%;" ></div>
</div>
    <script type="text/javascript">
       //var envio = 'ajax_calendario.php';
       var envio = 'index.php?r=agenda/calendario';
       var valdoctor = "";
       var informacion = '<?php print isset(Yii::app()->user->idrol) ? Yii::app()->user->idrol: ''; ?>'
       var agenda=$(".cal");
       var nomdoctor = '<?php if($doct){print $ident; }else{print $doctor;}?>';
       var nomespe = '';
       var ID ="";
       var datos;
       var busq = false;
       var limiref = 0;
    function generar_calendario(mes,anio)
	{
		var agenda=$(".cal");
		agenda.html("<img src='images/loading.gif'>");
		$.ajax({
			type: "GET",
			url: envio,
			cache: false,
			data: { mes:mes,anio:anio,accion:"generar_calendario",especialidad:nomespe,doctor:nomdoctor}
		}).done(function( respuesta )
		{
			agenda.html(respuesta);
                        datos =$("#titulocalendario").attr("rel");
                        if(informacion==4){
                            $.ajax({
                                url: 'index.php?r=agenda/horarioinformacion',
                                data :{doctor:nomdoctor},
                                type:'GET',
                                success: function(resp){
                                    $('#horarioinformacion').html(resp);
                                }
                            });
                        }
		});
	}
    
    function iniciofin(inicio, fin){
        if(inicio>=fin){
            $('#mensaje').attr('class','alert in fade alert-danger');
            $('#mensaje').html('<strong>Citas completas</strong>');
            $('#mensaje').show('show');
            $('#yw0').attr('disabled','disabled');
         }else{
            $('#mensaje').hide('show');
            $('#yw0').removeAttr('disabled');
         }
    }
    function verificarEnvio(){
        var r = true;
        $('#Agenda_numeroturno').val($('.active > input').val());
        if($('#nombre').val()=='' || $('#Agenda_nombre').val() == ''){
            r = false;
            $('#mensaje').attr('class','alert in fade alert-warning');
            $('#mensaje').html('<strong>Datos incompletos.</strong>');
            $('#mensaje').show('show');
            setTimeout(function(){ $('#mensaje').hide('show'); }, 1800);
        }
            
        return r;
    }
    function verificarReagenda(){
        var ident = $('#Agenda_idtipoagenda').val();
        if(($('#Agenda_idpaciente').val()!='' && $('#Agenda_idpaciente').val() != null) && (ident == 5 || ident == 6))
        $.ajax({
            url:'index.php?r=/agenda/disponibilidad',
            type:'GET',
            data: {'id':$('#Agenda_idtipoagenda').val(),'fecha':$('#Agenda_fecha').val(),'paciente':$('#Agenda_idpaciente').val(),'especialidad':nomespe},
            success: function (resp) {
                if(resp=='<strong>Atención: </strong>Paciente no tiene cita a reagendar'){
                    $('#mensaje').attr('class','alert in fade alert-warning');
                    //$('#yw0').attr('disabled','disabled');
                }else{
                    $('#mensaje').attr('class','alert in fade alert-success');
                    //$('#yw0').removeAttr("disabled")
                }
                $('#mensaje').show('show');
                $('#mensaje').html(resp);
            },
        });
    else{
    $('#mensaje').hide('show');
    $('#yw0').removeAttr('disabled');
    iniciofin(parseInt($('#ini').html())-1,parseInt($('#fin').html()));
    }
}
	function formatDate (input) {
		var datePart = input.match(/\d+/g),
		year = datePart[0].substring(2),
		month = datePart[1], day = datePart[2];
		return day+'-'+month+'-'+year;
	}
/*__________________________________________________________________________*/
	//COMENZAR CALENDARIO
		$(document).ready(function(){
			/* GENERAMOS CALENDARIO CON FECHA DE HOY */
			generar_calendario("<?php echo $mes ?>","<?php echo $anio ?>");
                        
/*__________________________________________________________________________*/
			/* AGREGAR UN EVENTO */
			$(document).on("click",'a.add',function(e){   
                            document.getElementById("Agenda_idtipoagenda").selectedIndex = 0;
                            $('#horario').show('show');
                            $('#nombre').val('');
                            $('#Agenda_nombre').val('');
                            $('#Agenda_telefono').val('');
                            $('#Agenda_idsubcentro').val('');
                            $('#Agenda_idpaciente').val('');
                            $('#Agenda_celular').val('');
                            $('#historial').val();
                            $('#subcentro').val('');
                            /*if($('#especialidad').val()==0)
                                return;*/
                            if ($(this).hasClass('limiref')){ 
                                limiref = 1;
                            }else{
                                limiref = 0;
                            }
                                
                            e.preventDefault();
                            //var id = $(this).data('evento');
                            var fecha = $(this).attr('rel');
                            $('#Agenda_fecha').val(fecha);
                            $('#fechaetiqueta').html(fecha);
				var par = {'id':nomdoctor,'fecha':fecha};
				  $.ajax({
				    url:'index.php?r=/agenda/valoresagenda',
				    type:'GET',
                                    data: par,
				    success: function (response) {
				    	var mat = new Array();
				    	mat = response.split('_');
                                        ID = mat[0];
                                        $('#Agenda_idmedico').find('option').each(function(){ $(this).remove(); });
                                        $('#Agenda_idmedico').append("<option value="+mat[0]+" > "+mat[1]+" </option>");
					$('#Agenda_idmedico').val();
					$('#ini').html(parseInt(mat[2])+1);   	
					$('#fin').html(mat[3]);
                                        $('#Agenda_numeroturno').val(parseInt(mat[2])+1);
                                        iniciofin(parseInt(mat[2]),parseInt(mat[3]));
                                        <?php if($doct) print '$("#Agenda_idtipoagenda").change();'?>
                                        par = {'idmedico':ID,'fecha':fecha};
                                        $.ajax({
                                              url:'index.php?r=/agenda/obtenerhorario',
                                              type:'GET',
                                              data: par,
                                              success: function (resp) {
                                                  $('#horario').html(resp);
                                                  validalimiref();
                                        },
                                        });
                                        $('#historial').focus();
			      },
			      });
			});
/*__________________________________________________________________________*/

/* EVENTO ONCHANGE PARA TIPO DE CONSULTA */
$('#Agenda_idtipoagenda').change(function(){
    var ident = this.value;
    verificarReagenda();
    var par = {'id':ident,'idme':ID,'fecha':$('#Agenda_fecha').val(),'pasar':'<?php if($doct) print 'si'; else print 'no';?>'};
    $.ajax({
    url:'index.php?r=/agenda/tipoagenda',
    type:'GET',
    data: par,
    success: function (response) {
        if(response == "._."){
            $('#yw0').attr('disabled','disabled');
            $('#mensaje').attr('class','alert in fade alert-danger');
            $('#mensaje').html('<strong>Alerta: </strong> No tiene permisos para el tipo de agendamiento');
            $('#mensaje').show('show');
        }else{
            $('#yw0').removeAttr('disabled');
            var mat = new Array();
            mat = response.split('_');
            $('#ini').html(parseInt(mat[0])+1);
            $('#fin').html(parseInt(mat[1]));
            $('Agenda_numeroturno').val(parseInt(mat[0]));
            var agend = parseInt($('#Agenda_idtipoagenda').val());
            iniciofin(parseInt(mat[0]),parseInt(mat[1]))
            if(agend==3 || agend==4){
                $('#horario').hide('show');
            }else{
                $('#horario').show("show");
            }
            /*REVISAR DESPUES */
//            if(parseInt(mat[0])==parseInt(mat[1])){
//                $('#mensaje').attr('class','alert in fade alert-warn');
//                $('#mensaje').html('<strong>Atencion: </strong> A partir de Ahora es agenda Extra');
//                $('#mensaje').show('show');
//            }
    }
      },
     });
});

/* FUNCION PARA VERIFICAR SI EL PACIENTE LO PUEDEN REAGENDAR*/
/*_______________________________________________________________________*/
			/* LISTAR EVENTOS DEL DIA */
			$(document).on("click",'a.falta',function(e)
			{
                                enviolista='index.php?r=agenda/lista';
				e.preventDefault();
				var fecha = $(this).attr('rel');

				$('#mask').fadeIn(1000).html("<div id='nuevo_evento' class='window' rel='"+fecha+"'>Eventos del "+formatDate(fecha)+"</h2><a href='#' class='close' rel='"+fecha+"'>&nbsp;</a><div id='respuesta'></div><div id='respuesta_form'></div></div>");
				$.ajax({
					type: "GET",
					url: enviolista,
					cache: false,
					data: { fecha:fecha,accion:"lista_evento",especialidad:nomespe,doctor:nomdoctor }
				}).done(function( respuesta )
				{       
                                    $("#respuesta_form").html(respuesta);
				});

			});
                        /* FIN DE LISTA DE EVENTO */
			$(document).on("click",'.close',function (e)
			{
				e.preventDefault();
				$('#mask').fadeOut();
				setTimeout(500);
			});

			//guardar evento
			$(document).on("click",'.enviar',function (e)
			{
				e.preventDefault();
				if ($("#evento_titulo").valid()==true)
				{
					$("#respuesta_form").html("<img src='images/loading.gif'>");
					var evento=$("#evento_titulo").val();
					var fecha=$("#evento_fecha").val();
					$.ajax({
						type: "GET",
						url: envio,
						cache: false,
						data: { evento:evento,fecha:fecha,accion:"guardar_evento" }
					}).done(function( respuesta2 )
					{
						$("#respuesta_form").html(respuesta2);
						$(".formeventos.close").hide();
						setTimeout(function()
						{
							$('#mask').fadeOut('fast');
							var fechacal=fecha.split("-");
							generar_calendario(fechacal[1],fechacal[0]);
						}, 3000);
					});
				}
			});
			$(document).on("click",".anterior,.siguiente",function(e)
			{
				e.preventDefault();
				var datos=$(this).attr("rel");
				var nueva_fecha=datos.split("-");
				generar_calendario(nueva_fecha[1],nueva_fecha[0]);
			});
                    /* Aquí podría filtrar que controles necesitará manejar,
                     * en el caso de incluir un dropbox $('input, select');
                     */
                   $('#historial').keyup(enter2tab);
		});
function enter2tab(e) {
                if (e.keyCode == 13) {
                    var estado = true;
                    if(this.value=="")
                        return;
                    var action = 'index.php?r=/agenda/consultahistorial&historial='+this.value;
                    $.getJSON(action, function(listaJson) {
                    $.each(listaJson, function(key, valor) {
                        estado = false;
                        $('#Agenda_celular').val(valor.celular);
                        $('#nombre').val(valor.nombre+' '+valor.apellido);
                        $('#Agenda_nombre').val(valor.nombre+' '+valor.apellido);
                        $('#Agenda_telefono').val(valor.telefono);
                        $('#Agenda_idpaciente').val(valor.idpaciente);
                        
                    });
                    if(estado){
                        $('#Agenda_celular').val('')
                        $('#nombre').val('')
                        $('#Agenda_nombre').val('')
                        $('#Agenda_telefono').val('')
                        $('#Agenda_idpaciente').val('')
                        alert('Historial clínico no existe');
                        $('#nombre').focus();
                    }
                });
                        return false;
                }
}
// creamos un evento onchange para cuando el usuario cambie su seleccion
// importante: #combo1 hace referencia al ID indicado arriba con: array('id'=>'combo1')
$('#servicio').change(function(){
var opcionSeleccionada = $(this); // el <option> seleccionado
var opcion = opcionSeleccionada.val(); // el "value" de ese <option> seleccionado
if(opcion == 0) {
//$('#siguiente').hide('slow');
return;
}
var action = 'index.php?r=/medico/obtenerespecialidad&opcion='+opcion;
// se pide al action la lista de productos de la categoria seleccionada
//
$('#reportarerror').html("");
$.getJSON(action, function(listaJson) {
//
// el action devuelve los productos en su forma JSON, el iterador "$.each" los separará.
//
// limpiar el combo productos
$('#especialidad').find('option').each(function(){ $(this).remove(); });
$('#especialidad').append("<option value='"+'0'+"'>"
+'-Seleccione Especialidad-'+"</option>");

$.each(listaJson, function(key, valor) {
//
// "producto" es un objeto JSON que representa al modelo Producto
// por tanto una llamada a: alert(producto.nombre) dira: "camisas"
$('#especialidad').append("<option value='"+valor.idespecialidad+"'>"
+valor.especialidad+"</option>");
});

}).error(function(e){ $('#reportarerror').html(e.responseText); });
});

$('#especialidad').change(function(){
    var opcionSeleccionada = $(this); // el <option> seleccionado
var opcion = opcionSeleccionada.val(); // el "value" de ese <option> seleccionado
if(opcion == 0) {
    return;
}
var action = 'index.php?r=/agenda/obtenermedicos&opcion='+opcion;
$.getJSON(action, function(listaJson) {
$('#cssmenu').html("");
var bandera=false;
//$('a.add').attr('data-target','#myModal');
$.each(listaJson, function(key, valor) {
 if(key==false){
     //$('#cssmenu').append('<option class="activo" onclick="calendarespe(this)" value="'+nomespe+'">Todos los Medicos</option>');
     $('#cssmenu').append('<option id="medico'+valor.idmedico+'" onclick="calendardoc(this)" class="activo" value="'+valor.idmedico+'">'+valor.nombre+' '+valor.apellido+'</option>');
     nomdoctor = valor.idmedico;
     if (!busq)
         ID= valor.idmedico;
     else
         busq = false;
 }else{
    $('#cssmenu').append('<option id="medico'+valor.idmedico+'" onclick="calendardoc(this)" value="'+valor.idmedico+'">'+valor.nombre+' '+valor.apellido+'</option>');
 }
});
    $('#medico'+ID).click();
});
});

function calendardoc(val){
    $('.activo').removeClass('activo');
    $(val).addClass('activo');
    nomdoctor = val.value;
    var nueva_fecha=datos.split("-");
    generar_calendario(nueva_fecha[1],nueva_fecha[0]);
}
function calendarespe(val){
    if ($(val).html()=="Todos los Medicos")
    {
        $('.activo').removeClass('activo');
        $(val).addClass('activo');
    }
    if(val.value!=='0'){
        nomdoctor = '';
        nomespe  = val.value;
        var nueva_fecha=datos.split("-");
        generar_calendario(nueva_fecha[1],nueva_fecha[0]);
    }
}
function borrarnombre() {
    $('#nombre').val('');
    $('#Agenda_nombre').val('');
    $('#Agenda_telefono').val('');
    $('#historial').val('');
    $('#nombre').focus();
}
function borrarsubcentro(){
    $('#subcentro').val('');
    $('#Agenda_idsubcentro').val('');
    $('#ciudad').html('');
    $('#subcentro').focus();
}
function validalimiref(){
    if(limiref==1 && $('#Agenda_idtipoagenda').val()=='1'){
        $('#mensaje').attr('class','alert in fade alert-danger');
        $('#mensaje').html('<strong>Agenda por \'referencia\' esta bloqueda.</strong>');
        $('#mensaje').show('show');
        $('#yw0').attr('disabled','disabled');
    }else{
         $('#mensaje').hide('show');
         $('#yw0').removeAttr('disabled');
    }
}
</script>
    
    
    <?php 
    //POPUP
    $this->beginWidget(
    'booster.widgets.TbModal',
    array('id' => 'myModal')
    ); ?>
     
<div class="modal-header" style="margin: 0px; padding: 2px 8px;">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Ingreso de agenda
    <span id="fin" style="color:blue; float:right"></span>
    <span style="float:right;">&nbsp;de&nbsp;</span> 
    <span id="ini" style="color:green; float:right"></span> 
    </h4>
    </div>
     
    <div class="modal-body">
        <label id="fechaetiqueta" style="float:right;"></label> 
            
    <?php 
    //FORMULARIO
    $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'agenda-form',
        'htmlOptions'=>array('name'=>'forma','style'=>'font-size:12px;'),
	'type' => 'horizontal',
	'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
	)); ?>
	<p class="help-block">Campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>

        <div id="mensaje" class="alert in fade alert-danger"></div>
        
        <div class="" style="float: right; font-size: 10px">
            <input size="15" type="text" value="" id="historial" name="historial" maxlength="10" class="form-control" placeholder="# Archivo o Cédula" autocomplete="off" onkeyup="this.value=this.value.toUpperCase();" style="font-size: 12px; padding: 0px;text-align: center;" >
        </div>
		<?php //$tiposAgenda = array('1'=>'Referencia','3'=>'Extra Referencia','Reagendar'=>'Referencia');
                if($doct):
                    $tiposAgenda = CHtml::listData(Tipoagenda::model()->findAll('estado = true and idrol = 2 order by idtipoagenda'),'idtipoagenda','tipoagenda');
                else:
                    $tiposAgenda = CHtml::listData(Tipoagenda::model()->findAll('estado = true order by idtipoagenda'),'idtipoagenda','tipoagenda');
		endif;
                echo $form->dropDownListGroup($model,'idtipoagenda',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $tiposAgenda ,))); ?>
                <?php echo $form->hiddenField($model,'idpaciente','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
                <?php echo $form->hiddenField($model,'numeroturno','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
                <div class="form-group">
                <label class="col-sm-3 control-label required" for="Agenda_nombre">Nombre Del Paciente<span class="required">*</span></label>
                <?php echo $form->hiddenField($model,'nombre','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>		
               
                <div class="col-sm-9">
                <div class="input-group">
                <?php $form->hiddenField($model,'nombre','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control')); 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'nombre',
                'model'=>$model,
                'source'=>$this->createUrl('Agenda/autocompletar'),
                'options'=>array
                (
                    'showAnim'=>'fold',
                    'select'=>"js:function(event, ui) 
                    {
                        $('#".CHtml::activeId($model,'nombre')."').val(ui.item.value); 
                        $('#Agenda_idpaciente').val(ui.item.id);
                        $('#Agenda_telefono').val(ui.item.telefono);
                        $('#Agenda_celular').val(ui.item.celular);
                        $('#historial').val(ui.item.historial);
                        $('#Agenda_idmedico').val(ID);
                        verificarReagenda();
                    }",
                ),
                'htmlOptions'=>array
                (
                    //'size'=>30,
                    'maxlength'=>50,
                    'placeholder'=>'Buscar Paciente...',
                    'title'=>'Indique el Paciente',
                    'style'=>'position:initial;',
                    'class'=>'span5 form-control',
                    'onkeyup'=>"this.value=this.value.toUpperCase();$('#".CHtml::activeId($model,'nombre')."').val($('#nombre').val()); ",
                ),
                ));
                ?> 
                
                <span class="input-group-addon"><a href="#" onclick ="borrarnombre()" class="glyphicon glyphicon-trash" title='Borrar Paciente'></a></span>
                </div>
                    </div>
                 </div>
    <?php echo $form->textFieldGroup($model,'telefono',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10,"autocomplete"=>"off")))); ?>		            
    <?php echo $form->textFieldGroup($model,'celular',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10,"autocomplete"=>"off")))); ?>		            
  <div class="form-group">
       
                <?php //echo $form->labelEx($model,'idsubcentro',array('class'=>'col-sm-3 control-label required')); ?> 
               <label class="col-sm-3 control-label required" for="Agenda_idsubcentro">Centro de Salud de procedencia<span class="required">*</span></label>
                <div class="col-sm-9">
                    <span style="font-size: 11px;"><strong>CIUDAD: </strong></span> <strong><span style="font-size: 11px;" id="ciudad"></span></strong>
                    <div class="input-group">
                <?php echo $form->hiddenField($model,'idsubcentro','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>25)))); 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'subcentro',
                'model'=>$model,
                'source'=>$this->createUrl('Agenda/autocompletarcentro'),
                'options'=>array
                (
                    'showAnim'=>'fold',
                    'select'=>"js:function(event, ui) 
                    {
                        $('#".CHtml::activeId($model,'idsubcentro')."').val(ui.item.id); 
                        $('#ciudad').html(ui.item.canton);
                        
                    }",
                ),
                'htmlOptions'=>array
                (
                    'size'=>35,
                    'maxlength'=>50,
                    'placeholder'=>'Buscar Centros de Salud...',
                    'title'=>'Indique el Cetro de Salud',
                    'style'=>'position:initial;',
                    'class'=>'span5 form-control',
                    'onkeyup'=>'this.value=this.value.toUpperCase()',
                ),
                ));
  	?>      <span class="input-group-addon"><a href="#" onclick ="borrarsubcentro()" class="glyphicon glyphicon-trash" title='Borrar Centro de Salud'></a></span>
                </div>
                </div>
                 </div>
                <?php echo $form->dropDownListGroup($model,'idmedico',array('wrapperHtmlOptions' => array('class' => 'span5',),'widgetOptions' => array())); ?>
                 <?php echo $form->hiddenField($model,'fecha','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control')); ?>
                 <?php //echo $form->textFieldGroup($model,'fecha',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','onkeypress'=>'return 0;')))); ?>		
     <div class="btn-group" data-toggle="buttons" id='horario'>
    </div>
        <div class="modal-footer" style="margin: 0px; padding: 5px 0px 0px 5px;">

			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'button',
					'context'=>'primary',
                                'htmlOptions' => array(
                                        'onclick'=>'if(verificarEnvio())forma.submit();',
                                ),
					'label'=>$model->isNewRecord ? 'Ingresa' : 'Guardar',
				)); ?>
		<?php $this->endWidget(); 
		//FIN DE FORMULARIO	?>
	</div>
    </div>
     
    <?php $this->endWidget(); ?>
        <script type="text/javascript">
            $('#cerrar').click(function(){
                $('#mensaje').hide('show');
            });
            $('#mensaje').hide();
//            function turnotemporal(obj){
//                alert($(obj +'> input').val());
//            }
               <?php 
              if ($doctor!=''){
              print '$("#especialidad").val('.$especialidad.');';
              print "$('#especialidad').change();";
              print "busq= true;";
              print "ID=".$doctor.";";
                     }
              ?>
        </script>
