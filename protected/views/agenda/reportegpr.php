<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:28px; font-size:12px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
if($mes==$i)
$selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
else
$selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios = '<select style="background:white ;height:28px; font-size:12px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2014 ;$i<=2030;$i++):
if($anio==$i)
$selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
else
$selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
echo 'Seleccionar mes/año: '.$selectmes;
$contmayora15=0;
$totalmenor=0;
$totalmayor = 0;
?>
&nbsp;&nbsp;
<?php echo $selectanios; ?>
&nbsp;&nbsp; <?php echo CHtml::link('Actualizar','index.php?r=/agenda/indicadoresGPR',array('id'=>'btnact','class'=>'btn btn-primary'));?>
&nbsp;&nbsp;
<?php //echo CHtml::link(CHtml::image(Yii::app()->baseUrl."/images/pdf.png","Generar reporte PDF",array("title"=>"Exportar a PDF",'width'=>'145px','id'=>'btnpdf')),array("reporteindicadoresgpr"), array('target'=>'_blank')); 
echo CHtml::link('Descargar Pdf','index.php?r=/agenda/reporteindicadoresgpr',array('id'=>'btnpdf','class'=>'btn btn-primary' ,'target'=>'_blank'));

?>
&nbsp;&nbsp;
<?php
echo CHtml::link('Descargar excel','index.php?r=/agenda/reporteindicadoresgprExcel',array('id'=>'btnexcel','class'=>'btn btn-primary'));
?>
<hr>
<h4 style="text-align: center">Distribución según los 15 días límites</h4>
<br>

<table id="tabla" class="items table table-bordered">
    <tr>
    <th style="background: Whitesmoke">Etiquetas de fila</th><th style="background: Whitesmoke">Menor 15 días</th><th style="background: Whitesmoke">Mayor a 15 días</th><th style="background: Whitesmoke">Total general</th></tr>
    <?php foreach($sqlespe as $row):
    $row2 = $sqlespe2[$contmayora15];
    $totalmenor = $row['total'] +$totalmenor;
    $totalmayor = $row2['total']+$totalmayor;
            ?>
    <tr>
    <td><?php print $row['especialidad'];?></td>
    <td><?php print $row['total'];?></td>
    <td><?php print $row2['total'];?></td>
    <td><?php print $row['total']+$row2['total'];?></td>
    </tr>
       <?php $contmayora15++; endforeach; ?>
    <tr style="background: Whitesmoke"> <th>Total General</th><td><?php print $totalmenor;?></td><td><?php print $totalmayor;?></td><td><?php print $totalmenor+$totalmayor;?></td></tr>       
  
</table>
<script type="text/javascript">
    $("#btnpdf").click(function (){
var anio =$('#anios').val();
var mes =$('#meses1').val();

var enlace= 'index.php?r=/agenda/reporteindicadoresgpr';
enlace = enlace +'&anio='+anio+'&mes='+mes;
var link = document.getElementById('btnpdf');
$(link).attr('href',enlace);
} );

 $("#btnexcel").click(function (){
var anio =$('#anios').val();
var mes =$('#meses1').val();

var enlace= 'index.php?r=/agenda/reporteindicadoresgprExcel';
enlace = enlace +'&anio='+anio+'&mes='+mes;
var link = document.getElementById('btnexcel');
$(link).attr('href',enlace);
} );

   $("#btnact").click(function (){
var anio =$('#anios').val();
var mes =$('#meses1').val();

var enlace= 'index.php?r=/agenda/indicadoresGPR';
enlace = enlace +'&anio='+anio+'&mes='+mes;
$(this).attr('href',enlace);
} );
    </script>