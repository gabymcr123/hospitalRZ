<?php
/* @var $this AgendaController */
/* @var $model Agenda */
/*
$this->breadcrumbs=array(
	'Citas'=>array('admin'),
	'Lista',
);
*/
$info=(isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 4) ? true : false ;
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 3) ? true : false ;
$this->menu=array(
	//array('label'=>'Lista de Citas', 'url'=>array('admin')),
	array('label'=> $info ?'Horario de los medico':'Crear Cita', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Lista de Citas</h2>
<?php 
$date = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[fx]',
                                    'value' => $model->fx,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'dd/mm/yy',
                                        'changeMonth' => 'true',
                                        'yearRange'=>'-2:+2',
                                        'minDate'=>'-2Y',
                                        'maxDate'=>'2Y',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
   'changeYear'=>'true',
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:35px;width:120px;',
                                        'class'=>'span5 form-control',
                                    ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                                ),true);

?>




<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'empresa-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'afterAjaxUpdate'=>"function() {
jQuery('#Agenda_fx').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
}",    
'filter'=>$model,
'htmlOptions'=>array('style'=>'font-size: 12px'),
'columns'=>array(
     array('name'=>'idtipoagenda',
                    'value'=>'$data->pktipoagenda->tipoagenda',
                    'filter'=> CHtml::listData(Tipoagenda::model()->findAll('estado=true order by idtipoagenda'),'idtipoagenda','tipoagenda'),
                    //'filter'=>CHtml::activeTextField($model, 'nombretipoag',array('class'=>'form-control')),
                    'sortable'=>false
                    ),
    array('name'=>'Número historial clínico',
                    'value'=>'$data->pkpaciente->historialclinico',
                    'filter'=>CHtml::activeTextField($model,'historial',array('class'=>'form-control')),
                    'sortable'=>false
                    ),
    array('name'=>'Cédula',
                    'value'=>'$data->pkpaciente->cedula',
                    'filter'=>CHtml::activeTextField($model,'cedu',array('class'=>'form-control')),
                    'sortable'=>false
                    ),
               
    
		array('name'=>'nombre',
                    'value'=>'$data->pkpaciente->nombre." ".$data->pkpaciente->apellido',
                    'filter'=>CHtml::activeTextField($model,'nombrepac',array('class'=>'form-control')),
                    'sortable'=>false
                    ),
    
                array(
                    'header'=>'Especialidad',
                    'value'=>'$data->pkmedico->idespecialidad1->especialidad',
                    'sortable'=>false,
                    'filter'=>CHtml::listData(Especialidadmedica::model()->findAll('estado=true order by especialidad'),'idespecialidad','especialidad'),
                    ),
    
		array('name'=>'idmedico',
                    'value'=>'$data->pkmedico->nombreCompleto',
                    'sortable'=>false,
                    'filter'=>CHtml::activeTextField($model, 'nombredoc',array('class'=>'form-control')),),
    
//                array('name'=>'idsubcentro',
//                    'value'=>'$data->pksubcentro->subcentro',
//                    'filter'=>CHtml::activeTextField($model, 'nombresub',array('class'=>'form-control')),
//                    'sortable'=>false),
                
                array('name'=>'fecha',
                    'value'=>'$data->fecha',
                    'filter'=> $date,
                    'sortable'=>false
                    ),
    'horainicio'=>array(
        'name' => 'horainicio',
        'sortable'=>false
                    ),
                array('name'=> 'atendido',
                'value'=> '$data->atendido ? "Si": "No";',
                //'filter'=> array(1=>'si',0=>'no'),
                    'sortable'=>false,
                    'filter' =>false
                ),
array('name'=>'Creado por',
                    'value'=>'$data->creadopor',
                    'filter'=> false,
                    'sortable'=>false,
    'visible'=>$admin
                    ),
                /*
                array('name'=> 'reagendada',
                    'value'=> '$data->reagendado ? "Si": "No";',
                    'filter'=> '',
                    'sortable'=>false,
                ),

                */

               //'idmedico',
		//'idsubcentro',
		
		/*
		'numeroturno',
		
		'horainicio',
		'horafin',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
		array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{view} {delete} {update}',
'buttons'=>array(
    'delete'=>array(
                    'label'=>'Cancelar',
                    'visible'=>"'".!$info."'",
    ),
    'update'=>array(
             'visible'=>'$data->fecha >= date("Y-m-d") ? true : false and'."'".!$info."'"
        
    ),
    ),
),
	),
)); ?>
