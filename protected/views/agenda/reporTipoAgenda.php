<Center>  <h4>Pacientes agendados por tipo de agenda</h4>
<p> 

<?php //echo CHtml::textField('fecha_inicio', ''); ?>

    <?php
    /*
$this->widget(
        'booster.widgets.TbDatePicker',
        array(
            'name'=> 'fecha_inicial',
            'options' =>  array(
               'language' => 'es',
                'format' => 'dd-mm-yyyy',
        )
        )
        ); 
     *
     */
?>
  
<?php
/*
$this->widget(
        'booster.widgets.TbDatePicker',
        array(
            'name'=> 'fecha_final',
            'options' =>  array(
               'language' => 'es',
                'format' => 'dd-mm-yyyy',
        )
        )
        );
 * 
 */ 
?>
  
<?php 
if(!isset($model->f3)){
    $model->f3= date_format(date_create(), 'd-m-Y');
}
if(!isset($model->f4)){
    $model->f4= date_format(date_create(), 'd-m-Y');
}
?>    
<?php 
$date = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[f3]',
                                    'value' => $model->f3,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'dd/mm/yy',
                                        'changeMonth' => 'true',
                                        'yearRange'=>'-80:+3',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
   'changeYear'=>'true',
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:40px;width:110px;',
                                        'class'=>'span5 form-control',
                                    ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                                ),true) . '   Hasta <br> ' . $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[f4]',
                                    
                                        'value' => $model->f4,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'dd/mm/yy',
                                                                                                                    'yearRange'=>'-80:-0',
                                                                            'minDate'=>'-80Y',
                                                                            'maxDate'=>'-0Y',

                                        'changeMonth' => 'true',
                                        'changeYear'=>'true',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:40px;width:110px',
                                        'class'=>'span5 form-control',
                                    ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                                ),true);

?>
    
    
<?php //echo CHtml::textField('txt_1'); ?>
</p>


<?php  

$this->widget('booster.widgets.TbGridView', array(
	'id'=>'agenda-grid',
	'dataProvider'=>$model->busqueda2(),
        'afterAjaxUpdate'=>"function() {    
                                        jQuery('#Agenda_f3').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
                                        jQuery('#Agenda_f4').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));

                                                }",
	'filter'=>$model,
        'htmlOptions'=>array('style'=>'word-wrap:break-word;width:950px;'),
	'columns'=>array(
		//'idagenda',
                //'fecha',
                array(
                    'name' =>'fecha',  
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    'filter'=> $date,
                    'value'=>'$data->fecha',
                    ),
                //'idmedico',
                 array(
                    'name'=>'idtipoagenda',
                    'value'=>'$data->pktipoagenda->tipoagenda',
                    'filter'=> CHtml::listData(Tipoagenda::model()->findAll(array('order'=>'tipoagenda')), 'idtipoagenda', 'tipoagenda'),
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
            ),
                
                array(
                    'name'=>'idmedico',
                    'value'=>'$data->pkmedico->nombreCompleto',
                    //'filter'=>CHtml::activeTextField($model, 'doctor',array('class'=>'form-control')),
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                 ),
		//'idpaciente',
                 array(
                    'name'=>'idpaciente',
                    'value'=>'$data->nombreCompleto',
                    //'filter'=>CHtml::activeTextField($model,'paciente',array('class'=>'form-control')),
                    'filter'=> false,
                     'htmlOptions'=>array('style'=>'height:40px;width:200px'),

                     ),
            array(
                    'name'=>'Historial clinico',
                    'value'=>'$data->pkpaciente->historialclinico',
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                     ),
		//'idtipoagenda',
		//'idsubcentro',
		//'nombre',
		
		//'numeroturno',
           //array
                  //ame'=>'fecha1', 
                  //alue'=>'$data->fecha'
            //),
            /*
		'fecha',
		'horainicio',
		'horafin',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
		
	),
)); ?>
<br>
<br>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl."/images/pdf.jpg","Generar reporte PDF",array("title"=>"Exportar a PDF")),array("generarpdf2"), array('target'=>'_blank')); ?>
</div>
<br>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl."/images/excel.jpg","Generar reporte EXCEL",array("title"=>"Exportar a EXCEL")),array("generarExcelAgenda")); ?>
</div>