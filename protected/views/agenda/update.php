<?php
/* @var $this AgendaController */
/* @var $model Agenda */

$this->breadcrumbs=array(
	'Citas'=>array('admin'),
	//$model->idagenda=>array('view','id'=>$model->idagenda),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Lista de Citas', 'url'=>array('admin')),
	array('label'=>'Nueva Cita', 'url'=>array('create')),
	array('label'=>'Detalles de Cita', 'url'=>array('view', 'id'=>$model->idagenda)),
	//array('label'=>'Administrar citas', 'url'=>array('admin')),
);
?>

<h2>Actualizar Cita</h2>

<?php $this->renderPartial('_form_update', array('model'=>$model)); ?>