<?php
/* @var $this AgendaController */
/* @var $model Agenda */
/*
$this->breadcrumbs=array(
	'Citas'=>array('admin'),
	'Lista',
);
*/
$info=(isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 4) ? true : false ;
$this->menu=array(
	//array('label'=>'Lista de Citas', 'url'=>array('admin')),
	array('label'=> $info ?'Horario de los medico':'Crear Cita', 'url'=>array('create')),
    array('label'=> 'Agenda del día', 'url'=>array('agenda2/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Ultimas citas agendadas</h3>
<?php 
$date = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[fx]',
                                    'value' => $model->fx,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'dd/mm/yy',
                                        'changeMonth' => 'true',
                                        'yearRange'=>'-2:+2',
                                        'minDate'=>'-2Y',
                                        'maxDate'=>'2Y',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
   'changeYear'=>'true',
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:35px;width:120px;',
                                        'class'=>'span5 form-control',
                                    ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                                ),true);


$model->creadopor = $user;
?>




<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'empresa-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'afterAjaxUpdate'=>"function() {
                                                jQuery('#Agenda_fx').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
                                                }",    
'filter'=>$model,
'htmlOptions'=>array('style'=>'font-size: 12px'),
'columns'=>array(
                array('name'=>'idtipoagenda',
                    'value'=>'$data->pktipoagenda->tipoagenda',
                    //'filter'=> CHtml::listData(Tipoagenda::model()->findAll('estado=true order by idtipoagenda'),'idtipoagenda','tipoagenda'),
                    'filter'=>'',
                    'sortable'=>false
                    ),
    
		array('name'=>'nombre',
                    'value'=>'$data->pkpaciente->nombre." ".$data->pkpaciente->apellido',
                    'filter'=>CHtml::activeTextField($model,'nombrepac',array('class'=>'form-control')),
                    'sortable'=>false
                    ),
    
                array(
                    'header'=>'Especialidad',
                    'value'=>'$data->pkmedico->idespecialidad1->especialidad',
                    'sortable'=>false,
                    'filter'=>CHtml::listData(Especialidadmedica::model()->findAll('estado=true order by especialidad'),'idespecialidad','especialidad'),
                    ),

                array('name'=>'fecha',
                    'value'=>'$data->fecha',
                    'filter'=> $date,
                    'sortable'=>false
                    ),
                array('name'=> 'atendido',
                'value'=> '$data->atendido ? "Si": "No";',
                //'filter'=> array(1=>'si',0=>'no'),
                    'sortable'=>false,
                    'filter' =>false
                ),

                /*
                array('name'=> 'reagendada',
                    'value'=> '$data->reagendado ? "Si": "No";',
                    'filter'=> '',
                    'sortable'=>false,
                ),

                */

               //'idmedico',
		//'idsubcentro',
		
		/*
		'numeroturno',
		
		'horainicio',
		'horafin',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
		array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{view} {delete}',
'buttons'=>array(
    'delete'=>array(//'label'=>'Cancelar',
                    'label'=>'Cancelar',
                    //'visible'=>'$data->fecha <  ? true, false',
    ),
    'update'=>array(
             'visible'=>'$data->fecha >= date("Y-m-d") ? true : false'
        
    ),
    ),
),
	),
)); ?>
