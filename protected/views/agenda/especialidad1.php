<?php $contador=count($model); if ($model !== null):?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 10pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
 border-left: 0.1mm solid #000000;
 border-right: 0.1mm solid #000000;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items td.blanktotal {
 background-color: #FFFFFF;
 border: 0mm none #000000;
 border-top: 0.1mm solid #000000;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
</head>
<body>
 
<!--mpdf
 <htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="50%" style="color:#0000BB;"><span style="font-weight: bold; font-size: 14pt;">Hospital General Rodriguez Zambrano De Manta</span><br />Manta-Ecuador<br /><span style="font-size: 15pt;">&#9742;</span> falta</td>
 <td width="50%" style="text-align: right;"><b>Pacientes agendados por doctor</b></td>
 </tr></table>
 </htmlpageheader>
 
<htmlpagefooter name="myfooter">
 <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
 Página {PAGENO} de {nb}
 </div>
 </htmlpagefooter>
 
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>

<br>
 <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse;" cellpadding="5">
 <thead>
 <tr>
 <td width="16.666666666667%">Fecha</td>
 <td width="16.666666666667%">Profesional de la salud</td>
 <td width="16.666666666667%">Nombre del paciente</td>
 <td width="16.666666666667%">Tipo de agenda</td>
 <td width="16.666666666667%">Numero de turno</td>
 <td width="16.666666666667%">Hora de incio</td>
 <td width="16.666666666667%">Hora de fin</td>
 </tr>
 </thead>
 <tbody>
 <!-- ITEMS -->
 <?php foreach($model as $row): ?>
 <tr>
 <td align="center">
 <?php echo $row->fecha; ?>
 </td>
 <td align="center">
 <?php echo $row->pkmedico->nombreCompleto;?>
 </td>
 <td align="center">
 <?php echo $row->pkpaciente->nombreCompleto;?>
 </td>
 <td align="center">
 <?php echo $row->pktipoagenda->tipoagenda;?>
 </td> 
 <td align="center">
 <?php echo $row->numeroturno;?>
 </td>
 <td align="center">
 <?php echo $row->horainicio;?>
 </td> 
  <td align="center">
 <?php echo $row->horafin;?>
 </td> 
 </tr>
 <?php endforeach; ?>
 <!-- FIN ITEMS -->
 <tr>
 <td class="blanktotal" colspan="6" rowspan="6"></td>
 </tr>
 </tbody>
 </table>
 </body>
 </html>
<?php endif; ?>

