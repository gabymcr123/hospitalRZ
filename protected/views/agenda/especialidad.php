<?php
$this->breadcrumbs=array(
	//'Eventos'=>array('index'),
	'Reportes',
);
?>
<h2>Cantidad de pacientes por especialidad</h2>
<br>
<?php $model=  Agenda::model(); ?>
<h4><b>*</b>&nbsp;&nbsp;Seleccione rango de fechas</h4>
<br>
<div class="col-sm-4">
<?php
$this->widget(
        'booster.widgets.TbDatePicker',
        array(
            'name'=> 'combo1',
            'options' =>  array(
               'language' => 'es',
                'format' => 'dd-mm-yyyy',
        )
        )
        );
?>
    </div>
   Hasta &nbsp;&nbsp;
<?php
// CHtml::activeDateField($model, 'fecha', array('class'=>'form-control','id'=>'combo2'));

$this->widget('booster.widgets.TbDatePicker',array('name'=> 'combo2','options' =>  array('language' => 'es','format' => 'dd-mm-yyyy',)));?>
<br>
<h4><b>*</b>&nbsp;&nbsp;Seleccionar especialidad</h4>
<br>
<div class="col-sm-4">
<?php
$lista = CHtml::listData(Especialidadmedica::model()->findAll(), 'idespecialidad', 'especialidad');
echo CHtml::dropDownList('especialidad','', $lista,array('prompt'=>'--Seleccione--','class'=>'form-control'));
?><br>
</div>
<br>
<br>
<center>
<div class="enlaceboton" style="width: 120px">
<?php echo CHtml::ajaxLink('Ver reporte',   array('listaespecialidad'),// the URL for the AJAX request. If empty, it is assumed to be the current URL.
        array(
        'update'=>'#pacientes',
        'data'=> 'js:{"fechainicio":getfechainicio(),"fechafinal":getfechafinal(),"especialidad1":getespecialidad()}',)); ?>
</div>
</center>
<hr>
<div id="pacientes">
</div>
<center>
<?php
echo CHtml::link('Descargar Excel','index.php?r=/agenda/reporteexcelespecialidadantidad',array('id'=>'btnpdf','class'=>'btn btn-primary'));
?>
    </center>
<script type="text/javascript" >
    function getfechainicio(){
        return  $("#combo1").val();
    }
     function getfechafinal(){
        return  $("#combo2").val();
    }
    
    function getespecialidad(){
        return $("#especialidad").val();
    }
    
    $("#btnpdf").click(function (){
var f1 = $("#combo1").val();
var f2 = $("#combo2").val();
var espe = $("#especialidad").val();
var enlace= 'index.php?r=/agenda/reporteexcelespecialidadantidad';
enlace = enlace +'&f1='+f1+'&f2='+f2+'&espe='+espe;
 var link = document.getElementById('btnpdf');
$(link).attr('href',enlace);

} );
    
   
</script>


