<?php $contador=count($model); if ($model !== null):?>
<html>
    <meta chartset="utf-8" />
<head>
     <meta charset="<?php echo Yii::app()->charset;?>">
<style>
 body {
font-family: sans-serif;
 font-size: 9pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
 border-left: 0.1mm solid #000000;
 border-right: 0.1mm solid #000000;
 }
 table thead td { 
background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items td.blanktotal {
 background-color: #FFFFFF;
 border: 0mm none #000000;
 border-top: 0.1mm solid #000000;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
</head>
<body>
<table width="100%"><tr>
 <td width="50%" style="color:#0000BB;"><span style="font-weight: bold; font-size: 14pt;">Hospital General Rodriguez Zambrano De Manta</span><br />Manta-Ecuador<br /><span style="font-size: 15pt;">&#9742;</span> 593(5)2611849 Ext 5056</td>
 <td width="50%" style="text-align: right;"><b>Pacientes Agendados</b></td>
 </tr></table>

<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<div style="text-align: right"><b>Total: </b> <?php echo $contador; ?></div>

<br>
<table id="tabla" class="items table table-bordered" style="margin: 0 auto;">
 <thead>
 <tr>
 <td width="16.666666666667%">Nombres</td>
 <td width="16.666666666667%">Apellidos</td>
 <td width="16.666666666667%">Número de historia clínica</td>
 <td width="16.666666666667%">Cédula</td>
 <td width="16.666666666667%">Teléfono</td>
 <td width="16.666666666667%">Teléfono convencional</td>
 <td width="16.666666666667%">Subcentro de procedencia</td>
 </tr>
 </thead>
 <tbody>
 <!-- ITEMS -->
 <?php foreach($model as $row): ?>
 
 <tr>
 <td align="center">
    <?php echo $row->fecha?>
    </td>
    <td align="center">
    <?php echo $row->pkmedico->apellido." ".$row->pkmedico->nombre?>
    </td>
  
    <td align="center">
    <?php echo $row->pkpaciente->apellido." ".$row->pkpaciente->nombre?>
    </td> 
      <td align="center">
    <?php echo $row->pkpaciente->telefono?>
    </td>
     <td align="center">
    <?php echo $row->pkpaciente->celular?>
    </td> 
    <td align="center">
    <?php echo $row->pkpaciente->historialclinico?>
    </td>
    <td align="center">
    <?php echo $row->numeroturno?>
    </td> 
 </tr>
 <?php endforeach; ?>
 <!-- FIN ITEMS -->
 <tr>
 <td class="blanktotal" colspan="6" rowspan="6"></td>
 </tr>
 </tbody>
 </table>
 </body>
 </html>
<?php endif; ?>



