<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'Eventos'=>array('index'),
	'Reportes',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php 
//if(!isset($model->fs)){
 //   $model->fs= date_format(date_create(), 'd-m-Y');
//}
?>
<center><h4>Pacientes atendidos en el período de 15 días</h4>

<?php 
if(!isset($model->f3)){
    $model->f3= date_format(date_create(), 'Y-m-d');
}
if(!isset($model->f4)){
    $model->f4= date_format(date_create(), 'Y-m-d');
}
?>    
<?php 
$date = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'Agenda[f3]',
                                    'value' => $model->f3,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'yy-mm-dd',
                                        'changeMonth' => 'true',
                                        'yearRange'=>'-4:+3',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                                    'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                                    'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
                                    'changeYear'=>'true',
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:40px;width:110px;',
                                        'class'=>'span5 form-control',
                                    ), ),true);
$date2 = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[f4]',
                                    'value' => $model->f4,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'yy-mm-dd',
                                        'changeMonth' => 'true',
                                        'yearRange'=>'-4:+3',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                                    'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                                    'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
                                    'changeYear'=>'true',
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:40px;width:110px;',
                                        'class'=>'span5 form-control',
                                    ), ),true);

?>
<?php  

$this->widget('booster.widgets.TbGridView', array(
	'id'=>'agenda-grid',
	'dataProvider'=>$model->busqueda4(),
        'afterAjaxUpdate'=>"function() {    
            jQuery('#Agenda_f3').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
            jQuery('#Agenda_f4').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
        }",
	'filter'=>$model,
        'htmlOptions'=>array('style'=>'word-wrap:break-word;width:950px;'),
	'columns'=>array(
		//'idagenda',
                //'fecha',
            array(
                    'header' =>'Fecha agendada',  
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    'filter'=> $date,
                    'value'=>'$data->fechacreado',
                    ),
                array(
                    'header' =>'Fecha de atencion medica',  
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    'filter'=> $date2,
                    'value'=>'$data->fecha',
                    ),
            
                //'idmedico',
                 array(
                    'name'=>'idtipoagenda',
                    'value'=>'$data->pktipoagenda->tipoagenda',
                    //'filter'=> CHtml::listData(Tipoagenda::model()->findAll(array('order'=>'tipoagenda')), 'idtipoagenda', 'tipoagenda'),
                     'filter'=>'',
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
            ),
                
                array(
                    'name'=>'idmedico',
                    'value'=>'$data->pkmedico->nombreCompleto',
                    //'filter'=>CHtml::activeTextField($model, 'doctor',array('class'=>'form-control')),
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                 ),
		//'idpaciente',
                 array(
                    'name'=>'idpaciente',
                    'value'=>'$data->nombreCompleto',
                    //'filter'=>CHtml::activeTextField($model,'paciente',array('class'=>'form-control')),
                    'filter'=> false,
                     'htmlOptions'=>array('style'=>'height:40px;width:200px'),

                     ),
            array(
                    'name'=>'Historial clinico',
                    'value'=>'$data->pkpaciente->historialclinico',
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                     ),
		//'idtipoagenda',
		//'idsubcentro',
		//'nombre',
		
		//'numeroturno',
           //array
                  //ame'=>'fecha1', 
                  //alue'=>'$data->fecha'
            //),
            /*
		'fecha',
		'horainicio',
		'horafin',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
		
	),
)); ?>
<br>
<br>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl."/images/pdf.png","Generar reporte PDF",array("title"=>"Exportar a PDF",'width'=>'145px')),array("generarpdf4"), array('target'=>'_blank')); ?>
</div>
<br>
<div class="enlaceboton" style="width: 160px">
<?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl."/images/excel.png","Generar reporte EXCEL",array("title"=>"Exportar a EXCEL",'width'=>'85px','height'=>'60px')),array("generarExcelPaci15Dias")); ?>
</div>

