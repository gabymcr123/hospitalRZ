<?php
/* @var $this AgendaController */
/* @var $model Agenda */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idagenda'); ?>
		<?php echo $form->textField($model,'idagenda'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idtipoagenda'); ?>
		<?php echo $form->textField($model,'idtipoagenda'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idpaciente'); ?>
		<?php echo $form->textField($model,'idpaciente'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idmedico'); ?>
		<?php echo $form->textField($model,'idmedico'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idsubcentro'); ?>
		<?php echo $form->textField($model,'idsubcentro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numeroturno'); ?>
		<?php echo $form->textField($model,'numeroturno'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'horainicio'); ?>
		<?php echo $form->textField($model,'horainicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'horafin'); ?>
		<?php echo $form->textField($model,'horafin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado'); ?>
		<?php echo $form->checkBox($model,'estado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'creadopor'); ?>
		<?php echo $form->textField($model,'creadopor',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modificadopor'); ?>
		<?php echo $form->textField($model,'modificadopor',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechacreado'); ?>
		<?php echo $form->textField($model,'fechacreado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechamodificado'); ?>
		<?php echo $form->textField($model,'fechamodificado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->