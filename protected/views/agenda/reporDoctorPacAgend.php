<?php
$this->breadcrumbs=array(
	//'Eventos'=>array('index'),
	'Reportes',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php 
if(!isset($model->f1)){
    $model->f1= date_format(date_create(), 'd-m-Y');
}
if(!isset($model->f2)){
    $model->f2= date_format(date_create(), 'd-m-Y');
}
?>
<center><h4>Pacientes agendados por profesional de la salud</h4>

<?php 
$date = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[f1]',
                                    'value' => $model->f1,
                                    
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'dd/mm/yy',
                                        'changeMonth' => 'true',
                                        'yearRange'=>'-80:+3',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
   'changeYear'=>'true',
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:40px;width:110px;',
                                        'class'=>'span5 form-control',
                                    ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                                ),true) . 'Hasta <br> ' . $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[f2]',
                                    
                                        'value' => $model->f2,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'dd/mm/yy',
                                       'yearRange'=>'-80:+3',
                                        'changeMonth' => 'true',
                                        'changeYear'=>'true',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:40px;width:110px',
                                        'class'=>'span5 form-control',
                                    ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                                ),true);

?>

<?php
$this->widget('booster.widgets.TbGridView', array(
	'id'=>'agenda-grid',
	'dataProvider'=>$model->busqueda1(),
        'afterAjaxUpdate'=>"function() {
                                                jQuery('#Agenda_f1').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
                                                jQuery('#Agenda_f2').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
                                                }",
	'filter'=>$model,
        'htmlOptions'=>array('style'=>'word-wrap:break-word;width:950px;'),
	'columns'=>array(
		//'idagenda',
                array(
                    'name' =>'fecha',                           
                    'filter'=> $date,
                    'value'=>'$data->fecha',
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    ),
            //hola
                //'idmedico',
                array(
                    'name'=>'idmedico',
                    'value'=>'$data->pkmedico->nombreCompleto',
                    'filter'=> CHtml::listData(Medico::model()->findAll(array('order'=>'nombre')), 'idmedico', 'nombreCompleto'),
                    'htmlOptions'=>array('style'=>'height:40px;width:220px'),
                    //'filter'=>false,
                    //'filter'=>CHtml::activeTextField($model, 'doctor',array('class'=>'form-control')),
                 ),
		//'idpaciente',
            //font face="impact"
                 array(
                    'name'=>'idpaciente',
                    'value'=>'$data->nombreCompleto',
                    'filter'=>false,     
                    'htmlOptions'=>array('style'=>'word-wrap:break-word;height:40px;width:200px'),
//'filter'=>CHtml::activeTextField($model, 'paciente',array('class'=>'form-control')),
                 ),
		//'idtipoagenda',
                array(
                    'name'=>'idtipoagenda',
                    'value'=>'$data->pktipoagenda->tipoagenda',
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    //'filter'=>CHtml::activeTextField($model, 'agenda',array('class'=>'form-control')),
                 ),
		//'idsubcentro',
		//'nombre',
		
		//'numeroturno',
                array(
                    'name'=>'Historial clinico',
                    'value'=>'$data->pkpaciente->historialclinico',
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    ),
                //'horainicio',
               
           //array
                  //ame'=>'fecha1', 
                  //alue'=>'$data->fecha'
            //),
            /*
		'fecha',
		'horainicio',
		'horafin',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
		
	),
)); ?>
<br>
<br>

  
  <label class="checkbox-inline"><input id="che" type="checkbox" value="">Agregar celular/télefono al reporte</label>

<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link('Generar pdf',array("generarPdfDoctorPacAgend"), array('id'=>'btnpdf')); ?>
</div>
<br>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link('Generar Excel',array("generarExcelDoctor")); ?>
</div>

<script type="text/javascript">
    var val;
    if($("#che").is(':checked')) {  
              
        } else {  
            
        }  
 $("#btnpdf").click(function (){
var idmedico =$('#Agenda_idmedico').val();

if($("#che").is(':checked')) {  
        val = 1;
        } else {  
             val = 0;
        }
if(idmedico===""){
    alert("Seleccione doctor");
     var enlace= '';
var link = document.getElementById('btnpdf');
$(link).attr('href',enlace);
}else{
     var enlace= 'index.php?r=/agenda/generarPdfDoctorPacAgend&val='+val;
var link2 = document.getElementById('btnpdf');
$(link2).attr('href',enlace);
$(link2).attr('target','_blank');
}

});
</script>