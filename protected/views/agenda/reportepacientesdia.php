<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'Eventos'=>array('index'),
	'Reportes',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php 
//if(!isset($model->fs)){
 //   $model->fs= date_format(date_create(), 'd-m-Y');
//}
?>
<center><h4>Pacientes en el dia</h4>

<?php 
$this->widget('booster.widgets.TbGridView', array(
	'id'=>'agenda-grid',
	'dataProvider'=>$model->busquedaPacienteDia(),
	'filter'=>$model,
        'htmlOptions'=>array('style'=>'word-wrap:break-word;width:950px;'),
	'columns'=>array(
		//'idagenda',
                array(
                    'name' =>'fechacreado',                           
                    //'filter'=> $date,
                    'value'=>'$data->fechacreado',
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    ),
            array(
                    'name' =>'fecha',   
                    //'filter'=> $date,
                    'value'=>'$data->fecha',
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    ),
            //hola
                //'idmedico',
                array(
                    'name'=>'idmedico',
                    'value'=>'$data->pkmedico->nombreCompleto',
                    'filter'=> false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    //CHtml::listData(Medico::model()->findAll(array('order'=>'nombre')), 'idmedico', 'nombreCompleto')

                    //'filter'=>false,
                    //'filter'=>CHtml::activeTextField($model, 'doctor',array('class'=>'form-control')),
                 ),
		//'idpaciente',
                 array(
                    'name'=>'idpaciente',
                    'value'=>'$data->pkpaciente->nombre',
                    'filter'=>false,    
                     'htmlOptions'=>array('style'=>'height:40px;width:200px'),
//'filter'=>CHtml::activeTextField($model, 'paciente',array('class'=>'form-control')),
                 ),
		//'idtipoagenda',
                array(
                    'name'=>'idtipoagenda',
                    'value'=>'$data->pktipoagenda->tipoagenda',
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    //'filter'=>CHtml::activeTextField($model, 'agenda',array('class'=>'form-control')),
                 ),
		//'idsubcentro',
		//'nombre',
		
		//'numeroturno',
                array(
                    'name'=>'numeroturno',
                    'value'=>'$data->numeroturno',
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    //'filter'=>CHtml::activeTextField($model, 'agenda',array('class'=>'form-control')),
                 ),
            /*
                //'horainicio',
                array(
                    'name'=>'horainicio',
                    'value'=>'$data->horainicio',
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    //'filter'=>CHtml::activeTextField($model, 'agenda',array('class'=>'form-control')),
                 ),
                //'horafin',
                array(
                    'name'=>'horafin',
                    'value'=>'$data->horafin',
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px'),
                    //'filter'=>CHtml::activeTextField($model, 'agenda',array('class'=>'form-control')),
                 ),
           //array
                  //ame'=>'fecha1', 
                  //alue'=>'$data->fecha'
            //),
            
		'fecha',
		'horainicio',
		'horafin',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
		
	),
)); ?>
<br>
<br>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link('Generar PDF',array("pdfpacientedia"), array('target'=>'_blank')); ?>
</div>


