<?php
$this->breadcrumbs=array(
	'Citas'=>array('admin'),
	'Lista',
);

$this->menu=array(
	array('label'=>'Lista de Citas', 'url'=>array('admin')),
	array('label'=>'Crear Cita', 'url'=>array('create')),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Lista de Pacientes Reagendados</h2>

<?php
$model=  Agenda::model();
?>


<div class="form-group">
    <div class="col-sm-5">
        <div class="input-group">
            <center>
        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        <span class="input-group-addon"><?php $this->widget('booster.widgets.TbDatePicker', array('model' => $model,'attribute' =>'fecha' ,'options' => array('format'=>'yyyy/mm/dd','language' => 'es'),'htmlOptions' => array('class'=>'col-sm-4','placeholder'=>'Fecha Inicio','id'=>'combo11'),)); ?></span>
        <span class="input-group-addon"><?php $this->widget('booster.widgets.TbDatePicker', array('model' => $model,'attribute' =>'fecha' ,'options' => array('format'=>'yyyy/mm/dd','language' => 'es'),'htmlOptions' => array('class'=>'col-sm-4','placeholder'=>'Fecha Fin','id'=>'combo22'),)); ?></span></center>
        </div>
    </div>
</div>
<br><br><br>
<center>
    <div class="enlaceboton" style="width: 230px">
<?php echo CHtml::ajaxLink(
    'Ver Pacientes Reagendados',          // the link body (it will NOT be HTML-encoded.)
    array('agenda/listareagendados2'), // the URL for the AJAX request. If empty, it is assumed to be the current URL.
    array(
        'update'=>'#pacientes',
        'data'=> 'js:{"fechainicio2":getfechainicio(),"fechafinal2":getfechafinal()}',
        
        
    )
); ?>

</div>
    </center>
<hr>
<div id="pacientes">
</div>
<hr>

<script type="text/javascript" >
    function getfechainicio(){
        return  $("#combo11").val();
    }
     function getfechafinal(){
        return  $("#combo22").val();
    }
    
    
         $('#combo22').change(function()
{
	var fech1 = $('#combo11').val();
	var fech2 = $('#combo22').val();
	if((Date.parse(fech1)) > (Date.parse(fech2)))
	{
		alert('La fecha inicial no puede ser mayor que la fecha final');
		$('#combo22').val("");
	}
});



 $('#combo22').click(function()
{
    var fecha = $('#combo11').val();
    if(fecha.length < 1)
    {
        alert('Ingrese primero la Fecha Inicio');
        $('#combo22').val("");
    }
});
    
</script>