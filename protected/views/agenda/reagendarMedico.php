<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'evento-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php
/*@var $this AgendaController*/
/*@var $dataProvider CActiveDataProvider*/

$this->breadcrumbs=array(
	'Agendas',
);

$this->menu=array(
	array('label'=>'Crear Agenda', 'url'=>array('create')),
	
);

?>
<h2>Reagendamiento de pacientes</h2>
<br>
<center>
<b>Fecha: &nbsp; &nbsp; </b>
<?php 
 $this->widget(
		    'booster.widgets.TbDatePicker',
		    array('id'=>'date',
		    'name' => 'date',
		    'options' => array(
		    'language' => 'es',
		    'format' => 'dd-mm-yyyy',                       
		    )
		    )
		    );?>
    <br><br>
    <div class="enlaceboton" style="width: 130px">
<?php echo CHtml::ajaxLink(
    'Enviar',          
    array('agenda/obtenerPacientesReagendar'), 
    array(
        'update'=>'#pacientes',
        'data'=> 'js:{"fecha":getfecha(),}'        
    )
); ?>
</div>

</center>
<hr>
<div id="pacientes">
    
</div>
<hr>

<center>
    <b>Fecha a reagendar: &nbsp; &nbsp; </b>
<?php 
      $this->widget(
		    'booster.widgets.TbDatePicker',
		    array( 'id'=>'dateR',
		    'name' => 'dateR',
		    'options' => array(
                        
		    'language' => 'es',
		    'format' => 'dd-mm-yyyy',)));
	  ?>


<br><br>
<div class="enlaceboton" style="width: 250px">
<?php echo CHtml::ajaxLink(
    'Reagendar los pacientes',          // the link body (it will NOT be HTML-encoded.)
    array('agenda/reagendarMed'), // the URL for the AJAX request. If empty, it is assumed to be the current URL.
    array(
        'update'=>'#pacientes',
        'data'=> 'js:{"fecha":getfecha(),"fecha2":getfecha2()}',
        'htmlOptions'=>'class:enlaceboton',
         "success" => "function(result){
                alert(result);
        }",
        
        
    )
); ?>
    </div>
</center>
<?php $this->endWidget(); ?>
<script type="text/javascript" >

    
    function getfecha(){
        return  $("#date").val();
    }
    
    function getfecha2(){
        return  $("#dateR").val();
    }
    
</script>



