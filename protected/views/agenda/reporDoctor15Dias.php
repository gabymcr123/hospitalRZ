

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'Eventos'=>array('index'),
	'Reportes',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php 
//if(!isset($model->fs)){
  //  $model->fs= date_format(date_create(), 'd-m-Y');
//}
?>


<center><h4>Pacientes atendidos en 15 días por profesional de la salud</h4>

<?php 
$date = $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        // 'model'=>$model,
                                    'name' => 'Agenda[fs]',
                                    'value' => $model->fs,
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'dateFormat'=>'dd/mm/yy',
                                        'changeMonth' => 'true',
                                        'yearRange'=>'-80:+3',
                                        'monthNames' => array('Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre'),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                                                    'dayNames' => array('Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado'),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                                    'language'=> Yii::app()->getLanguage(),
   'changeYear'=>'true',
                                        'constrainInput' => 'false',
                                    ),
                                    'htmlOptions'=>array(
                                        'style'=>'height:35px;width:120px;',
                                        'class'=>'span5 form-control',
                                    ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                                ),true);

?>

<?php
$this->widget('booster.widgets.TbGridView', array(
	'id'=>'agenda-grid',
	'dataProvider'=>$model->busqueda5(),
        'afterAjaxUpdate'=>"function() {
                                                jQuery('#Agenda_fs').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'dd-mm-yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));
                                                }",
	'filter'=>$model,
        'htmlOptions'=>array('style'=>'word-wrap:break-word;width:950px;'),
	'columns'=>array(
		//'idagenda',
                array(
                    'name' =>'fecha',                           
                    'filter'=> $date,
                    'value'=>'$data->fecha',
                    'htmlOptions'=>array('style'=>'height:40px;width:200px')
                    ),
            //hola
                //'idmedico',
                       
                array(
                    'name'=>'idmedico',
                    'value'=>'$data->pkmedico->nombreCompleto',
                    'filter'=> CHtml::listData(Medico::model()->findAll(array('order'=>'nombre')), 'idmedico', 'nombreCompleto'),
                    'htmlOptions'=>array('style'=>'height:40px;width:220px'),
                    //'filter'=>false,
                    //'filter'=>CHtml::activeTextField($model, 'doctor',array('class'=>'form-control')),
                 ),
		//'idpaciente',
                 array(
                    'name'=>'idpaciente',
                    'value'=>'$data->nombreCompleto',
                    'filter'=>false,   
                     'htmlOptions'=>array('style'=>'height:40px;width:220px'),
//'filter'=>CHtml::activeTextField($model, 'paciente',array('class'=>'form-control')),
                 ),
		//'idtipoagenda',
                array(
                    'name'=>'idtipoagenda',
                    'value'=>'$data->pktipoagenda->tipoagenda',
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px')
                    //'filter'=>CHtml::activeTextField($model, 'agenda',array('class'=>'form-control')),
                 ),
		//'idsubcentro',
		//'nombre',
		
		//'numeroturno',
            /*    array(
                    'name'=>'numeroturno',
                    'value'=>'$data->numeroturno',
                    'filter'=>false,
                    'htmlOptions'=>array('style'=>'height:40px;width:200px')
                    //'filter'=>CHtml::activeTextField($model, 'agenda',array('class'=>'form-control')),
                 ),
                //'horainicio',
           //array
                  //ame'=>'fecha1', 
                  //alue'=>'$data->fecha'
            //),
            
		'fecha',
		'horainicio',
		'horafin',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
		
	),
)); ?>
<br>
<br>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl."/images/pdf.jpg","Generar reporte PDF",array("title"=>"Exportar a PDF")),array("generarpdf5"), array('target'=>'_blank')); ?>
</div>
<br>
<div class="enlaceboton" style="width: 180px">
<?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl."/images/excel.jpg","Generar reporte EXCEL",array("title"=>"Exportar a EXCEL")),array("generarExcelDoctorPaci15Dias")); ?>
</div>
