<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'especialidadmedica-form',
'enableClientValidation'=>true,
'clientOptions' => array(
'validateOnSubmit' => true,)
)); ?>

<p class="help-block">Los campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model); ?>
        <?php $datos = CHtml::listData(Serviciomedico::model()->findAll(),'idserviciomedico','serviciomedico'); ?>
	<?php echo $form->dropDownListGroup($model,'idserviciomedico',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $datos))); ?>

	<?php echo $form->textFieldGroup($model,'especialidad',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>
<?php echo $form->textFieldGroup($model,'intervalo',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>
	

	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
