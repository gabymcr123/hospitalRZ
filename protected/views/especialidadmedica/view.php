<?php
$this->breadcrumbs=array(
	'Especialidad Medica'=>array('admin'),
	//$model->idespecialidad,
);

$this->menu=array(
array('label'=>'Lista de Especialidad Médica','url'=>array('admin')),
array('label'=>'Crear Especialidad Médica','url'=>array('create')),
array('label'=>'Actualizar Especialidad Médica','url'=>array('update','id'=>$model->idespecialidad)),
array('label'=>'Eliminar Especialidad Mèdica','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idespecialidad),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Administrar Especialidad Mèdica','url'=>array('admin')),
);
?>

<h2>Especialidad </h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'idespecialidad',
                array(
			'label'=>'Servcio Médico',
			'type'=>'raw',
			'value'=>$model->idserviciomedico1->serviciomedico),
		'especialidad',
		
),
)); ?>
