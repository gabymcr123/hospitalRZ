<?php
$this->breadcrumbs=array(
	'Especialidad Médica'=>array('admin'),
	//$model->idespecialidad=>array('view','id'=>$model->idespecialidad),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Especialidad Médica','url'=>array('admin')),
	array('label'=>'Crear Especialidad Médica','url'=>array('create')),
	array('label'=>'Detalles de Especialidad Médica','url'=>array('view','id'=>$model->idespecialidad)),
	//array('label'=>'Administrar Especialidades Médicas','url'=>array('admin')),
	);
	?>

	<h1>Modificar Especialidad Médica <?php echo $model->especialidad; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>