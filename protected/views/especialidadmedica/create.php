<?php
$this->breadcrumbs=array(
	'Especialidad Médica'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Especialidad Médica','url'=>array('admin')),
//array('label'=>'Manage Especialidadmedica','url'=>array('admin')),
);
?>

<h1>Crear Especialidad Médica</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>