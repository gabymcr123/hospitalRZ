<?php
$this->breadcrumbs=array(
	'Especialidadmedicas',
);

$this->menu=array(
array('label'=>'Crear Especialidad Médica','url'=>array('create')),
array('label'=>'Administrar Especialidad Médica','url'=>array('admin')),
);
?>

<h1>Especialidades Médicas</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
