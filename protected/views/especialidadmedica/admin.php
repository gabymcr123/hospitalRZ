<?php
$this->breadcrumbs=array(
	'Especialidad Médica'=>array('admin'),
	'Lista',
);

$this->menu=array(
//array('label'=>'Lista de Especialidades Medicas','url'=>array('index')),
array('label'=>'Crear Especialidad Médica','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('especialidadmedica-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Especialidad Médica</h2>


<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'especialidadmedica-grid',
'rowCssClassExpression'=>'!$data->estado ? "danger":""',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		/*'idespecialidad',*/
		//'idserviciomedico',
                array(
		   'name'=>'idserviciomedico',
		   'value'=>'$data->idserviciomedico1->serviciomedico',
                   'filter'=> CHtml::listData(Serviciomedico::model()->findAll('estado=true'),'idserviciomedico','serviciomedico'), 
		   //'filter'=>CHtml::activeTextField($model, 'serviciomedico', array('class'=>'form-control')),
		   
		),
		'especialidad',
		'intervalo',
		/*'estado',
		'creadopor',
		
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
    array(
    'class' => 'booster.widgets.TbToggleColumn',
    'toggleAction' => 'especialidadmedica/delete',
    'name' => 'estado',
    //'header' => 'Toggle',
    //'value'=>'$data->estado ? 1 : 0',
    'filter'=>'',
    ),
array(
'class'=>'booster.widgets.TbButtonColumn',
    'template'=>'{update}'
),
),
)); ?>
