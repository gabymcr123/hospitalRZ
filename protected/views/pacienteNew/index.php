<?php
/* @var $this PacienteNewController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Paciente News',
);

$this->menu=array(
	array('label'=>'Create PacienteNew', 'url'=>array('create')),
	array('label'=>'Manage PacienteNew', 'url'=>array('admin')),
);
?>

<h1>Paciente News</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
