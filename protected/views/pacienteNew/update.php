<?php
/* @var $this PacienteNewController */
/* @var $model PacienteNew */

$this->breadcrumbs=array(
	'Paciente News'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PacienteNew', 'url'=>array('index')),
	array('label'=>'Create PacienteNew', 'url'=>array('create')),
	array('label'=>'View PacienteNew', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PacienteNew', 'url'=>array('admin')),
);
?>

<h1>Update PacienteNew <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>