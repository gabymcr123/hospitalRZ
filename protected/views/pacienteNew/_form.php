<?php
/* @var $this PacienteNewController */
/* @var $model PacienteNew */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'paciente-new-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'numero_archivo'); ?>
		<?php echo $form->textField($model,'numero_archivo'); ?>
		<?php echo $form->error($model,'numero_archivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cedula'); ?>
		<?php echo $form->textField($model,'cedula',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'cedula'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'primer_apellido'); ?>
		<?php echo $form->textField($model,'primer_apellido',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'primer_apellido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'segundo_apellido'); ?>
		<?php echo $form->textField($model,'segundo_apellido',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'segundo_apellido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'primer_nombre'); ?>
		<?php echo $form->textField($model,'primer_nombre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'primer_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'segundo_nombre'); ?>
		<?php echo $form->textField($model,'segundo_nombre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'segundo_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'historia_clinica_temporal'); ?>
		<?php echo $form->textField($model,'historia_clinica_temporal',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'historia_clinica_temporal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre_padre'); ?>
		<?php echo $form->textField($model,'nombre_padre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombre_padre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellido_padre'); ?>
		<?php echo $form->textField($model,'apellido_padre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'apellido_padre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre_madre'); ?>
		<?php echo $form->textField($model,'nombre_madre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombre_madre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellido_madre'); ?>
		<?php echo $form->textField($model,'apellido_madre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'apellido_madre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
		<?php echo $form->textField($model,'fecha_nacimiento'); ?>
		<?php echo $form->error($model,'fecha_nacimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lugar_nacimiento'); ?>
		<?php echo $form->textField($model,'lugar_nacimiento',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'lugar_nacimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nacionalidad'); ?>
		<?php echo $form->textField($model,'nacionalidad',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nacionalidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sexo'); ?>
		<?php echo $form->textField($model,'sexo'); ?>
		<?php echo $form->error($model,'sexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grupo_cultural'); ?>
		<?php echo $form->textField($model,'grupo_cultural'); ?>
		<?php echo $form->error($model,'grupo_cultural'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_civil'); ?>
		<?php echo $form->textField($model,'estado_civil'); ?>
		<?php echo $form->error($model,'estado_civil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'celular'); ?>
		<?php echo $form->textField($model,'celular',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'celular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'discapacitado'); ?>
		<?php echo $form->checkBox($model,'discapacitado'); ?>
		<?php echo $form->error($model,'discapacitado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'discapacidad_predomina'); ?>
		<?php echo $form->textField($model,'discapacidad_predomina',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'discapacidad_predomina'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'instruccion'); ?>
		<?php echo $form->textField($model,'instruccion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'instruccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ocupacion'); ?>
		<?php echo $form->textField($model,'ocupacion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'ocupacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'empresa'); ?>
		<?php echo $form->textField($model,'empresa',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'empresa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_seguro'); ?>
		<?php echo $form->textField($model,'tipo_seguro'); ?>
		<?php echo $form->error($model,'tipo_seguro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_admision'); ?>
		<?php echo $form->textField($model,'fecha_admision'); ?>
		<?php echo $form->error($model,'fecha_admision'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'provincia'); ?>
		<?php echo $form->textField($model,'provincia',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'provincia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'canton'); ?>
		<?php echo $form->textField($model,'canton',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'canton'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parroquia'); ?>
		<?php echo $form->textField($model,'parroquia',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'parroquia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'barrio'); ?>
		<?php echo $form->textField($model,'barrio',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'barrio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zona'); ?>
		<?php echo $form->textField($model,'zona',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'zona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion2'); ?>
		<?php echo $form->textField($model,'direccion2',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'direccion2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'emergencia_llamar'); ?>
		<?php echo $form->textField($model,'emergencia_llamar',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'emergencia_llamar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre_refenrecia'); ?>
		<?php echo $form->textField($model,'nombre_refenrecia',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombre_refenrecia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parentesco'); ?>
		<?php echo $form->textField($model,'parentesco',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'parentesco'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion_parentesco'); ?>
		<?php echo $form->textField($model,'direccion_parentesco',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'direccion_parentesco'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_parentesco'); ?>
		<?php echo $form->textField($model,'telefono_parentesco',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'telefono_parentesco'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_creador'); ?>
		<?php echo $form->textField($model,'usuario_creador',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'usuario_creador'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_modificador'); ?>
		<?php echo $form->textField($model,'usuario_modificador',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'usuario_modificador'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_creado'); ?>
		<?php echo $form->textField($model,'fecha_creado'); ?>
		<?php echo $form->error($model,'fecha_creado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_modificado'); ?>
		<?php echo $form->textField($model,'fecha_modificado'); ?>
		<?php echo $form->error($model,'fecha_modificado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->textField($model,'estado'); ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nmod'); ?>
		<?php echo $form->textField($model,'nmod'); ?>
		<?php echo $form->error($model,'nmod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'calta'); ?>
		<?php echo $form->checkBox($model,'calta'); ?>
		<?php echo $form->error($model,'calta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date02'); ?>
		<?php echo $form->textField($model,'date02'); ?>
		<?php echo $form->error($model,'date02'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'res01'); ?>
		<?php echo $form->textField($model,'res01',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'res01'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prm_01'); ?>
		<?php echo $form->textField($model,'prm_01',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'prm_01'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'na'); ?>
		<?php echo $form->textField($model,'na'); ?>
		<?php echo $form->error($model,'na'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->