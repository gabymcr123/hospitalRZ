<?php
/* @var $this PacienteNewController */
/* @var $model PacienteNew */

$this->breadcrumbs=array(
	'Paciente News'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PacienteNew', 'url'=>array('index')),
	array('label'=>'Create PacienteNew', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#paciente-new-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Paciente News</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'paciente-new-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'numero_archivo',
		'cedula',
		'primer_apellido',
		'segundo_apellido',
		'primer_nombre',
		/*
		'segundo_nombre',
		'historia_clinica_temporal',
		'nombre_padre',
		'apellido_padre',
		'nombre_madre',
		'apellido_madre',
		'fecha_nacimiento',
		'lugar_nacimiento',
		'nacionalidad',
		'sexo',
		'grupo_cultural',
		'estado_civil',
		'telefono',
		'celular',
		'discapacitado',
		'discapacidad_predomina',
		'instruccion',
		'ocupacion',
		'empresa',
		'tipo_seguro',
		'fecha_admision',
		'provincia',
		'canton',
		'parroquia',
		'barrio',
		'zona',
		'direccion',
		'direccion2',
		'emergencia_llamar',
		'nombre_refenrecia',
		'parentesco',
		'direccion_parentesco',
		'telefono_parentesco',
		'usuario_creador',
		'usuario_modificador',
		'fecha_creado',
		'fecha_modificado',
		'estado',
		'nmod',
		'calta',
		'date02',
		'res01',
		'prm_01',
		'na',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
