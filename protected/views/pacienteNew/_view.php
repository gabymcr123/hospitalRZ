<?php
/* @var $this PacienteNewController */
/* @var $data PacienteNew */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero_archivo')); ?>:</b>
	<?php echo CHtml::encode($data->numero_archivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula')); ?>:</b>
	<?php echo CHtml::encode($data->cedula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primer_apellido')); ?>:</b>
	<?php echo CHtml::encode($data->primer_apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('segundo_apellido')); ?>:</b>
	<?php echo CHtml::encode($data->segundo_apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primer_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->primer_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('segundo_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->segundo_nombre); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('historia_clinica_temporal')); ?>:</b>
	<?php echo CHtml::encode($data->historia_clinica_temporal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_padre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_padre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido_padre')); ?>:</b>
	<?php echo CHtml::encode($data->apellido_padre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_madre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_madre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido_madre')); ?>:</b>
	<?php echo CHtml::encode($data->apellido_madre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_nacimiento')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_nacimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lugar_nacimiento')); ?>:</b>
	<?php echo CHtml::encode($data->lugar_nacimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nacionalidad')); ?>:</b>
	<?php echo CHtml::encode($data->nacionalidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sexo')); ?>:</b>
	<?php echo CHtml::encode($data->sexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grupo_cultural')); ?>:</b>
	<?php echo CHtml::encode($data->grupo_cultural); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_civil')); ?>:</b>
	<?php echo CHtml::encode($data->estado_civil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular')); ?>:</b>
	<?php echo CHtml::encode($data->celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discapacitado')); ?>:</b>
	<?php echo CHtml::encode($data->discapacitado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discapacidad_predomina')); ?>:</b>
	<?php echo CHtml::encode($data->discapacidad_predomina); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instruccion')); ?>:</b>
	<?php echo CHtml::encode($data->instruccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ocupacion')); ?>:</b>
	<?php echo CHtml::encode($data->ocupacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('empresa')); ?>:</b>
	<?php echo CHtml::encode($data->empresa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_seguro')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_seguro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_admision')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_admision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('provincia')); ?>:</b>
	<?php echo CHtml::encode($data->provincia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('canton')); ?>:</b>
	<?php echo CHtml::encode($data->canton); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parroquia')); ?>:</b>
	<?php echo CHtml::encode($data->parroquia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('barrio')); ?>:</b>
	<?php echo CHtml::encode($data->barrio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zona')); ?>:</b>
	<?php echo CHtml::encode($data->zona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion2')); ?>:</b>
	<?php echo CHtml::encode($data->direccion2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emergencia_llamar')); ?>:</b>
	<?php echo CHtml::encode($data->emergencia_llamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_refenrecia')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_refenrecia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parentesco')); ?>:</b>
	<?php echo CHtml::encode($data->parentesco); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion_parentesco')); ?>:</b>
	<?php echo CHtml::encode($data->direccion_parentesco); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_parentesco')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_parentesco); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_creador')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_creador); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_modificador')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_modificador); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_creado')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_creado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_modificado')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_modificado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nmod')); ?>:</b>
	<?php echo CHtml::encode($data->nmod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('calta')); ?>:</b>
	<?php echo CHtml::encode($data->calta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date02')); ?>:</b>
	<?php echo CHtml::encode($data->date02); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('res01')); ?>:</b>
	<?php echo CHtml::encode($data->res01); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prm_01')); ?>:</b>
	<?php echo CHtml::encode($data->prm_01); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('na')); ?>:</b>
	<?php echo CHtml::encode($data->na); ?>
	<br />

	*/ ?>

</div>