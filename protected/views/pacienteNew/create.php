<?php
/* @var $this PacienteNewController */
/* @var $model PacienteNew */

$this->breadcrumbs=array(
	'Paciente News'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PacienteNew', 'url'=>array('index')),
	array('label'=>'Manage PacienteNew', 'url'=>array('admin')),
);
?>

<h1>Create PacienteNew</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>