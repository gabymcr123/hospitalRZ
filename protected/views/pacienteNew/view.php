<?php
/* @var $this PacienteNewController */
/* @var $model PacienteNew */

$this->breadcrumbs=array(
	'Paciente News'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PacienteNew', 'url'=>array('index')),
	array('label'=>'Create PacienteNew', 'url'=>array('create')),
	array('label'=>'Update PacienteNew', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PacienteNew', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PacienteNew', 'url'=>array('admin')),
);
?>

<h1>View PacienteNew #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'numero_archivo',
		'cedula',
		'primer_apellido',
		'segundo_apellido',
		'primer_nombre',
		'segundo_nombre',
		'historia_clinica_temporal',
		'nombre_padre',
		'apellido_padre',
		'nombre_madre',
		'apellido_madre',
		'fecha_nacimiento',
		'lugar_nacimiento',
		'nacionalidad',
		'sexo',
		'grupo_cultural',
		'estado_civil',
		'telefono',
		'celular',
		'discapacitado',
		'discapacidad_predomina',
		'instruccion',
		'ocupacion',
		'empresa',
		'tipo_seguro',
		'fecha_admision',
		'provincia',
		'canton',
		'parroquia',
		'barrio',
		'zona',
		'direccion',
		'direccion2',
		'emergencia_llamar',
		'nombre_refenrecia',
		'parentesco',
		'direccion_parentesco',
		'telefono_parentesco',
		'usuario_creador',
		'usuario_modificador',
		'fecha_creado',
		'fecha_modificado',
		'estado',
		'nmod',
		'calta',
		'date02',
		'res01',
		'prm_01',
		'na',
	),
)); ?>
