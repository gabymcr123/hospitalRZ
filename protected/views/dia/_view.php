<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('iddia')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->iddia),array('view','id'=>$data->iddia)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idmedico')); ?>:</b>
	<?php echo CHtml::encode($data->idmedico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numhora')); ?>:</b>
	<?php echo CHtml::encode($data->numhora); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dia')); ?>:</b>
	<?php echo CHtml::encode($data->dia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creadopor')); ?>:</b>
	<?php echo CHtml::encode($data->creadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modificadopor')); ?>:</b>
	<?php echo CHtml::encode($data->modificadopor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacreado')); ?>:</b>
	<?php echo CHtml::encode($data->fechacreado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechamodificado')); ?>:</b>
	<?php echo CHtml::encode($data->fechamodificado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('horainicio')); ?>:</b>
	<?php echo CHtml::encode($data->horainicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('horafin')); ?>:</b>
	<?php echo CHtml::encode($data->horafin); ?>
	<br />

	*/ ?>

</div>