<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'dia-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model_1); ?>
<?php echo $form->errorSummary($model2); ?>
<?php echo $form->errorSummary($model3); ?>
<?php echo $form->errorSummary($model4); ?>
<?php echo $form->errorSummary($model5); ?>
<!-- DOCTOR-->

<?php $datos = CHtml::listData(Medico::model()->findAll('idmedico='.$idmedico),'idmedico','nombreCompleto'); ?>
                    <?php echo $form->dropDownListGroup($model_1,'idmedico',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $datos))); ?>

<!--Horario-->	
						
<fieldset>							
<legend>Horario</legend>							
<div class="well">
<?php 

echo CHtml::activeCheckBox($model_1, '[1]estado', array('onclick'=>'ActivarCampoOtroTema();','id'=>"dia1")); echo "<strong>Lunes</strong>";
?>
<br>

<div id="horLunes"style="display:none; margin: 10px 0px;">
<!-- LUNES -->
<?php echo $form->hiddenField($model_1,'[1]dia','',array('size'=>10,'maxlength'=>8,'class'=>'form-control')); ?>
<table><tr><td>
<label>Hora Inicio</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horainicio',
    
            'model'=>$model_1,
            'name'=>'horainicio',
        'attribute'=>'1][horainicio',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,
            ),
        ));?>
</td><td>
<label>Hora Fin</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horafin',
          
            'model'=>$model_1,
            'name'=>'horafin',
     'attribute'=>'1][horafin',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,
            ),
        ));?>
</td></tr>
</table>
</div>	
</div>
<div class="well">
<?php 
echo CHtml::activeCheckBox($model2, '[2]estado', array('onclick'=>'ActivarCampoOtroTema1();','id'=>"dia2")); echo "<strong>Martes</strong>";
?>
<br>
<div	id="horMartes"	style="display:none;"	>				
	<!-- MARTES -->

<?php echo $form->hiddenField($model2,'[2]dia','',array('size'=>10,'maxlength'=>8,'class'=>'form-control')); ?>
        <table><tr><td>
        <label>Hora Inicio</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horainicio2',
            'model'=>$model2,
            'name'=>'horainicio',
        'attribute'=>'2][horainicio',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,'style'=>'margin:20px 0px;'
            ),
    
    ));?></td><td>
        <label>Hora Fin</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horafin2',
            'model'=>$model2,
     'attribute'=>'2][horafin',
            'name'=>'horafin',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,
            ),
        ));?>
        </td></tr>
</table>
</div>
</div>	
<div class="well">
<?php 
echo CHtml::activeCheckBox($model3, '[3]estado', array('onclick'=>'ActivarCampoOtroTema2();','id'=>"dia3")); echo "<strong>Miercoles</strong>";
?>
<br>	
<div	id="horMiercoles"	style="display:none;"	>				
	<!-- MIERCOLES -->

<?php echo $form->hiddenField($model3,'[3]dia','',array('size'=>10,'maxlength'=>8,'class'=>'form-control')); ?>
        <table><tr><td>
        <label>Hora Inicio</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horainicio3',
            'model'=>$model3,
            'name'=>'horainicio',
        'attribute'=>'3][horainicio',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,'style'=>'margin:20px 0px;'
            ),
    
    ));?></td><td>
        <label>Hora Fin</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horafin3',
            'model'=>$model3,
     'attribute'=>'3][horafin',
            'name'=>'horafin',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,
            ),
        ));?>	
        </td></tr></table>
</div>	
</div>
<div class="well">
<?php 
echo CHtml::activeCheckBox($model4, '[4]estado', array('onclick'=>'ActivarCampoOtroTema3();','id'=>"dia4")); echo "<strong>Jueves</strong>";
?>
<br>
<div	id="horJueves"	style="display:none;">					
	<!-- JUEVES -->

<?php echo $form->hiddenField($model4,'[4]dia','',array('size'=>10,'maxlength'=>8,'class'=>'form-control')); ?>
        <table><tr><td>
                    <label>Hora Inicio</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horainicio4',
            'model'=>$model4,
    'attribute'=>'4][horainicio',
            'name'=>'horainicio',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,'style'=>'margin:20px 0px;'
            ),
    
        ));?>
            </td><td>
        <label>Hora Fin</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horafin4',
            'model'=>$model4,
     'attribute'=>'4][horafin',
            'name'=>'horafin',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,
            ),
        ));?>
            <td></tr></table>
</div>	
</div>	

<div class="well">
<?php 
echo CHtml::activeCheckBox($model5, '[5]estado', array('onclick'=>'ActivarCampoOtroTema4();','id'=>"dia5")); echo "<strong>Viernes</strong>";
?>
<br>	
<div	id="horViernes"	style="display:none;"	>				
<!-- VIERNES -->

<?php echo $form->hiddenField($model5,'[5]dia','',array('size'=>10,'maxlength'=>8,'class'=>'form-control')); ?>
<table><tr><td>
<label>Hora Inicio</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horainicio5',
            'model'=>$model5,
            'name'=>'horainicio',
        'attribute'=>'5][horainicio',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,'style'=>'margin:20px 0px;'
            ),
    
    ));?></td><td>
        <label>Hora Fin</label><br>
<?php $this->widget('ext.clockpick.ClockPick', array(
            'id'=>'horafin5',
            'model'=>$model5,
     'attribute'=>'5][horafin',
            'name'=>'horafin',
            'options'=>array(
                'starthour'=>7,
                'endhour'=>21,
                'event'=>'click',
                'showminutes'=>true,
                'minutedivisions'=>2,
                'military' =>false,
                'layout'=>'horizontal',
                'hoursopacity'=>1,
                'minutesopacity'=>1,
            ),
        ));?>
    </td></tr></table>
</div>							
</div>
</fieldset>	
<br>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model_1->isNewRecord ? 'Create' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
 function ActivarCampoOtroTema(){
       var contenedor = document.getElementById("horLunes");
        if(dia1.checked==true){
          
        contenedor.style.display = "block";
        return true;
        }else{
            
        contenedor.style.display = "none";
        return false;
        }
        
        
    }
    
     function ActivarCampoOtroTema1(){
       var contenedor = document.getElementById("horMartes");
        if(dia2.checked==true){
          
        contenedor.style.display = "block";
        return true;
        }else{
            
        contenedor.style.display = "none";
        return false;
        }
        
        
    }
    
     function ActivarCampoOtroTema2(){
       var contenedor = document.getElementById("horMiercoles");
        if(dia3.checked==true){
          
        contenedor.style.display = "block";
        return true;
        }else{
            
        contenedor.style.display = "none";
        return false;
        }
        
        
    }
    
     function ActivarCampoOtroTema3(){
       var contenedor = document.getElementById("horJueves");
        if(dia4.checked==true){
          
        contenedor.style.display = "block";
        return true;
        }else{
            
        contenedor.style.display = "none";
        return false;
        }
        
        
    }
    
     function ActivarCampoOtroTema4(){
       var contenedor = document.getElementById("horViernes");
        if(dia5.checked==true){
          
        contenedor.style.display = "block";
        return true;
        }else{
            
        contenedor.style.display = "none";
        return false;
        }
        
        
    }
    ActivarCampoOtroTema();
    ActivarCampoOtroTema1();
    ActivarCampoOtroTema2();
    ActivarCampoOtroTema3();
    ActivarCampoOtroTema4();
    ActivarCampoOtroTema5();
    
</script>