<?php
$this->breadcrumbs=array(
	'Dias'=>array('index'),
	$model->iddia,
);

$this->menu=array(
array('label'=>'List Dia','url'=>array('index')),
array('label'=>'Create Dia','url'=>array('create')),
array('label'=>'Update Dia','url'=>array('update','id'=>$model->iddia)),
array('label'=>'Delete Dia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->iddia),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Dia','url'=>array('admin')),
);
?>

<h1>View Dia #<?php echo $model->iddia; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'iddia',
		'idmedico',
		'numhora',
		'dia',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		'horainicio',
		'horafin',
),
)); ?>


Horario
