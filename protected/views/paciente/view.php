<?php
$this->breadcrumbs=array(
	'Pacientes'=>array('admin'),
	//$model->idpaciente,
);

$this->menu=array(
//array('label'=>'Lista Paciente','url'=>array('index')),
array('label'=>'Crear Paciente','url'=>array('create')),
//array('label'=>'Actualizar Paciente','url'=>array('update','id'=>$model->idpaciente)),
//array('label'=>'Borrar Paciente','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idpaciente),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Lista Paciente','url'=>array('admin')),
);
?>

<h2>Paciente </h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		
		'nombre',
		'apellido',
		'cedula',
		'telefono',
                'celular',
		'historialclinico',
		
),
)); ?>
