<?php
$this->breadcrumbs=array(
	'Pacientes',
);

$this->menu=array(
array('label'=>'Crear paciente','url'=>array('create')),
array('label'=>'Administrar Paciente','url'=>array('admin')),
);
?>

<h1>Pacientes</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
