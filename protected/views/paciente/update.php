<?php
$this->breadcrumbs=array(
	'Pacientes'=>array('admin'),
	//$model->idpaciente=>array('view','id'=>$model->idpaciente),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista Paciente','url'=>array('admin')),
	array('label'=>'Crear Paciente','url'=>array('create')),
	array('label'=>'Detalles de Paciente','url'=>array('view','id'=>$model->idpaciente)),
	//array('label'=>'Administrar Paciente','url'=>array('admin')),
	);
	?>

	<h2>Actualizar Paciente </h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>