<?php
$this->breadcrumbs=array(
	'Pacientes'=>array('admin'),
	'Lista',
);

$this->menu=array(
//array('label'=>'Lista de pacientes','url'=>array('index')),
array('label'=>'Crear paciente','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('paciente-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Pacientes</h2>




<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'paciente-grid',
    'rowCssClassExpression'=>'!$data->estado ? "danger":""',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
 'htmlOptions'=>array('style'=>'font-size: 13px'),
'columns'=>array(
		'historialclinico',
		
    array('name'=>'nombre',
        'value'=>'$data->apellido." ".$data->nombre'
        ),
                
		'cedula',
		//'telefono',
		//'celular',
		/*
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		'estado',
		*/
array(
    'class'=>'booster.widgets.TbButtonColumn',
    'template'=>'{view} {update} {delete}',
    'buttons'=>array(
        'update'=>array('visible'=>'Yii::app()->user->idrol == 3 || Yii::app()->user->idrol== 1',
            ),
        'delete'=>array('visible'=>'Yii::app()->user->idrol == 3',
            )
    ),
    
),
    array(
        'class'=>'CLinkColumn',
		'header'=>'',
		'imageUrl'=>'images/refresh.png',
		'urlExpression'=>'Yii::app()->createUrl("Agenda/reagendarpaciente",array())',
        //'urlExpression'=>'Yii::app()->createUrl("Agenda/reagendarpaciente",array("id"=>$data->ID))',
        'linkHtmlOptions'=>array('title'=>'Reagendar'),  
        //'linkHtmlOptions'=>array('target'=>'_blank')),  
		),
),
)); ?>