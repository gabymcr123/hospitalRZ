<?php
$this->breadcrumbs=array(
	'Pacientes'=>array('admin'),
	'Crear',
);

$this->menu=array(
//array('label'=>'Lista Paciente','url'=>array('index')),
array('label'=>'Lista Paciente','url'=>array('admin')),
);
?>

<h2>Nuevo Paciente</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>