<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language;?>">
<meta charset="utf-8">
<head>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta charset="<?php echo Yii::app()->charset;?>">
 
<link href="css/main.css" type="text/css" rel="stylesheet">
</head>
<body style="background: url('images/ecuador.png') top right no-repeat;">
<header>
<?php
    $admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 3) ? true : false ;
    $medico = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 2) ? true : false ;
    $agendador=(isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
    $info=(isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 4) ? true : false ;
    $this->widget(
    'booster.widgets.TbNavbar',
    array(
    //'type' => 'inverse',
    'brand' => '',
    'brandUrl' => '#',
    'collapse' => true, // requires bootstrap-responsive.css
    'fixed' => false,
    'fluid' => true,
    'items' => array(
    array(
    'class' => 'booster.widgets.TbMenu',
    'type' => 'navbar',
    'items' => array(
    array('label' => 'Inicio', 'url' =>array('/site/index'), ),
    array('label' => 'Agendar',  'url'=>array('/agenda/create') ,'visible'=>$agendador ),
    
    array('label' => 'Citas',  'url'=>array('/agenda/admin'),'visible'=>$agendador || $info ),
    array('label' => 'Citas de hoy',  'url'=>array('agenda2/admin'),'visible'=>$medico),
        array('label' => $info ? 'Horario' :'Agendar',  'url'=>array('/agenda/create') ,'visible'=>$medico || $info  ),
            array(
    'label' => 'Profesional de la salud',
    'url' => '#',
    'visible' => $admin,
    'items' => array(
    array('label' => 'Crear Profesional de la salud', 'url' => array('/medico/create')),
    //array('label' => 'Lista de Médicos', 'url' => array('/medico/index')),
    array('label' => 'Lista Profesional de la salud', 'url' =>array('/medico/admin')),
        array('label' => 'Turnos Extras', 'url' =>array('/agendaextra/create')),
     '---',
    array('label' => 'Crear Especialidad Médica', 'url' => array('/especialidadmedica/create')),
    array('label' => 'Crear Servicio Medico', 'url' =>array('/serviciomedico/create')),
    
        '---',
    array('label' => 'Crear Eventos', 'url' => array('/evento/create')),
    array('label' => 'Concepto', 'url' => array('/concepto/admin')),
    array('label' => 'Permisos', 'url' => array('/permisos/admin')),
        
    )
    ),
      array('label' => 'Pacientes', 'url' => array('/paciente/admin'),'visible'=>!$medico and $agendador),  
        
        //array('label' => 'Permisos', 'url' => array('/permisos/admin'),'visible'=>$medico ),
        
        //array('label' => 'Lista Reagendados',  'url'=>array('/agenda/listareagendar2'),'visible'=>$medico),
      
        array(
    'label' => 'Usuarios',
    'url' => '#',
    'visible' => $admin,
    'items' => array(
    
        array('label' => 'Crear usuario', 'url' => array('/usuario/create')),
        array('label' => 'Lista de usuarios', 'url' => array('/usuario/admin')),
        array('label' => 'Límite de  fecha de agendamiento', 'url' => array('/usuario/admin_agendador')),
        //array('label' => 'Administrar usuarios', 'url' => array('/usuario/admin')),
   
    )
    ),
 
    array('label' => 'Reportes', 'url' => array('#'),'visible'=>$admin,
             'items' => array(
        array('label' => 'Reporte15dias', 'url' => array('/agenda/indicadoresGPR')),         
        array('label' => 'Pacientes agendados', 'url' => array('/agenda/reportePacientesAgendados')),
        array('label' => 'Pacientes agendados por doctor', 'url' => array('/agenda/reporteDoctorPacAgend')),
        array('label' => 'Pacientes por tipo agenda', 'url' => array('/agenda/reporte2')),
        array('label' => 'Pacientes agendados en el día', 'url' => array('/agenda/pacientesdeldia')),
        array('label' => 'Pacientes atendidos en el período de 15 días', 'url' => array('/agenda/reporte4')),
        array('label' => 'Pacientes atendidos en el período de 15 días por profesinal de la salud', 'url' => array('/agenda/reporte5')),
        array('label' => 'Cantidad de pacientes por profesional de la salud', 'url' => array('/agenda/especialidad')),
   )),
        
           array(
    'label' => 'Otros',
    'url' => '#',
    'visible' => $admin,
    'items' => array(
        array('label' => 'Tipo Agenda', 'url' => array('/tipoagenda/admin')),
    array('label' => 'Centro de Salud', 'url' => array('/subcentro/admin')),
   
    )
    ), 
    
   
        array('label' => 'Turnos Extras', 'url' => array('/agendaextra/create'),'visible'=>$medico ),
        
        array('label' => 'Últimas citas agendadas por mi',  'url'=>array('/agenda/adminmedico'),'visible'=>$medico ),
        array('label' => 'Datos de Usuario',  'url'=>array('/usuario/update_1','id'=>Yii::app()->user->id),'visible'=>$medico or $agendador or $admin),
    ),
    ),
    //'<form class="navbar-form navbar-left" action=""><div class="form-group"><input type="text" class="form-control" placeholder="Search"></div></form>',
    array(
    'class' => 'booster.widgets.TbMenu',
    'type' => 'navbar',
    'htmlOptions' => array('class' => 'pull-right'),
    'items' => array(
    array('label' => 'Iniciar Sesión', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
        array('label'=>'Cerrar Sesión ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
    '---',
    ),
    ),
    ),
    )
    );
?>
</header>
<div class="container" id="main" >
 <?php if(isset($this->breadcrumbs)):?>
 <?php $this->widget('booster.widgets.TbBreadcrumbs', array(
 'links'=>$this->breadcrumbs,
 )); ?>
 <?php endif?>
 <?php echo $content; ?>
 <hr>
 
<footer>
 Copyright &copy; <?php echo date('Y'); ?> <?php //echo CHtml::encode(Yii::app()->params['empresa']); ?> | <?php echo CHtml::encode((Yii::app()->name).' '.Yii::app()->params['version']); ?> - All Rights Reserved.<br/>
 <?php //echo Yii::powered(); ?>
 </footer>
 
</div>
</body>
</html>
