<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'porcentajes-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Campos con <span class="required">*</span> son obligatorios.</p>
<br>
<?php echo $form->errorSummary($model); ?>

	<b><?php echo CHtml::encode($model->getAttributeLabel('idenmedico')); ?>:</b><br>
	<?php 
        $datos2 = CHtml::listData(Medico::model()->findAll(''),'idmedico','nombreCompleto');
        echo $form->dropDownList($model, 'idenmedico',$datos2,array('class'=>'form-control','disabled'=>'disabled'));
        ?>
        <br>
        
        <b><?php echo CHtml::encode($model->getAttributeLabel('identipoagenda')); ?>:</b>
	<?php 
        $datos2 = CHtml::listData(Tipoagenda::model()->findAll(''),'idtipoagenda','tipoagenda');
        echo $form->dropDownList($model, 'identipoagenda',$datos2,array('class'=>'form-control','disabled'=>'disabled'));
        ?><BR>
        
	<?php echo $form->textFieldGroup($model,'porcentaje',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>
        
        <?php echo CHtml::encode($model->getAttributeLabel('valorlunes')); ?>
        <?php echo $form->textField($model,'valorlunes',array('class'=>'form-control','size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>

	<br><?php echo CHtml::encode($model->getAttributeLabel('valormartes')); ?>
        <?php echo $form->textField($model,'valormartes',array('class'=>'form-control','size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>

	<br><?php echo CHtml::encode($model->getAttributeLabel('valormiercoles')); ?>
        <?php echo $form->textField($model,'valormiercoles',array('class'=>'form-control','size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>

	<br><?php echo CHtml::encode($model->getAttributeLabel('valorjueves')); ?>
        <?php echo $form->textField($model,'valorjueves',array('class'=>'form-control','size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>

	<br><?php echo CHtml::encode($model->getAttributeLabel('valorviernes')); ?>
        <?php echo $form->textField($model,'valorviernes',array('class'=>'form-control','size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>
        <br>
        
         <?php 
         $idmedico = $model->idenmedico;
         $idtipoagenda = $model->identipoagenda;
         
         $sql = 'select porcentaje from agendamiento.porcentajes inner join agendamiento.medico on  agendamiento.porcentajes.idenmedico=agendamiento.medico.idmedico where idmedico='.$idmedico.' and identipoagenda!='. $idtipoagenda;
         $resultado = Yii::app()->db->createCommand($sql)->queryScalar();
        
         $porcentajeactual=$model->porcentaje;
         echo CHtml::hiddenField('porc1',  $porcentajeactual, array('size'=>60,'maxlength'=>100)); 
         echo CHtml::hiddenField('porc2',  $resultado, array('size'=>60,'maxlength'=>100)); 
        ?>                

      
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script> 
 $('#Porcentajes_porcentaje').focusout(function()
{
        var valor_original=$('#porc1').val();
	var porc1 = parseFloat($('#Porcentajes_porcentaje').val());
        var porc2 = parseFloat($('#porc2').val());
        var total_porc = porc1 + porc2;
        var valor_resta =100-porc2; 
 
        if (total_porc > 100)
        {
            alert('Error: El porcentaje no puede ser mayor a '+valor_resta);
            $('#Porcentajes_porcentaje').val(valor_original);
        }
        
});


 $(document).ready(function() {
   /* Aquí podría filtrar que controles necesitará manejar,
    * en el caso de incluir un dropbox $('input, select');
    */
   tb = $('#Porcentajes_porcentaje');
    
   if ($.browser.mozilla) {
       $(tb).keypress(enter2tab);
   } else {
       $(tb).keydown(enter2tab);
   }
   });
   
   
  function enter2tab(e) {
       if (e.keyCode == 13) {
           cb = parseInt($(this).attr('tabindex'));
    
           if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
               $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
               $(':input[tabindex=\'' + (cb + 1) + '\']').select();
               e.preventDefault();
    
               return false;
           }
       }
   }
</script>
            