<?php
$this->breadcrumbs=array(
	'Porcentajes'=>array('admin'),
	//$model->idporcentaje=>array('view','id'=>$model->idporcentaje),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Profesionales de la Salud','url'=>array('Medico/admin')),
	array('label'=>'Ver Porcentajes','url'=>array('view','id'=>$model->idporcentaje)),
	//array('label'=>'Administrar Porcentajes','url'=>array('admin')),
	);
	?>

<h3>ACTUALIZAR PORCENTAJES DE <?php echo $model->medico->nombreCompleto; ?></h3><BR>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
