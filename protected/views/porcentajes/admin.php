<?php
$this->breadcrumbs=array(
	'Porcentajes'=>array('admin'),
	'Lista',
);

$this->menu=array(
//array('label'=>'Lista Porcentajes','url'=>array('admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('porcentajes-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Porcentajes</h2>

<?php 
    $this->widget(
    'booster.widgets.TbExtendedGridView',
    array(
    'type' => 'striped bordered',
    'dataProvider' => $model->search($id),
    'columns' => array(
    array(
            'name'=>'Profesional de la Salud',
            'value'=>'$data->medico->nombreCompleto',
            'filter'=>CHtml::activeTextField($model, 'nombredoc',array('class'=>'form-control')),
        ),
	array(
            'name'=>'Tipo de Agenda',
            'value'=>'$data->tipoagenda->tipoagenda',
            'filter'=>CHtml::activeTextField($model, 'agenda',array('class'=>'form-control')),
        ),
        'valorlunes',
        'valormartes',
        'valormiercoles',
	'valorjueves',
	'valorviernes',
    array(
    'name' => 'porcentaje',
    'header' => 'Porcentajes',
    'class' => 'booster.widgets.TbEditableColumn',
    //'headerHtmlOptions' => array('style' => 'width:200px'),
    'editable' => array(
    //'type' => 'text',
        'url' => 'index.php?r=/porcentajes/valoresporcentajes',
        'htmlOptions'=>array('onclick'=>'$("#jose").val("jose");'),
    ),
     
    ),
    array(
          'class'=>'booster.widgets.TbButtonColumn',
          'template'=>'{update}',
    ),
    )
    )
    );
    ?>
<div style="text-align: right;margin-top: 10px;">
<?php
    print CHtml::link('Actualizar','index.php?r=porcentajes/admin&id='.$id,array('class'=>'btn btn-primary','id'=>'jose'));
?>
</div>