<?php
$this->breadcrumbs=array(
	'Porcentajes'=>array('admin'),
	$model->idporcentaje,
);

$this->menu=array(
array('label'=>'Lista de Profesionales de la Salud','url'=>array('medico/admin')),
array('label'=>'Actualizar Porcentajes','url'=>array('update','id'=>$model->idporcentaje)),
//array('label'=>'Borrar Porcentajes','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idporcentaje),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Administrar Porcentajes','url'=>array('admin')),
);
?>

<h1>Detalles </h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
                array('label'=>'Profesional de la Salud',
                    'value'=>$model->medico->nombreCompleto,
                    'type'=>'raw'),
                array('label'=>'Tipo de Agenda',
                    'value'=>$model->tipoagenda->tipoagenda,
                    'type'=>'raw'),
		
		'porcentaje',
		'valorlunes',
		'valormartes',
		'valormiercoles',
		'valorjueves',
		'valorviernes',
),
)); ?>
