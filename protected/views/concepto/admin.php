<?php
$this->breadcrumbs=array(
	'Conceptos'=>array('admin'),
	'Administración',
);

$this->menu=array(
//array('label'=>'Administrar Concepto','url'=>array('index')),
array('label'=>'Crear Concepto','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('concepto-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Conceptos</h2>




<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'concepto-grid',
    'rowCssClassExpression'=>'!$data->estado ? "danger":""',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'idconcepto',
		'concepto',
	//creadopor',
	//modificadopor',
	//fechacreado',
	//fechamodificado',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
