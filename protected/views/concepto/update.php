<?php
$this->breadcrumbs=array(
	'Conceptos'=>array('admin'),
	//$model->idconcepto=>array('view','id'=>$model->idconcepto),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista Concepto','url'=>array('admin')),
	array('label'=>'Crear Concepto','url'=>array('create')),
	array('label'=>'Detalles de Concepto','url'=>array('view','id'=>$model->idconcepto)),
	//array('label'=>'Administrar Concepto','url'=>array('admin')),
	);
	?>

	<h2>Actualizar Concepto </h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>