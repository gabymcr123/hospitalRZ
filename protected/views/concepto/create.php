<?php
$this->breadcrumbs=array(
	'Conceptos'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista Concepto','url'=>array('admin')),
//array('label'=>'Administrar Concepto','url'=>array('admin')),
);
?>

<h1>Crear Concepto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>