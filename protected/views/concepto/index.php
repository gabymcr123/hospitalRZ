<?php
$this->breadcrumbs=array(
	'Conceptos',
);

$this->menu=array(
array('label'=>'Crear Concepto','url'=>array('create')),
array('label'=>'Administrar Concepto','url'=>array('admin')),
);
?>

<h2>Conceptos</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
'htmlOptions' => array('class' => 'listaView'),
)); ?>
