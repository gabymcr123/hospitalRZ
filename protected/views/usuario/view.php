<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	//$model->usuario,
);
  $admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 3) ? true : false ;
$this->menu=array(
array('label'=>'Lista de Usuarios','url'=>array('admin'),'visible'=>$admin),
array('label'=>'Crear Usuario','url'=>array('create'),'visible'=>$admin),
array('label'=>'Actualizar Usuario','url'=>array('update','id'=>$model->idusuario),'visible'=>$admin),
array('label'=>'Eliminar Usuario','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idusuario),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>$admin),
//array('label'=>'Administrar Usuarios','url'=>array('admin')),
);
?>

<h2>Usuario:  <?php echo $model->usuario; ?></h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'idusuario',
		array(
			'label'=>'Rol',
			'type'=>'raw',
			'value'=>$model->idrol1->rol),
		'nombre',
		'apellido',
		'usuario',
                array('name'=>'password',
                    'value'=>'******',
                    ),
		/*'password',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',*/
),
)); ?>
