<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Usuarios','url'=>array('admin')),
//array('label'=>'Administrar Usuario','url'=>array('admin')),
);
?>

<h2>Crear Usuario</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>