<?php
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	'Lista',
);

$this->menu=array(
//array('label'=>'Lista de Usuarios','url'=>array('index')),
array('label'=>'Crear Usuario','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('usuario-grid', {
data: $(this).serialize()
});
return false;
});
");
$modeldatos= Datosgenerales::model()->find(); 
?>

<h3>Configuraciones Límite</h3>
<br>
<form class="form-horizontal">
<div class="form-group well">

    <label class="col-sm-3 control-label required" for="Usuario_nombre">

      Fechas límite a agendadores:
    </label>
    <div class="col-sm-6">
       <div class="input-group">
        <?php                    
        $this->widget('CJuiDateTimePicker',array(
                'language'=>'es',
                //'model'=>'modeldatos',                                // Model object
                'name' =>'limite',
                
                'attribute'=>'', // Attribute name
                'mode'=>'date',                     // Use "time","date" or "datetime" (default)
                'options'=>array(          
                    'dateFormat'=>'yy-mm-dd',
                ),                     // jquery plugin options
                'htmlOptions'=>array('class'=>'form-control') // HTML options
        ));                             
        ?> 
       
        
           <span class="input-group-addon" style="padding: 0px;"><input type="button" value="ACTUALIZAR" class="btn btn-primary" onclick="guardarlimite()"> </span></span>
    </div>
        <label>Valor Actual: </label><span id="valor"> <?php print $modeldatos->limiteagenda; ?></span>
</div>
</div>

<div class="form-group well">

    <label class="col-sm-3 control-label required" for="Usuario_nombre">
      Fechas límite a doctores:
    </label>
    <div class="col-sm-6">
       <div class="input-group">
        <?php                    
        $this->widget('CJuiDateTimePicker',array(
                'language'=>'es',
                //'model'=>'modeldatos',                                // Model object
                'name' =>'limitedoctor',
                
                'attribute'=>'', // Attribute name
                'mode'=>'date',                     // Use "time","date" or "datetime" (default)
                'options'=>array(          
                    'dateFormat'=>'yy-mm-dd',
                ),                     // jquery plugin options
                'htmlOptions'=>array('class'=>'form-control') // HTML options
        ));                             
        ?> 
       
        
           <span class="input-group-addon" style="padding: 0px;"><input type="button" value="ACTUALIZAR" class="btn btn-primary" onclick="guardarlimite()"> </span></span>
    </div>
        <label>Valor Actual: </label><span id="valordoctor"> <?php print $modeldatos->limiteagendadoctor; ?></span>
</div>
</div>
<div class="form-group well">

    <label class="col-sm-3 control-label required" for="Usuario_nombre">
      Fechas límite de referencia:
    </label>
    <div class="col-sm-6">
       <div class="input-group">
        <?php                    
        $this->widget('CJuiDateTimePicker',array(
                'language'=>'es',
                //'model'=>'modeldatos',                                // Model object
                'name' =>'limiteref',
                
                'attribute'=>'', // Attribute name
                'mode'=>'date',                     // Use "time","date" or "datetime" (default)
                'options'=>array(          
                    'dateFormat'=>'yy-mm-dd',
                ),                     // jquery plugin options
                'htmlOptions'=>array('class'=>'form-control') // HTML options
        ));                             
        ?> 
       
        
           <span class="input-group-addon" style="padding: 0px;"><input type="button" value="ACTUALIZAR" class="btn btn-primary" onclick="guardarlimite()"> </span></span>
    </div>
        <label>Valor Actual: </label><span id="valorreferencia"> <?php print $modeldatos->limitereferencia; ?></span>
</div>
</div>
</form>
<br>
<?php /* $this->widget('booster.widgets.TbGridView',array(
'id'=>'usuario-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->searchAgendador(),
'rowCssClassExpression'=>'!$data->estado ? "danger":""',
'type' => 'striped bordered condensed',
'filter'=>$model,
'columns'=>array(
		//'idusuario',
		array(
		   'name'=>'idrol',
                    'type'=>'text',
		   'value'=>'$data->idrol1->rol',
		   'filter'=>'',
		   
		),
		'nombre',
		'apellido',
		'usuario',
		//'password',
		
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		
array(
			'class'=>'booster.widgets.TbButtonColumn',
                        'template'=>'{update} {view}',
                    'buttons'=>array
    ()),
),
)); */ ?>

<script type="text/javascript">
    function guardarlimite(){
        var val = $('#limite').val();
        var agen = 'si';
        if (val == ''){
            val = $('#limitedoctor').val();
            agen = 'no';
            if(val==''){
                val = $('#limiteref').val();
                agen = 'ref';
            }
        }
            
        var url = 'index.php?r=/usuario/cambiolimite&valor='+val+'&agenda='+agen;
        
       $.ajax({
		type: "GET",
                url: url,
		cache: false,
	}).done(function( respuesta )
        {
            if(respuesta =='no'){
                $.notify("Error a guardar", "danger");
            }else{
                $.notify("Guardado correctamente", "success");
                if(agen=='si'){
                    $('#valor').html(' '+respuesta);
                    $('#limite').val('');
                }else if(agen=='no'){
                    $('#valordoctor').html(' '+respuesta);
                    $('#limitedoctor').val('');
                }else{
                    $('#valorreferencia').html(' '+respuesta);
                    $('#limiteref').val('');
                }
            }
	});
    }
</script>
    