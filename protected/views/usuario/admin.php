<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	'Lista',
);

$this->menu=array(
//array('label'=>'Lista de Usuarios','url'=>array('index')),
array('label'=>'Crear Usuario','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('usuario-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Usuario</h2>


<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'usuario-grid',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'rowCssClassExpression'=>'!$data->estado ? "danger":""',
'type' => 'striped bordered condensed',
'filter'=>$model,
'columns'=>array(
		//'idusuario',
		array(
		   'name'=>'idrol',
                    'type'=>'text',
		   'value'=>'$data->idrol1->rol',
		   'filter'=>CHtml::activeTextField($model, 'rol', array('class'=>'form-control')),
                    
		),
		'nombre',
		'apellido',
		'usuario',
		//'password',
		/*
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
    array(
        'class' => 'booster.widgets.TbToggleColumn',
        'toggleAction' => 'usuario/delete',
        'name' => 'estado',
        'uncheckedButtonLabel'=>'Activar',
        'checkedButtonLabel'=>'Desactivar',
        //'header' => 'Toggle',
        //'value'=>'$data->estado ? 1 : 0',
        'filter'=>'',
    ),
array(
'class'=>'booster.widgets.TbButtonColumn',
    'template'=>'{view}  {update}',
),
),
)); ?>
