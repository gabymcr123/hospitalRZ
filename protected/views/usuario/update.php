<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	//$model->usuario=>array('view','id'=>$model->idusuario),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Usuarios','url'=>array('admin')),
	array('label'=>'Crear Usuario','url'=>array('create')),
	array('label'=>'Detalles de Usuario','url'=>array('view','id'=>$model->idusuario)),
	//array('label'=>'Administrar Usuario','url'=>array('admin')),
	);
	?>

	<h2>Usuario:  <?php echo $model->usuario; ?></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>