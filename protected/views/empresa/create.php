<?php
$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista Empresa','url'=>array('admin')),
//array('label'=>'Manage Empresa','url'=>array('admin')),
);
?>

<h2>Crear Empresa</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>