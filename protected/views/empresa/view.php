<?php
$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	//$model->idempresa,
);

$this->menu=array(
array('label'=>'Lista Empresa','url'=>array('admin')),
array('label'=>'Crear Empresa','url'=>array('create')),
array('label'=>'Actualizar Empresa','url'=>array('update','id'=>$model->idempresa)),
//array('label'=>'Delete Empresa','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idempresa),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Manage Empresa','url'=>array('admin')),
);
?>

<h1>Empresa</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'idempresa',
		'nombre',
		'razonsocial',
		'direccion',
		'lleno1',
		'lleno2',
		//'creadopor',
		//'modificado',
		//'fechacreado',
		//'fechamodificado',
),
)); ?>
