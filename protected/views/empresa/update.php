<?php
$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	//$model->idempresa=>array('view','id'=>$model->idempresa),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista Empresa','url'=>array('admin')),
	array('label'=>'Crear Empresa','url'=>array('create')),
	array('label'=>'Detalles de Empresa','url'=>array('view','id'=>$model->idempresa)),
	//array('label'=>'Administrar Empresa','url'=>array('admin')),
	);
	?>

	<h2>Actualizar Empresa </h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>