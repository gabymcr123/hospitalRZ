<?php
$this->breadcrumbs=array(
	'Empresas',
);

$this->menu=array(
array('label'=>'Crear Empresa','url'=>array('create')),
array('label'=>'Administrar Empresa','url'=>array('admin')),
);
?>

<h2>Empresas</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
