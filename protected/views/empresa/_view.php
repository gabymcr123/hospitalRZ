<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('idempresa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idempresa),array('view','id'=>$data->idempresa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razonsocial')); ?>:</b>
	<?php echo CHtml::encode($data->razonsocial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lleno1')); ?>:</b>
	<?php echo CHtml::encode($data->lleno1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lleno2')); ?>:</b>
	<?php echo CHtml::encode($data->lleno2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creadopor')); ?>:</b>
	<?php echo CHtml::encode($data->creadopor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('modificado')); ?>:</b>
	<?php echo CHtml::encode($data->modificado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacreado')); ?>:</b>
	<?php echo CHtml::encode($data->fechacreado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechamodificado')); ?>:</b>
	<?php echo CHtml::encode($data->fechamodificado); ?>
	<br />

	*/ ?>

</div>