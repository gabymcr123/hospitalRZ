<?php
$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	'Administración',
);

$this->menu=array(
//array('label'=>'List Empresa','url'=>array('index')),
array('label'=>'Crear Empresa','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('empresa-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Empresas</h2>



<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'empresa-grid',
'rowCssClassExpression'=>'!$data->estado ? "danger":""',
    'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'idempresa',
		'nombre',
		'razonsocial',
		'direccion',
		'lleno1',
		'lleno2',
		/*
		'creadopor',
		'modificado',
		'fechacreado',
		'fechamodificado',
		*/
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
