<?php
$this->breadcrumbs=array(
	'Medicocabeceras',
);

$this->menu=array(
array('label'=>'Create Medicocabecera','url'=>array('create')),
array('label'=>'Manage Medicocabecera','url'=>array('admin')),
);
?>

<h1>Medicocabeceras</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
