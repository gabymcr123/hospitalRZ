<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('idcabecera')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idcabecera),array('view','id'=>$data->idcabecera)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idmedico')); ?>:</b>
	<?php echo CHtml::encode($data->idmedico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('horainicio')); ?>:</b>
	<?php echo CHtml::encode($data->horainicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('horafin')); ?>:</b>
	<?php echo CHtml::encode($data->horafin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('intervaloconsulta')); ?>:</b>
	<?php echo CHtml::encode($data->intervaloconsulta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creadopor')); ?>:</b>
	<?php echo CHtml::encode($data->creadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modificadopor')); ?>:</b>
	<?php echo CHtml::encode($data->modificadopor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacreado')); ?>:</b>
	<?php echo CHtml::encode($data->fechacreado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechamodificado')); ?>:</b>
	<?php echo CHtml::encode($data->fechamodificado); ?>
	<br />

	*/ ?>

</div>