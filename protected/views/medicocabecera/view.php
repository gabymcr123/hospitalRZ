<?php
$this->breadcrumbs=array(
	'Medicocabeceras'=>array('index'),
	$model->idcabecera,
);

$this->menu=array(
array('label'=>'List Medicocabecera','url'=>array('index')),
array('label'=>'Create Medicocabecera','url'=>array('create')),
array('label'=>'Update Medicocabecera','url'=>array('update','id'=>$model->idcabecera)),
array('label'=>'Delete Medicocabecera','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idcabecera),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Medicocabecera','url'=>array('admin')),
);
?>

<h1>View Medicocabecera #<?php echo $model->idcabecera; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'idcabecera',
		'idmedico',
		'horainicio',
		'horafin',
		'intervaloconsulta',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
),
)); ?>
