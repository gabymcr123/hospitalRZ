<?php
$this->breadcrumbs=array(
	'Medicocabeceras'=>array('index'),
	$model->idcabecera=>array('view','id'=>$model->idcabecera),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Medicocabecera','url'=>array('index')),
	array('label'=>'Create Medicocabecera','url'=>array('create')),
	array('label'=>'View Medicocabecera','url'=>array('view','id'=>$model->idcabecera)),
	array('label'=>'Manage Medicocabecera','url'=>array('admin')),
	);
	?>

	<h1>Update Medicocabecera <?php echo $model->idcabecera; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>