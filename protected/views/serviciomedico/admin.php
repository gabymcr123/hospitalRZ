<?php
$this->breadcrumbs=array(
	'Servicio médicos'=>array('admin'),
	'Lista',
);

$this->menu=array(
//array('label'=>'Lista de Servicios médico','url'=>array('index')),
array('label'=>'Crear Servicio médico','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('serviciomedico-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Servicio médico</h2>




<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'serviciomedico-grid',
    'rowCssClassExpression'=>'!$data->estado ? "danger":""',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'serviciomedico',
		//'creadopor',
		//'modificadopor',
		//'fechacreado',
		//'fechamodificado',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
