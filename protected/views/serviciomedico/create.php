<?php
$this->breadcrumbs=array(
	'Servicio médicos'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista Servicio médico','url'=>array('admin')),
//array('label'=>'Administrar servicios Médicos','url'=>array('admin')),
);
?>

<h1>Crear Servicio médico</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>