﻿<?php
$this->breadcrumbs=array(
	'Servicio médico'=>array('admin'),
	//$model->idserviciomedico,
);

$this->menu=array(
array('label'=>'Lista Servicio médicos','url'=>array('admin')),
array('label'=>'Crear Servicio médico','url'=>array('create')),
array('label'=>'Modificar Servicio médico','url'=>array('update','id'=>$model->idserviciomedico)),
array('label'=>'Eliminar servicio médico','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idserviciomedico),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Administrar servicios médicos','url'=>array('admin')),
);
?>

<h2>Servicio médico </h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'idserviciomedico',
		'serviciomedico',
		//'creadopor',
		//'modificadopor',
		//'fechacreado',
		//'fechamodificado',
),
)); ?>
