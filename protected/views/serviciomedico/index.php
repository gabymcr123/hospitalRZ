﻿<?php
$this->breadcrumbs=array(
	'Serviciomedicos',
);

$this->menu=array(
array('label'=>'Crear servicio médico','url'=>array('create')),
array('label'=>'Administrar servicios médico','url'=>array('admin')),
);
?>

<h2>Servicios médicos</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
