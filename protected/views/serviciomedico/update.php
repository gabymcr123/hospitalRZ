<?php
$this->breadcrumbs=array(
	'Servicio médico'=>array('admin'),
	//$model->idserviciomedico=>array('view','id'=>$model->idserviciomedico),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista Servicio médico','url'=>array('admin')),
	array('label'=>'Crear Servicio médico','url'=>array('create')),
	array('label'=>'Detalles de Servicio médico','url'=>array('view','id'=>$model->idserviciomedico)),
	//array('label'=>'Administar Servicio médico','url'=>array('admin')),
	);
	?>

	<h2>Actualizar Servicio médico </h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>