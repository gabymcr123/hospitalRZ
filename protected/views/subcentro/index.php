<?php
$this->breadcrumbs=array(
	'Centro de Salud',
);

$this->menu=array(
array('label'=>'Crear Centro de Salud','url'=>array('create')),
array('label'=>'Administrar Centro de Salud','url'=>array('admin')),
);
?>

<h1>Centro de Salud</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
