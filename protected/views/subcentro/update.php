<?php
$this->breadcrumbs=array(
	'Subcentros'=>array('index'),
	$model->idsubcentro=>array('view','id'=>$model->idsubcentro),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Subcentro','url'=>array('index')),
	array('label'=>'Create Subcentro','url'=>array('create')),
	array('label'=>'View Subcentro','url'=>array('view','id'=>$model->idsubcentro)),
	array('label'=>'Manage Subcentro','url'=>array('admin')),
	);
	?>

	<h1>Update Subcentro <?php echo $model->idsubcentro; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>