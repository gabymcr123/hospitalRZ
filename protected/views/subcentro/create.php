<?php
$this->breadcrumbs=array(
	'Centro de Salud'=>array('admin'),
	'Crear',
);

$this->menu=array(
//array('label'=>'Lista Subcentro','url'=>array('index')),
array('label'=>'Lista Centro de Salud','url'=>array('admin')),
);
?>

<h2>Crear Centro de Salud</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>