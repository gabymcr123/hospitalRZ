<?php
$this->breadcrumbs=array(
	'Centro de Salud'=>array('admin'),
	//$model->idsubcentro,
);

$this->menu=array(
array('label'=>'Lista Centro de Salud','url'=>array('admin')),
array('label'=>'Crear Centro de Salud','url'=>array('create')),
array('label'=>'Actualizar Centro de Salud','url'=>array('update','id'=>$model->idsubcentro)),
array('label'=>'Eliminar Centro de Salud','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idsubcentro),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Administrar Subcentro','url'=>array('admin')),
);
?>

<h2>Centro de Salud </h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'idsubcentro',
		'subcentro',
		//'creadopor',
		//'modificadopor',
		//'fechacreado',
		//'fechamodificado',
),
)); ?>
