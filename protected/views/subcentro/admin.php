<?php
$this->breadcrumbs=array(
	'Centro de Salud'=>array('admin'),
	'Lista',
);

$this->menu=array(
//array('label'=>'Lista de Subcentros','url'=>array('index')),
array('label'=>'Crear Centro de Salud','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('subcentro-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Centro de Salud</h2>




<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'subcentro-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'idsubcentro',
		'subcentro',
                'provincia',
                'canton',
                'parroquia',
		//'creadopor',
		//'modificadopor',
		//'fechacreado',
		//'fechamodificado',
array(
			'class'=>'booster.widgets.TbButtonColumn',
                        'template'=>'{update}{delete}',
                    'buttons'=>array
    ()),
),
)); ?>
