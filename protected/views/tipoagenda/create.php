<?php
$this->breadcrumbs=array(
	'Tipo de agenda'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista Tipo de agenda','url'=>array('admin')),
//array('label'=>'Administrar Tipo de agenda','url'=>array('admin')),
);
?>

<h1>Crear Tipo de agenda</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
