<?php
$this->breadcrumbs=array(
	'Tipo de Agenda'=>array('admin'),
	'Lista',
);

$this->menu=array(
//array('label'=>'Lista Tipos de agenda','url'=>array('index')),
array('label'=>'Crear Tipo de agenda','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('tipoagenda-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Tipos de agenda</h2>
<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'tipoagenda-grid',
'rowCssClassExpression'=>'!$data->estado ? "danger":""',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'htmlOptions'=>array('style'=>'font-size: 11px'),
'columns'=>array(
		//'idtipoagenda',
		'tipoagenda',
        array('header'=>'Rol',
        'value'=>'$data->idroles->rol',
        ),
	//creadopor',
	//modificadopor',
	//fechacreado',
	//fechamodificado',
    array(
    'class' => 'booster.widgets.TbToggleColumn',
    'toggleAction' => 'tipoagenda/delete',
    'name' => 'estado',
    'uncheckedButtonLabel'=>'Activar',
    'checkedButtonLabel'=>'Desactivar',
    //'header' => 'Toggle',
    //'value'=>'$data->estado ? 1 : 0',
    'filter'=>'',
    ),
array(
'class'=>'booster.widgets.TbButtonColumn',
'template'=>'{update}',
),
),
)); ?>
