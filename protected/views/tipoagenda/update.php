<?php
$this->breadcrumbs=array(
	'Tipo Agenda'=>array('admin'),
	//$model->idtipoagenda=>array('view','id'=>$model->idtipoagenda),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista Tipo de agenda','url'=>array('admin')),
	array('label'=>'Crear Tipo de agenda','url'=>array('create')),
	array('label'=>'Detalles de Tipo de agenda','url'=>array('view','id'=>$model->idtipoagenda)),
	//array('label'=>'Administrar Tipo de agenda','url'=>array('admin')),
	);
	?>

	<h2>Actualizar Tipo de agenda </h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>