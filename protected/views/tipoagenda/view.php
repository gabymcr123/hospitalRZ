<?php
$this->breadcrumbs=array(
	'Tipo de agenda'=>array('admin'),
	//$model->idtipoagenda,
);

$this->menu=array(
array('label'=>'Lista Tipo de agenda','url'=>array('admin')),
array('label'=>'Crear Tipo de agenda','url'=>array('create')),
array('label'=>'Actualizar Tipo de agenda','url'=>array('update','id'=>$model->idtipoagenda)),
array('label'=>'Eliminar Tipo de agenda','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idtipoagenda),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Administrar Tipo de agenda','url'=>array('admin')),
    
);
?>

<h2>Tipo de agenda</h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'idtipoagenda',
		'tipoagenda',
		//'creadopor',
		//'modificadopor',
		//'fechacreado',
		//'fechamodificado',
),
)); ?>
