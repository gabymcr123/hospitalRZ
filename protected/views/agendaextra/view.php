<?php
$this->breadcrumbs=array(
	'Agenda Extra'=>array('admin'),
	//$model->idextra,
);

$this->menu=array(
array('label'=>'Lista Agenda Extra','url'=>array('admin')),
array('label'=>'Crear Agenda Extra','url'=>array('create')),
array('label'=>'Actualizar Agenda Extra','url'=>array('update','id'=>$model->idextra)),
array('label'=>'Eliminar Agendaextra','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idextra),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Administrar Agendaextra','url'=>array('admin')),
);
?>

<h1>Agenda Extra </h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		
                 array(
			'label'=>'Profesional de la Salud',
			'type'=>'raw',
			'value'=>$model->medico1->nombreCompleto),
    
                 array(
			'label'=>'Tipo de Agenda',
			'type'=>'raw',
			'value'=>$model->tipo1->tipoagenda),
                 array(
			'label'=>'Responsable',
			'type'=>'raw',
			'value'=>$model->usuario1->nombreCompleto),
		'fecha',
		'numturno',
),
)); ?>
