<div class="view">


	<b><?php echo CHtml::encode($data->getAttributeLabel('idmedico')); ?>:</b>
	<?php echo CHtml::encode($data->idmedico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idtipo')); ?>:</b>
	<?php echo CHtml::encode($data->idtipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('responsable')); ?>:</b>
	<?php echo CHtml::encode($data->responsable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numturno')); ?>:</b>
	<?php echo CHtml::encode($data->numturno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('creadopor')); ?>:</b>
	<?php echo CHtml::encode($data->creadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modificadopor')); ?>:</b>
	<?php echo CHtml::encode($data->modificadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacreado')); ?>:</b>
	<?php echo CHtml::encode($data->fechacreado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechamodificado')); ?>:</b>
	<?php echo CHtml::encode($data->fechamodificado); ?>
	<br />

	*/ ?> <hr>

</div>