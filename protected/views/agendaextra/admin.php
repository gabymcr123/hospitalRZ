<?php
$this->breadcrumbs=array(
	'Agenda Extra'=>array('admin'),
	'Lista',
);

$this->menu=array(
array('label'=>'Lista Agenda Extra','url'=>array('admin')),
array('label'=>'Crear Agenda Extra','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('agendaextra-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Lista Agenda Extra</h1>




<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'agendaextra-grid',
    'rowCssClassExpression'=>'!$data->estado ? "danger":""',
'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array('name'=>'idmedico',
                    'value'=>'$data->medico1->nombreCompleto',
                    'filter'=>CHtml::activeTextField($model, 'medico',array('class'=>'form-control')),),
		
		array('name'=>'idtipo',
                    'value'=>'$data->tipo1->tipoagenda',
                    'filter'=>CHtml::activeTextField($model, 'tipoagenda',array('class'=>'form-control')),),
    
                array('name'=>'responsable',
                    'value'=>'$data->usuario1->nombreCompleto',
                    'filter'=>CHtml::activeTextField($model, 'responsable',array('class'=>'form-control')),),
             array('name'=>'fecha',
                    'value'=>'$data->fecha',
                    'filter'=> CHtml::activeDateField($model, 'fecha', array('class'=>'form-control')),),
		'numturno',
		/*
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		*/
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>


