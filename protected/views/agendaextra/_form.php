<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
    'type'=>'horizontal',
	'id'=>'agendaextra-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Campos con <span class="required">*</span> Son Requeridos.</p>


<?php
$sql = 'select rolusuario.rol from agendamiento.usuario inner join agendamiento.rolusuario on usuario.idrol = rolusuario.idrol where idusuario ='.Yii::app()->user->id;
$tipo = Yii::app()->db->createCommand($sql)->queryScalar();
$idusuario = Yii::app()->user->id;
if($tipo =="MEDICO")
    {
    $datos3 = CHtml::listData(Medico::model()->findAll('idusuario='.$idusuario),'idmedico','nombreCompleto'); 
    echo $form->dropDownListGroup($model, 'idmedico',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $datos3))); 
    
    }
else
    {
        echo $form->errorSummary($model); 
        echo $form->labelEx($model,'idmedico',array('class'=>'col-sm-3 control-label required')); 

        echo $form->hiddenField($model,'idmedico','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); 	
        $form->hiddenField($model,'idmedico','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control')); 
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'medico',
                'model'=>$model,
                'source'=>$this->createUrl('Agendaextra/autocompletar'),
                'options'=>array
                (
                    'showAnim'=>'fold',
                    'select'=>"js:function(event, ui) 
                    {
                        $('#".CHtml::activeId($model,'idmedico')."').val(ui.item.id); 
                    }",
                ),
                'htmlOptions'=>array
                (
                    'size'=>30,
                    'placeholder'=>'Buscar Profesional....',
                    'title'=>'Indique el Profesional',
                    'class'=>'span5 form-control',
                    'onkeypress'=>"$('#".CHtml::activeId($model,'idmedico')."').val($('#idmedico').val()); "
                ),
                ));
                echo("<br>");
                
}?>
        <?php 
        $datos2 = CHtml::listData(Tipoagenda::model()->findAll('idtipoagenda=4 or idtipoagenda=3'),'idtipoagenda','tipoagenda');
        echo $form->dropDownListGroup($model, 'idtipo',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $datos2)));
        ?>
	<?php echo $form->datePickerGroup($model,'fecha',array('widgetOptions'=>array('options'=>array('format'=>'yyyy/mm/dd'),'htmlOptions'=>array('class'=>'span5','style'=>'position:initial'),), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click para seleccionar una Fecha')); ?>

	<?php echo $form->textFieldGroup($model,'numturno',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

       


	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>


<script> 
 $('#Agendaextra_fecha').change(function()
{
	var fech1 = $('#Agendaextra_fecha').val();
	
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var fech2 = (month<10 ? '0' : '') + month + '/'+day+'/'+ +d.getFullYear();
        if((Date.parse(fech1)) < (Date.parse(fech2)))
	{
		alert('No puede seleccionar una fecha pasada');
		$('#Agendaextra_fecha').val("");
	}
});



 
</script>
