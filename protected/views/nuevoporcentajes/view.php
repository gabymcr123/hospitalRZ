<?php
$this->breadcrumbs=array(
	'Nuevoporcentajes'=>array('index'),
	$model->idnuevo,
);

$this->menu=array(
array('label'=>'List Nuevoporcentajes','url'=>array('index')),
array('label'=>'Create Nuevoporcentajes','url'=>array('create')),
array('label'=>'Update Nuevoporcentajes','url'=>array('update','id'=>$model->idnuevo)),
array('label'=>'Delete Nuevoporcentajes','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idnuevo),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Nuevoporcentajes','url'=>array('admin')),
);
?>

<h1>View Nuevoporcentajes #<?php echo $model->idnuevo; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'idnuevo',
		'idmedico',
		'fechainicio',
		'fechafin',
		'valorreferencia',
		'valorsubcecuente',
		'creadopor',
		'fechacreado',
),
)); ?>
