<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'nuevoporcentajes-form',
        'type'=>'horizontal',
	'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<div class="form-group">

    <label class="col-sm-3 control-label required" for="Nuevoporcentajes_idmedico"></label>
    <div class="col-sm-9">
    <div id="elegir" class="btn-group" data-toggle="buttons" style="">
    <label class="btn btn-default btn-sm active" onclick="elegirprofesional();" name="hora">
        <input type="radio" value="1" name="profesional" id="profesional" /> Profesional
    </label>
    <label class="btn btn-default btn-sm" onclick="elegirespecialidad();" name="hora">
        <input type="radio" value="2" name="especialidad" id="especialidad" onclick="elegirespecialidad();">Especialidad
    </label>
    <label class="btn btn-default btn-sm" onclick="elegirgeneral();" name="hora">
        <input type="radio" value="3" name="general" id="general" /> General
    </label>
    </div>
    </div>
</div>
        <?php echo $form->hiddenField($model,'idmedico','',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>		

<div class="form-group">
<?php echo CHtml::label('Profesional de la salud','profesional', array('class'=>'col-sm-3 control-label required'));?>
<div class="col-sm-9" id="cont_profesional">
        <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'name'=>'profesional_salud',
                //'model'=>$model,
                'source'=>$this->createUrl('agenda/autocompletarprofesional'),
                'options'=>array
                (
                    'showAnim'=>'fold',
                    'select'=>"js:function(event, ui) 
                    {
                        $('#Nuevoporcentajes_idmedico').val(ui.item.id);
                    }",
                ),
                'htmlOptions'=>array
                (
                    'size'=>35,
                    'maxlength'=>50,
                    'placeholder'=>'Profesional de la salud',
                    'title'=>'Indique el Profesional',
                    'style'=>'position:initial;',
                    'class'=>'span5 form-control',
                    'onkeyup'=>'this.value=this.value.toUpperCase()',
                ),
                )); ?>
</div>    
    <div id="cont_especialidad" class="col-sm-9">
        <?php $datos = CHtml::listData(Especialidadmedica::model()->findAll('estado = true order by especialidad'),'idespecialidad','especialidad'); ?>
        <?php echo CHtml::dropDownList('listespecialidad',0,$datos, array('empty'=>'-Seleccione Especialidad-','class'=>'span5 form-control')); ?>
    </div>
    <div id="cont_general" class="col-sm-9">
        <label>Afectará a todos los profesionales de la salud</label>
        <input type="text" name="vgeneral" id="vgeneral" value="" hidden="true">
    </div>
</div>
      	<?php echo $form->textFieldGroup($model,'valorreferencia',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','onblur'=>'porcentajes(this);','style'=>'position:initial'),), 'prepend'=>'<img src="images/porcentaje.gif" />',)); ?>

	<?php echo $form->textFieldGroup($model,'valorsubcecuente',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','onblur'=>'porcentajes(this);','style'=>'position:initial')),'prepend'=>'<img src="images/porcentaje.gif" />')); ?>

	<?php echo $form->datePickerGroup($model,'fechainicio',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5','style'=>'position:initial')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>

	<?php echo $form->datePickerGroup($model,'fechafin',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5','style'=>'position:initial')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>


<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    $('#cont_especialidad').hide();
    $('#cont_general').hide();
    function elegirprofesional(){
        $('#cont_especialidad').hide('show');
        $('#cont_general').hide('show');
        $('#cont_profesional').show('show');
        $('#vgeneral').val('');
        document.getElementById('listespecialidad').selectedIndex='0';
    }
    function elegirespecialidad(){
        $('#cont_profesional').hide('show');
        $('#cont_general').hide('show');
        $('#cont_especialidad').show('show');
        $('#Nuevoporcentajes_idmedico').val('');
        $('#profesional_salud').val('');
        $('#vgeneral').val('');
    }
    function elegirgeneral(){
        $('#cont_profesional').hide('show');
        $('#cont_especialidad').hide('show');
        $('#cont_general').show('show');
        $('#Nuevoporcentajes_idmedico').val('');
        $('#profesional_salud').val('');
        $('#vgeneral').val('si');
        document.getElementById('listespecialidad').selectedIndex='0';
    }
    function porcentajes(input){
        var v1 = $('#Nuevoporcentajes_valorreferencia').val();
        var v2 = $('#Nuevoporcentajes_valorsubcecuente').val();
        if(parseInt(v1)+parseInt(v2)>100){
            alert("La suma de porcetajes no puede ser mayor a 100");
            $(input).val('');
        }
    }
</script>