<?php
$this->breadcrumbs=array(
	'Nuevoporcentajes'=>array('index'),
	$model->idnuevo=>array('view','id'=>$model->idnuevo),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Nuevoporcentajes','url'=>array('index')),
	array('label'=>'Create Nuevoporcentajes','url'=>array('create')),
	array('label'=>'View Nuevoporcentajes','url'=>array('view','id'=>$model->idnuevo)),
	array('label'=>'Manage Nuevoporcentajes','url'=>array('admin')),
	);
	?>

	<h1>Update Nuevoporcentajes <?php echo $model->idnuevo; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>