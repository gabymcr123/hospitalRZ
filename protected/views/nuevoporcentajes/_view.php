<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('idnuevo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idnuevo),array('view','id'=>$data->idnuevo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idmedico')); ?>:</b>
	<?php echo CHtml::encode($data->idmedico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechainicio')); ?>:</b>
	<?php echo CHtml::encode($data->fechainicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechafin')); ?>:</b>
	<?php echo CHtml::encode($data->fechafin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valorreferencia')); ?>:</b>
	<?php echo CHtml::encode($data->valorreferencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valorsubcecuente')); ?>:</b>
	<?php echo CHtml::encode($data->valorsubcecuente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creadopor')); ?>:</b>
	<?php echo CHtml::encode($data->creadopor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacreado')); ?>:</b>
	<?php echo CHtml::encode($data->fechacreado); ?>
	<br />

	*/ ?>

</div>