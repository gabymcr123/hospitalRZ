<?php
$this->breadcrumbs=array(
	'Eventos'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Eventos','url'=>array('admin')),    
//array('label'=>'Administrar eventos','url'=>array('admin')),
);
?>

<h2>Crear Evento</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>