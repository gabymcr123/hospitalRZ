<?php
$this->breadcrumbs=array(
	'Eventos'=>array('admin'),
	'Lista de Eventos',
);

$this->menu=array(
//array('label'=>'Lista de Eventos','url'=>array('admin')),
array('label'=>'Crear Evento','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('evento-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Evento</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'evento-grid',
'rowCssClassExpression'=>'!$data->estado ? "danger":""',
'dataProvider'=>$model->search(),
'type' => 'striped bordered condensed',
'filter'=>$model,
'columns'=>array(
		
	//idconcepto',
	 array(
                    'name'=>'idconcepto',
                    'value'=>'$data->idconcepto1->concepto',
                    'filter'=>CHtml::activeTextField($model, 'concepto',array('class'=>'form-control')),
                ),
                'descripcion',
                array('name'=>'fechainicio',
                    'value'=>'$data->fechainicio',
                    'filter'=> CHtml::activeDateField($model, 'fechainicio', array('class'=>'form-control')),),
		
		
                array('name'=>'fechafin',
                    'value'=>'$data->fechafin',
                    'filter'=> CHtml::activeDateField($model, 'fechafin', array('class'=>'form-control')),),
    
                array(
                    'name'=>'idmedico',
                    'value'=>'$data->medico->nombreCompleto',
                    'filter'=>CHtml::activeTextField($model, 'nombredoc',array('class'=>'form-control')),
                ),
		//'creadopor',
		/*
		'modificadopor',
		'fechacreado',
		'fechamodificado',
		'idmedico',
		*/
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
