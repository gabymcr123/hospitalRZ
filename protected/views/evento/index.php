<?php
$this->breadcrumbs=array(
	'Eventos',
);

$this->menu=array(
array('label'=>'Crear evento','url'=>array('create')),
array('label'=>'Administrar eventos','url'=>array('admin')),
);
?>

<h2>Eventos</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
