<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'evento-form',
    'enableAjaxValidation'=>false,
'enableClientValidation'=>true,
    'type'=>'horizontal',
'clientOptions' => array(
'validateOnSubmit' => true,)
)); ?>

<p class="help-block">Los Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model); ?>

	<?php $datos = CHtml::listData(Concepto::model()->findAll('ESTADO=true'),'idconcepto','concepto'); ?>
	<?php echo $form->dropDownListGroup($model, 'idconcepto',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $datos))); ?>

	<?php echo $form->textFieldGroup($model,'descripcion',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

        <?php echo $form->datePickerGroup($model,'fechainicio',array('widgetOptions'=>array('options'=>array('format'=>'yyyy/mm/dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Selecciona la Fecha Inicial del Evento')); ?>

        <?php echo $form->datePickerGroup($model,'fechafin',array('widgetOptions'=>array('options'=>array('format'=>'yyyy/mm/dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Selecciona la Fecha Final del Evento')); ?>
<hr>	
<?php echo CHtml::HiddenField('val1', 0, array('size'=>60,'maxlength'=>100));?>
<h3>Seleccione el Tipo de Evento</h3>

<h4><input value="1" type="radio" name="evento" id= "doctor"  onclick="toggle(this)">
<label for = "doctor"> Por doctor</label></h4>
<div id="esconder" style="display:none;">
        <?php $form->hiddenField($model,'idmedico',array()); 
	 $this->widget('zii.widgets.jui.CJuiAutoComplete', array(

            'name'=>'pais',

            'model'=>$model,

            'source'=>$this->createUrl('Evento/autocompletar'),

            'options'=>array
             (
                'showAnim'=>'fold',
                'select'=>"js:function(event, ui) 
                    {
                      $('#".CHtml::activeId($model,'idmedico')."').val(ui.item.id); 
                    }",
            ),
            'htmlOptions'=>array
            (
     		'size'=>30,
     		'placeholder'=>'Buscar profesional de la salud....',
     		'title'=>'Indique el profesional de la salud'
            ),
            ));
  	?>
        
    
        <?php $datos = CHtml::listData(Medico::model()->findAll(),'idmedico','nombreCompleto'); ?>
	<?php echo $form->error($model,'idmedico'); ?>
        <?php echo $form->dropDownListGroup($model, 'idmedico',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $datos))); ?>
</div>
<h4><input value="2"  type="radio" name="evento" id= "especialidad"  onclick="toggle(this)">
<label for = "especialidad">Por especialidad</label></h4>

    <div id="esconder1" style="display:none;">
  <?php $espe = CHtml::listData(Especialidadmedica::model()->findAll('estado = true'),'idespecialidad','especialidad'); ?>
        <?php echo CHtml::dropDownList('espe', '', 
              $espe,
              array('empty' => 'Seleccione'));?>  
</div>
<h4><input value="3" type="radio" name="evento" id= "general"  onclick="toggle(this)">
<label for = "general">Evento General</label></h4>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<script> 
 $('#Evento_fechafin').change(function()
{
	var fech1 = $('#Evento_fechainicio').val();
	var fech2 = $('#Evento_fechafin').val();
	if((Date.parse(fech1)) > (Date.parse(fech2)))
	{
		alert('La fecha inicial no puede ser mayor que la fecha final');
		$('#Evento_fechafin').val("");
	}
});



 $('#Evento_fechafin').click(function()
{
    var fecha = $('#Evento_fechainicio').val();
    if(fecha.length < 1)
    {
        alert('Ingrese primero la Fecha Inicio');
        $('#Evento_fechafin').val("");
    }
});

    function getValor(){
        
        return valor;
    }
    function getValor2(){
  
        return valor2;
        
    }
valor =0;

  function toggle(elemento) {
        if(elemento.value=="1") {
              document.getElementById("esconder").style.display = "block";
              $('#val1').val(1);
             
           }else{
                document.getElementById("esconder").style.display = "none";
              valor = 0;
           }
           
        if(elemento.value=="2") {
              document.getElementById("esconder1").style.display = "block";
              $('#val1').val(2);
             
           }else{
                document.getElementById("esconder1").style.display = "none";
              valor = 0;
           }
           
           if(elemento.value=="3") {
              
                            $('#val1').val(3);
             
           }else{
                
              valor = 0;
           }
           
               }
</script>