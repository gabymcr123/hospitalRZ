<?php
$this->breadcrumbs=array(
	'Eventos'=>array('admin'),
	//$model->idevento=>array('view','id'=>$model->idevento),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Eventos','url'=>array('admin')),
	array('label'=>'Crear Evento','url'=>array('create')),
	array('label'=>'Detalles de Eventos','url'=>array('view','id'=>$model->idevento)),
	//array('label'=>'Administrar eventos','url'=>array('admin')),
	);
	?>

	<h2>Actualizar Evento </h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>