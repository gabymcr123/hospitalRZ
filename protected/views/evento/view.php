<?php
$this->breadcrumbs=array(
	'Eventos'=>array('admin'),
	//$model->descripcion,
);

$this->menu=array(
array('label'=>'Lista de Eventos','url'=>array('admin')),
array('label'=>'Crear Evento','url'=>array('create')),
array('label'=>'Modificar Evento','url'=>array('update','id'=>$model->idevento)),
array('label'=>'Eliminar evento','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idevento),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Administrar eventos','url'=>array('admin')),
);
?>

<h2>Evento</h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
//'idevento',
		//'idconcepto',
		'descripcion',
		'fechainicio',
		'fechafin',
	//creadopor',
	//modificadopor',
	//fechacreado',
	//fechamodificado',
		array(
			'label'=>'Profesional de la Salud',
			'type'=>'raw',
			'value'=>$model->medico->nombreCompleto),
),
)); ?>
