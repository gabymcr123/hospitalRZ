<?php
$this->breadcrumbs=array(
	'Agenda2s'=>array('index'),
	$model->idagenda,
);

$this->menu=array(
array('label'=>'List Agenda2','url'=>array('index')),
array('label'=>'Create Agenda','url'=>array('create')),
array('label'=>'Update Agenda2','url'=>array('update','id'=>$model->idagenda)),
array('label'=>'Delete Agenda2','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idagenda),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Agenda2','url'=>array('admin')),
);
?>

<h1>View Agenda2 #<?php echo $model->idagenda; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		
		array('label'=>'Tipo de Agenda',
                    'value'=>$model->idtipoagenda1->tipoagenda,
                    'type'=>'raw'),
    
		array('name'=>'Subcentro',
                    'value'=>$model->idsubcentro1->subcentro,
                    'filter'=>CHtml::activeTextField($model, 'nombresub',array('class'=>'form-control')),),
    
		'nombre',
		'numeroturno',
		'fecha',
		'horainicio',
		'horafin',
                array(
                    'name'=>'atendido',
                    'header'=>'Atendido',
                    'filter'=>array('1'=>'Si','0'=>'No'),
                    'value'=>($model->atendido=="1")?("Si"):("No")),
),
)); ?>
