<link rel="stylesheet" type="text/css" media="all" href="css/calendario.css" />
<?php

$this->menu=array(
array('label'=>'Crear Cita','url'=>array('agenda/create')),
array('label'=>'Lista de citas agendadas','url'=>array('agenda/adminmedico')),
);

if (isset($_GET["mes"])) $mes = $_GET["mes"]; else $mes = '';
if (isset($_GET["anio"])) $anio = $_GET["anio"]; else $anio = '';
?>

<h3>Citas Agendadas</h3>
<div style="font-size: 12px; text-align: right;">
<label>Fecha:</label>
<?php     
$this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fechadia',
    'value'=>$f,
    'options' => array(
    'language' => 'es',
    'format' => 'yyyy-mm-dd',
    ),
    'htmlOptions'=>array('style' =>'height:20px;padding:3px;color:#000;min-width:100px;width:100px;'),
    )
    );
    ?>
    <a href="index.php?r=/agenda2/admin" class="btn btn-primary" id="fech" style="margin:2px;padding:2px 5px; height:22px;font-size: 11px;">Ver</a>
</div>
<?php 
$med = Yii::app()->db->createCommand('select idespecialidad from agendamiento.medico where idusuario='.Yii::app()->user->id)->queryScalar();
$med = $med == 11 ? true : false;
$this->widget('booster.widgets.TbGridView',array(
//'id'=>'agenda2-grid',
'dataProvider'=>$model->search($f),
//'filter'=>$model,
'columns'=>array(
                 array('name'=>'Tipo de Agenda',
                    'value'=>'$data->idtipoagenda1->tipoagenda',
                    'filter'=>CHtml::activeTextField($model, 'nombretipoag',array('class'=>'form-control')),),
                array('name'=>'nombre',
                    'sortable'=>false
                    ),
                array('name'=>'numeroturno',
                    'sortable'=>false,
                    'htmlOptions'=>array('style'=>'text-align:center;'),
                    ), 
                 array(
                    'name'=>'atendido',
                    'header'=>'Atendido',
                    'filter'=>array('1'=>'Si','0'=>'No'),
                    'sortable'=>false,
                    'value'=>'($data->atendido=="1")?("Si"):("No")'),
                array('name'=>'Consultorio',
                    'value'=>'$data->idmedico1->pkidusuario->nombre."".$data->idmedico1->pkidusuario->apellido',
                    'visible'=>$med
                    ),
                    array(
    'class' => 'booster.widgets.TbToggleColumn',
    'toggleAction' => 'agenda2/atender',
    'name' => 'atendido',
    'header' => 'Atender',
    'sortable'=>false,
    'checkedButtonLabel'=>'Cancelar atención',
    'uncheckedButtonLabel'=>'Atender',
    //'value'=>'$data->estado ? 1 : 0',
    'filter'=>'',
    ),
    array(
                'class'=>'booster.widgets.TbButtonColumn',
                'template'=>'{agendar}',
                'header'=>'Agendar',
                'buttons'=>array
                (           
                'agendar' => array
                (   
                    'icon'=>'plus',
                    'title'=>'porcentaje',
                    'url'=>'"javascript:agendar(".$data->idpaciente.",".$data->idmedico.",\'".$data->idpaciente1->nombre." ".$data->idpaciente1->apellido."\')"',
                ),
                )
		),
		
),
)); ?>
<script type="text/javascript">
    var Doctor;
    function agendar(paciente,medico,nombre){
      $('#Agenda_fecha').val('');
      $('#Agenda_numeroturno').val('');
      $('#horario').hide();
      $('input.fecha:checked').val('');
      $('#Agenda_idtipoagenda').val(2);
      $('#mensaje').hide();
      
      $('#modelo').click();
      Doctor = medico;
      $('#Agenda_idmedico').val(medico);
      $('#Agenda_idpaciente').val(paciente);
      $('#Agenda_nombre').val(nombre);
      $('#nombrepaciente').html(nombre);
      generar_calendario('<?php print $mes;?>','<?php print $anio; ?>');
      
    }
    function generar_calendario(mes,anio)
	{       
                $('#veragenda').fadeOut();
		var agenda=$(".cal");
		agenda.html("<img src='images/loading.gif'>");
		$.ajax({
			type: "GET",
			url: 'index.php?r=agenda2/calendario',
			cache: false,
			data: { mes:mes,anio:anio,accion:"generar_calendario",especialidad:'',doctor:Doctor}
		}).done(function( respuesta )
		{
			$('.cal').html(respuesta);
		});
	}
function selectfecha(fecha){
    $('#Agenda_fecha').val(fecha);
    par = {'idmedico':Doctor,'fecha':fecha};
    $.ajax({
         url:'index.php?r=/agenda/obtenerhorario',
         type:'GET',
         data: par,
         success: function (resp) {
         $('#horario').html(resp);
        },
    });
}    
    $(document).on("click",".anterior,.siguiente",function(e)
			{
				e.preventDefault();
				var datos=$(this).attr("rel");
				var nueva_fecha=datos.split("-");
				generar_calendario(nueva_fecha[1],nueva_fecha[0]);
			});
$(document).on("click",'a.falta',function(e)
			{
                                enviolista='index.php?r=agenda/lista';
				e.preventDefault();
				var fecha = $(this).attr('rel');
				$('#mask').fadeIn(1000).html("<div id='nuevo_evento' class='window' rel='"+fecha+"'><a href='#' class='close' >&nbsp;</a><div id='respuesta'></div><div id='respuesta_form'></div></div>");
				$.ajax({
					type: "GET",
					url: enviolista,
					cache: false,
					data: { fecha:fecha,accion:"lista_evento",especialidad:'',doctor:Doctor }
				}).done(function( respuesta )
				{       
                                    $("#respuesta_form").html(respuesta);
				});

			});
                        /* FIN DE LISTA DE EVENTO */
			$(document).on("click",'.close',function (e)
			{
				e.preventDefault();
				$('#mask').fadeOut();
				setTimeout(500);
			});
                        
$(document).on("change",'input.fecha',function(e){
        var fec = $('input.fecha:checked').val();
        if(fec){
            $('#Agenda_idtipoagenda').change();
        }
    });
    
$(document).on("change",'#Agenda_idtipoagenda',function(e){
//        if(this.value!='7'){
//            $('#Agenda_idmedico').val(Doctor);
//            $('.medico').hide();
//        }else{
//            $('.medico').show('show');
//        }
        var fech = $('input.fecha:checked').val();
        if(!fech){
            return;
        }
        
        $.ajax({
	type: "GET",
	url: 'index.php?r=agenda/tipoagenda',
	cache: false,
	data: {'id':this.value,'idme':Doctor,'fecha':fech }
	}).done(function( response )
	{       
           if(response == "._."){
            $('#yw0').attr('disabled','disabled');
            $('#mensaje').attr('class','alert in fade alert-danger');
            $('#mensaje').html('<strong>Alerta: </strong> No tiene permisos para el tipo de agendamiento');
            $('#mensaje').show('show');
        }else{
            $('#yw0').removeAttr('disabled');
            var mat = new Array();
            mat = response.split('_');
            $('#ini').html(parseInt(mat[0])+1);
            $('#fin').html(parseInt(mat[1]));
            $('Agenda_numeroturno').val(parseInt(mat[0]));
            var agend = parseInt($('#Agenda_idtipoagenda').val());
            iniciofin(parseInt(mat[0]),parseInt(mat[1]))
            if(agend==3 || agend==4){
                $('#horario').hide('show');
            }else{
                $('#horario').show("show");
            }
            
        }
	});
});
$(document).on("submit",'#agenda-form',function (e){
            var fech = $('input.fecha:checked').val();
            var numero = $('.active > input').val();
            var form = $('#agenda-form');
            $('#Agenda_numeroturno').val(numero);
            if(!fech || !numero){
                ret = false;
                $('#mensaje').attr('class','alert in fade alert-warning');
                $('#mensaje').html('<strong>Datos incompletos.</strong>');
                $('#mensaje').show('show');
                setTimeout(function(){ $('#mensaje').hide('show'); }, 2100);
            }else{
                $.ajax({
                    type: 'POST',
                    url: 'index.php?r=/agenda2/create',
                    data: form.serialize(),
                    success: function(data) {
                        $('#veragenda').fadeIn(1000).html("<div id='nuevo_evento' class='window' style='margin:55px 0px 0px 0px;'><div id='respuestaagenda'></div></div>");
                        $('#respuestaagenda').html(data);
                    }
                });
            }
        return false;
        });
        
        
        function verificarEnvio(){
            var ret = true;
            var fech = $('input.fecha:checked').val();
            var numero = $('.active > input').val();
            var form = $('#agenda-form');
            $('#Agenda_numeroturno').val(numero);
            if(!fech || !numero){
                ret = false;
                $('#mensaje').attr('class','alert in fade alert-warning');
                $('#mensaje').html('<strong>Datos incompletos.</strong>');
                $('#mensaje').show('show');
                setTimeout(function(){ $('#mensaje').hide('show'); }, 2100);
            }
            $.ajax({
                    type: 'POST',
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    // Mostramos un mensaje con la respuesta de PHP
                    success: function(data) {
                        //$('#result').html(data);
                        alert(data);
                    }
                });
                
        return ret;
        }
    function iniciofin(inicio, fin){
    if($('#Agenda_idtipoagenda').val()=='4'){
        inicio = 0;
        fin = 1;
    }
        if(inicio>=fin){
            $('#mensaje').attr('class','alert in fade alert-danger');
            $('#mensaje').html('<strong>Citas completas</strong>');
            $('#mensaje').show('show');
            $('#yw3').attr('disabled','disabled');
         }else{
            $('#mensaje').hide('show');
            $('#yw3').removeAttr('disabled');
         }
    }
    $('#fechadia').change(function(){
        $('#fech').attr('href','index.php?r=/agenda2/admin&f='+this.value);
    });
    
    </script>
        <?php $this->beginWidget(
    'booster.widgets.TbModal',
    array('id' => 'myModal')
    ); ?>
    
    <div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Agendar</h4>
    <span id="fin" style="color:blue; float:right"></span>
    <span style="float:right;">&nbsp;de&nbsp;</span> 
    <span id="ini" style="color:green; float:right"></span> 
    </div>
    <div class="modal-body" style="padding-top: 4px;">        
    <div id="mensaje" class="alert in fade alert-danger" style="display:none;padding: 5px;margin: 3px;"></div>
        <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'agenda-form',
        'htmlOptions'=>array('name'=>'forma','style'=>'font-size:12px;'),
	'type' => 'horizontal',
	//'enableAjaxValidation'=>true,
        //'enableClientValidation'=>true,
	)); 
        $modelo->idsubcentro = 1416;
        echo $form->hiddenField($modelo,'fecha','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control'));
        echo $form->hiddenField($modelo,'idmedico','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control'));
        echo $form->hiddenField($modelo,'idpaciente','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control'));
        echo $form->hiddenField($modelo,'idsubcentro','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control'));
        echo $form->hiddenField($modelo,'numeroturno','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control'));
        echo $form->hiddenField($modelo,'nombre','',array('size'=>10,'maxlength'=>8,'class'=>'span5 form-control'));
        ?>
        <?php echo $form->errorSummary($modelo); ?>
    <label>Paciente:&nbsp;&nbsp;&nbsp; <span id="nombrepaciente" style="font-size: 15px;"></span></label> 
    
            <?php $tiposAgenda = CHtml::listData(Tipoagenda::model()->findAll('estado = true and idrol = 2 and idtipoagenda<=6 order by idtipoagenda'),'idtipoagenda','tipoagenda'); 
            echo $form->dropDownListGroup($modelo,'idtipoagenda',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $tiposAgenda ,),'htmlOption'=>array('class'=>'nuevo'))); ?>
            <?php //echo $form->textFieldGroup($modelo,'nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10,"autocomplete"=>"off")))); ?>		            
    
        <?php //echo $form->textFieldGroup($modelo,'idmedico',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10,"autocomplete"=>"off")))); ?>

<!--    <div class="form-group medico">
        <label class="col-sm-3 control-label required" for="Agenda_idmedico">
                Profesional de la Salud
        </label>
        <div class="col-sm-9 col-sm-9"  >
            <?php 
//            $doctores = CHtml::listData(Medico::model()->findAll('estado=true'),'idmedico','FullnameEspe'); 
//            ksort($doctores);
//            echo $form->dropDownList($modelo,'idmedico',$doctores,array('class' => 'col-sm-8 form-control',)); ?>
        </div>
    </div>-->
    <div class="calendario_ajax">
            <div class="cal"></div><div id="mask"></div>                
    </div>
        <div id="horario" class="btn-group" data-toggle="buttons" style="">
    </div>
    <div style="margin-top: 10px;">
    <?php $this->widget(
    'booster.widgets.TbButton',
    array(
    'context' => 'primary',
    'label' => 'Agendar',
    'buttonType'=>'submit',
    'context'=>'primary',
    'htmlOptions' => array(
      //  'onclick'=>'if(verificarEnvio()) forma.submit();',
    ),
    )
    ); ?>
    </div>
    <?php $this->endWidget(); 
		//FIN DE FORMULARIO	?>
    </div>
    <div id="veragenda"></div>
    <?php $this->endWidget(); ?>
    
    <?php $this->widget(
    'booster.widgets.TbButton',
    array(
    'label' => 'Click me',
    'htmlOptions' => array(
    'data-toggle' => 'modal',
    'data-target' => '#myModal',
    'style'=>'display:none;',
    'id'=>'modelo'
    ),
    )
    );
