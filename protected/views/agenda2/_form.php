<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'agenda2-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>
        
        <b><?php echo CHtml::encode($model->getAttributeLabel('nombre')); ?>:</b><br>
	<?php echo $form->textField($model,'nombre',array('class'=>'form-control','size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>
        <br>
        
	<b><?php echo CHtml::encode($model->getAttributeLabel('numeroturno')); ?>:</b><br>
	<?php echo $form->textField($model,'numeroturno',array('class'=>'form-control','size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>
        <br>
        
	<b><?php echo CHtml::encode($model->getAttributeLabel('fecha')); ?>:</b><br>
	<?php echo $form->textField($model,'fecha',array('class'=>'form-control','size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>
        <br>

<!--	<b><?php echo CHtml::encode($model->getAttributeLabel('horainicio')); ?>:</b><br>
	<?php echo $form->textField($model,'horainicio',array('size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>
        <br><br>
        
	<b><?php echo CHtml::encode($model->getAttributeLabel('horafin')); ?>:</b><br>
	<?php echo $form->textField($model,'horafin',array('size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>
        <br><br>

	<b><?php echo CHtml::encode($model->getAttributeLabel('celular')); ?>:</b><br>
	<?php echo $form->textField($model,'celular',array('size'=>85,'maxlength'=>42,'disabled'=>'disabled')); ?>
        <br><br>
  -->      
        <b><?php echo CHtml::encode($model->getAttributeLabel('atendido')); ?>:</b><br>
	<?php echo $form->checkBox($model,'atendido'); ?>
        <br><br>
       
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
