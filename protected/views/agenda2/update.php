<?php
$this->breadcrumbs=array(
	'Agenda'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Crear Cita','url'=>array('agenda/create')),
        array('label'=>'Reagendar Cita','url'=>array('agenda/reagendar')),
        array('label'=>'Detalles de Cita','url'=>array('agenda2/admin')),
	);
	?>

<h3><b>Información del Paciente</b></h3>
        

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>