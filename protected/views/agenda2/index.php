<?php
$this->breadcrumbs=array(
	'Agenda',
);

$this->menu=array(
	array('label'=>'Crear Cita','url'=>array('agenda/create')),
        array('label'=>'Reagendar Cita','url'=>array('agenda/reagendar')),
        array('label'=>'Ver Citas','url'=>array('agenda2/admin')),
	);
?>

<h1>Agendas</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
