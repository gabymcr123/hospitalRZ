﻿<?php
$this->breadcrumbs=array(
	'Profesional de la salud'=>array('admin'),
	'Lista de Profesional de la salud',
);

$this->menu=array(
//array('label'=>'Lista de  médicos','url'=>array('admin')),
array('label'=>'Crear Profesional de la salud','url'=>array('medico/create')),
array('label'=>'Permisos Horas Extras','url'=>array('agendaextra/create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('medico-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Lista Profesional de la salud</h2>


<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'medico-grid',
    'rowCssClassExpression'=>'!$data->estado ? "danger":""',
'dataProvider'=>$model->search(),
'type' => 'striped bordered condensed',
'filter'=>$model,
'columns'=>array(
		//'idmedico',
		//'idespecialidad',
		array(
		   'name'=>'idespecialidad',
		   'value'=>'$data->idespecialidad1->especialidad',
		   'filter'=>CHtml::activeTextField($model, 'especialidad', array('class'=>'form-control')),
		   
		),
		'nombre',
		'apellido',
		'cedula',
		'telefono',
		array(
		'class'=>'CLinkColumn',
		//'header'=>'Porcentajes',
                'htmlOptions'=>array('title'=>'Horario'),
                'urlExpression'=>'Yii::app()->createUrl("dia/update", array("idmedico"=>$data->idmedico))',
                'imageUrl'=>Yii::app()->request->baseUrl.'/images/hora.png',
		),
                array(
               'class' => 'booster.widgets.TbToggleColumn',
               'toggleAction' => 'medico/delete',
               'name' => 'estado',
               'uncheckedButtonLabel'=>'Activar',
               'checkedButtonLabel'=>'Desactivar',
               //'header' => 'Toggle',
               //'value'=>'$data->estado ? 1 : 0',
               'filter'=>'',
               ),
               array(
                'class'=>'booster.widgets.TbButtonColumn',
                'template'=>'{porcentaje} {update} {view} ',
                'buttons'=>array
                (           
                'porcentaje' => array
                (
                    'title'=>'porcentaje',
                    'imageUrl'=>'images/porcentaje.gif',
                    'url'=>'Yii::app()->createUrl("porcentajes/admin",array("id"=>$data->idmedico))',
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png', 
                ),
                )
		),

),
)); ?>
