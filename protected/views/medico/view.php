﻿<?php
$this->breadcrumbs=array(
	'Profesional de la salud'=>array('admin'),
	$model->nombre.' '.$model->apellido,
);

$this->menu=array(
array('label'=>'Lista de Profesional de la salud','url'=>array('admin')),
array('label'=>'Crear Profesional de la salud','url'=>array('medico/create')),
array('label'=>'Modificar Profesional de la salud','url'=>array('update','id'=>$model->idmedico)),
array('label'=>'Eliminar médico','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idmedico),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Administrar médicos','url'=>array('admin')),
);
?>

<h2> Profesional </h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'idmedico',
		array(
			'label'=>'Especialidad',
			'type'=>'raw',
			'value'=>$model->idespecialidad1->especialidad),
		'nombre',
		'apellido',
		'cedula',
		'telefono',
                'intervalo',
                array('label'=>'Usuario',
                    'value'=>$model->pkusuario->usuario
                    ),
                    array('label'=>'Contraseña',
                         'type'=>'raw',
                         'value'=>'<span id="contra">*******</span>'.' '.CHtml::CheckBox('verpass',false, array('value'=>$model->pkusuario->password)).'<span style="font-size:10px;color:green;"><strong>Ver</strong></span>'
                    ),
		//'creadopor',
		//'modificadopor',
		//'fechacreado',
		//'fechamodificado',
  
			
),
)); ?>
<script type="text/javascript">
    $('#verpass').change(function (){
        if(this.checked)
            $('#contra').html(this.value);
        else
            $('#contra').html('*******');
    });
</script>
