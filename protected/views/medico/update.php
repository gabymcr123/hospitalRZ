﻿<?php
$this->breadcrumbs=array(
	'Profesional de la salud'=>array('admin'),
	//$model->nombre.' '.$model->apellido=>array('view','id'=>$model->idmedico),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Profesional de la salud','url'=>array('admin')),
	array('label'=>'Crear Profesional de la salud','url'=>array('usuario/create')),
	array('label'=>'Detalles de Profesional de la salud','url'=>array('view','id'=>$model->idmedico)),
	//array('label'=>'Administrar médicos','url'=>array('admin')),
	);
	?>

	<h2>Actualizar Profesional de la salud <?php echo $model->nombre.' '.$model->apellido; ?></h2>

<?php echo $this->renderPartial('_form_1',array('model'=>$model,'modelDi'=>$modelDi,'modelU'=>$modelU)); ?>