<div class="view">
    <?php /*
		<b><?php echo CHtml::encode($data->getAttributeLabel('idmedico')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idmedico),array('view','id'=>$data->idmedico)); ?>
	<br />
	*/ ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('idespecialidad')); ?>:</b>
	<?php echo CHtml::encode($data->idespecialidad1->especialidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido')); ?>:</b>
	<?php echo CHtml::encode($data->apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula')); ?>:</b>
	<?php echo CHtml::encode($data->cedula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />
    <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('creadopor')); ?>:</b>
	<?php echo CHtml::encode($data->creadopor); ?>
	<br />
	*/ ?>

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('modificadopor')); ?>:</b>
	<?php echo CHtml::encode($data->modificadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacreado')); ?>:</b>
	<?php echo CHtml::encode($data->fechacreado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechamodificado')); ?>:</b>
	<?php echo CHtml::encode($data->fechamodificado); ?>
	<br />

	*/ ?>

</div>
<hr/>