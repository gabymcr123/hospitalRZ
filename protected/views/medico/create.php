﻿<?php
$this->breadcrumbs=array(
	'Profesional de la salud'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de profesional de la salud','url'=>array('admin')),
//array('label'=>'Administrar médicos','url'=>array('admin')),
);
?>

<h2>Crear profesional de la salud</h2>

<?php $this->renderPartial('_form', array('model'=>$model,'modelc'=>$modelc)); ?>
