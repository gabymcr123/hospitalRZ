<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'medico-form',
        'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Los campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->errorSummary($modelDi); ?>
<?php echo $form->errorSummary($modelU); ?>

                    <b>Datos de usuario</b>
                    <br>
                    <?php echo $form->textFieldGroup($modelU,'usuario',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>

	<?php echo $form->passwordFieldGroup($modelU,'password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
                    <?php echo $form->passwordFieldGroup($modelU,'repeat_password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
                    <hr>
                    <b>Datos del médico</b>
<br>


                    <?php $espe = CHtml::listData(Especialidadmedica::model()->findAll('estado = true'),'idespecialidad','especialidad'); ?>
                    <?php echo $form->dropDownListGroup($model, 'idespecialidad',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $espe))); ?>

	<?php echo $form->textFieldGroup($model,'nombre',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>

	<?php echo $form->textFieldGroup($model,'apellido',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>

	<?php echo $form->textFieldGroup($model,'cedula',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

	<?php echo $form->textFieldGroup($model,'telefono',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>
        
<?php echo $form->textFieldGroup($model,'intervalo',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>
                    
                 
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
