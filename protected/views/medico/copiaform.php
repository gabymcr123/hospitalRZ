<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'medico-form',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        )); ?>

<p class="help-block">Los campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->errorSummary($modelDi); ?>


                    <?php $espe = CHtml::listData(Especialidadmedica::model()->findAll('estado = true'),'idespecialidad','especialidad'); ?>
                    <?php echo $form->dropDownListGroup($model, 'idespecialidad',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),'widgetOptions' => array('data' => $espe))); ?>

	<?php echo $form->textFieldGroup($model,'nombre',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textFieldGroup($model,'apellido',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textFieldGroup($model,'cedula',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textFieldGroup($model,'telefono',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

                    <!--Horario-->
                    
                    <fieldset>
                <legend>Horario</legend>
                        <input type="checkbox" name="horario" value="Lunes"id="dia1" onclick="ActivarCampoOtroTema();" />Lunes <br />
                        <div id="horLunes" style="display:none; " >
                           

<input type="number" name="age" id="age"
                min="1990" max="2030" step="1" value="2013" onchange ='anio(this.value,"a1")'>


<input type="number" name="age" id="age2"
                min="1990" max="2030" step="1" value="2014" onchange ='anio(this.value,"a2")'>

                        </div>
                        <input type="checkbox" name="horario" value="Martes" id="dia2" onclick="ActivarCampoOtroTema();"/>Martes<br />
                        <div id="horMartes" style="display:none;" >
                            
                        </div>
                        <input type="checkbox" name="horario" value="Miercoles" id="dia3" onclick="ActivarCampoOtroTema();"/>Miercoles<br />
                        <div id="horMiercoles" style="display:none;" >
                            
                        </div>
                        <input type="checkbox" name="horario" value="Miercoles" id="dia4" onclick="ActivarCampoOtroTema();" />Jueves<br />
                        <div id="horJueves" style="display:none;">
                            
                        </div>
                        <input type="checkbox" name="horario" value="Miercoles" id="dia5" onclick="ActivarCampoOtroTema();"/>Viernes<br />
                        <div id="horViernes" style="display:none;" >
                            
                        </div>
                       
        </fieldset>

                    
                    <?php echo $form->checkBoxGroup($modelDi,'lunes'); ?>

	<?php echo $form->checkBoxGroup($modelDi,'martes'); ?>

	<?php echo $form->checkBoxGroup($modelDi,'miercoles'); ?>

	<?php echo $form->checkBoxGroup($modelDi,'jueves'); ?>

	<?php echo $form->checkBoxGroup($modelDi,'viernes'); ?>

	<?php echo $form->checkBoxGroup($modelDi,'estado'); ?>
                    
                    <?php echo $form->textFieldGroup($modelDi,'horasatencion',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($modelDi,'intervaloconsulta',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
