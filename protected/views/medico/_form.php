<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
    'type'=>'horizontal',
	'id'=>'medico-form',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'clientOptions' => array(
       // 'validateOnSubmit' => true,
    ),
        )); ?>

<p class="help-block">Los campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->errorSummary($modelc); ?>

<!-- USUARIO -->

<?php echo $form->textFieldGroup($modelc,'usuario',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

<?php echo $form->passwordFieldGroup($modelc,'password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
<?php echo $form->passwordFieldGroup($modelc,'repeat_password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>

<!-- FIN USUARIO -->
<!-- MEDICO -->
        
        <?php echo $form->textFieldGroup($model,'titulo',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>
	<?php echo $form->textFieldGroup($modelc,'nombre',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textFieldGroup($modelc,'apellido',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','maxlength'=>35,'style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textFieldGroup($model,'cedula',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>

	<?php echo $form->textFieldGroup($model,'telefono',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>
        <?php $espe = CHtml::listData(Especialidadmedica::model()->findAll('estado = true ORDER BY especialidad'),'idespecialidad','especialidad'); ?>
<div class="form-group">
    <label class="col-sm-3 control-label required" >Especialidad <span class="required"> *</span></label>
    <div class="col-sm-5">
        <?php echo $form->dropdownList($model,'idespecialidad', $espe, array('prompt'=>'--Seleccione--','class'=>'form-control')); ?><br>
        </div>
       </div>
             
             <?php echo $form->textFieldGroup($model,'intervalo',array('widgetOptions'=>array('htmlOptions'=>array('autocomplete'=>'off','style'=>'text-transform:uppercase','onblur'=>'this.value=this.value.toUpperCase()')))); ?>       
   
<!-- FIN MEDICO -->
<div class="form-actions">
    
    
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    
    
    $('#Medico_idespecialidad').change(function (){
		
		var idespecialidad = $(this).val(); // el "value" de ese <option> seleccionado
		if(idespecialidad == 0) {
		return;
		}
		var action = 'index.php?r=medico/obtenerintervalo';

		$.ajax({
                    type: 'GET',
                    url: action,
                    cache: false,
                    data : {idespecialidad:idespecialidad}}
                        ).done(function(resp)
                {
                    $('#Medico_intervalo').val(resp);
                }        
                );
		});
</script>