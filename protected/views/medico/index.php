﻿<?php
$this->breadcrumbs=array(
	'Medicos',
);

$this->menu=array(
array('label'=>'Crear médico','url'=>array('medico/create')),
array('label'=>'Administrar médico','url'=>array('admin')),
);
?>

<h2>Médicos</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
