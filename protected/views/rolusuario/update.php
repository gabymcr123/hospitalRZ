<?php
$this->breadcrumbs=array(
	'Rolusuarios'=>array('index'),
	$model->idrol=>array('view','id'=>$model->idrol),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Rolusuario','url'=>array('index')),
	array('label'=>'Create Rolusuario','url'=>array('create')),
	array('label'=>'View Rolusuario','url'=>array('view','id'=>$model->idrol)),
	array('label'=>'Manage Rolusuario','url'=>array('admin')),
	);
	?>

	<h1>Update Rolusuario <?php echo $model->idrol; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>