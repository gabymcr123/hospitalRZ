<?php
$this->breadcrumbs=array(
	'Rolusuarios'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Rolusuario','url'=>array('index')),
array('label'=>'Manage Rolusuario','url'=>array('admin')),
);
?>

<h1>Create Rolusuario</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>