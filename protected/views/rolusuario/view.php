<?php
$this->breadcrumbs=array(
	'Rolusuarios'=>array('index'),
	$model->idrol,
);

$this->menu=array(
array('label'=>'List Rolusuario','url'=>array('index')),
array('label'=>'Create Rolusuario','url'=>array('create')),
array('label'=>'Update Rolusuario','url'=>array('update','id'=>$model->idrol)),
array('label'=>'Delete Rolusuario','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idrol),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Rolusuario','url'=>array('admin')),
);
?>

<h1>View Rolusuario #<?php echo $model->idrol; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'idrol',
		'rol',
		'estado',
		'creadopor',
		'modificadopor',
		'fechacreado',
		'fechamodificado',
),
)); ?>
