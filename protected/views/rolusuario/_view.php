<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('idrol')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idrol),array('view','id'=>$data->idrol)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rol')); ?>:</b>
	<?php echo CHtml::encode($data->rol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creadopor')); ?>:</b>
	<?php echo CHtml::encode($data->creadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modificadopor')); ?>:</b>
	<?php echo CHtml::encode($data->modificadopor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechacreado')); ?>:</b>
	<?php echo CHtml::encode($data->fechacreado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechamodificado')); ?>:</b>
	<?php echo CHtml::encode($data->fechamodificado); ?>
	<br />


</div>