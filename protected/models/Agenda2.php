<?php

/**
 * This is the model class for table "agendamiento.agenda".
 *
 * The followings are the available columns in table 'agendamiento.agenda':
 * @property integer $idagenda
 * @property integer $idtipoagenda
 * @property integer $idpaciente
 * @property integer $idmedico
 * @property integer $idsubcentro
 * @property string $nombre
 * @property integer $numeroturno
 * @property string $fecha
 * @property string $horainicio
 * @property string $horafin
 * @property boolean $estado
 * @property integer $idreferencia
 * @property string $telefono
 * @property string $celular
 * @property boolean $atendido
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 * @property integer $reagendado
 *
 * The followings are the available model relations:
 * @property Medico $idmedico
 * @property Paciente $idpaciente
 * @property Subcentro $idsubcentro
 * @property Tipoagenda $idtipoagenda
 */
class Agenda2 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.agenda';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, fecha', 'required'),
			//array('idtipoagenda, idpaciente, idmedico, idsubcentro, numeroturno, idreferencia, reagendado', 'numerical', 'integerOnly'=>true),
			//Array('nombre, creadopor, modificadopor', 'length', 'max'=>50),
			//array('telefono, celular', 'length', 'max'=>15),
			array('horainicio, horafin, estado, atendido, fechacreado, fechamodificado,idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, fecha,numeroturno, idreferencia, reagendado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idagenda, idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, numeroturno, fecha, horainicio, horafin, estado, idreferencia, telefono, celular, atendido, creadopor, modificadopor, fechacreado, fechamodificado, reagendado,nombredoc,nombresub,nombretipoag,nombrepac', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idmedico1' => array(self::BELONGS_TO, 'Medico', 'idmedico'),
			'idpaciente1' => array(self::BELONGS_TO, 'Paciente', 'idpaciente'),
			'idsubcentro1' => array(self::BELONGS_TO, 'Subcentro', 'idsubcentro'),
			'idtipoagenda1' => array(self::BELONGS_TO, 'Tipoagenda', 'idtipoagenda'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idagenda' => 'Idagenda',
			'idtipoagenda' => 'Tipo de Agenda',
			'idpaciente' => 'Idpaciente',
			'idmedico' => 'Medico',
			'idsubcentro' => 'Subcentro',
			'nombre' => 'Paciente',
			'numeroturno' => 'Número de Turno',
			'fecha' => 'Fecha',
			'horainicio' => 'Hora de Inicio',
			'horafin' => 'Hora de Fin',
			'estado' => 'Estado',
			'idreferencia' => 'Idreferencia',
			'telefono' => 'Telefono',
			'celular' => 'Celular',
			'atendido' => 'Atendido',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
			'reagendado' => 'Reagendado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
        public $nombredoc;
        public $nombresub;
        public $nombrepac;
        public $nombretipoag;
        public $fecha_hoy;
        
	public function search($f)
	{
                $med = Yii::app()->db->createCommand('select idmedico,idespecialidad from agendamiento.medico where idusuario='.Yii::app()->user->id)->queryRow();
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

                $criteria->compare('idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('t.nombre',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
                //SE FILTRA LOS PACIENTES DE LA FECHA DE HOY
                //if($med['idespecialidad']==11):
                  //   $criteria->compare('idmedico1.idespecialidad',$med['idespecialidad']);
//                else:
       $criteria->compare('t.idmedico',$med['idmedico']);
    //            endif;
                $this->fecha_hoy = $f;
		$criteria->addBetweenCondition('fecha', ''.$this->fecha_hoy.'', ''.$this->fecha_hoy.'');
                $criteria->with = array('idmedico1','idsubcentro1','idpaciente1','idtipoagenda1');
                $criteria->addSearchCondition("UPPER(idmedico1.nombre ||' '||idmedico1.apellido)",strtoupper($this->nombredoc), true);
                $criteria->addSearchCondition('UPPER(idsubcentro1.subcentro)',strtoupper($this->nombresub), true);
                $criteria->addSearchCondition('UPPER(idpaciente1.nombre)',strtoupper($this->nombrepac), true);
                $criteria->addSearchCondition('UPPER(idtipoagenda1.tipoagenda)',strtoupper($this->nombretipoag), true);
                $criteria->order = ' t.idmedico, numeroturno ASC';
                
//                $sql = 'select rolusuario.rol from agendamiento.usuario inner join agendamiento.rolusuario on usuario.idrol = rolusuario.idrol where idusuario ='.Yii::app()->user->id;
//                $tipo = Yii::app()->db->createCommand($sql)->queryScalar();
//                $idusuario = Yii::app()->user->id;
//                if($tipo =="MEDICO")
//                {
//                    $criteria->compare('idmedico1.idusuario',$idusuario);
//                }                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Agenda2 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
