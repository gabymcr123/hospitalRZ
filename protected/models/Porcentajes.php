<?php

/**
 * This is the model class for table "agendamiento.porcentajes".
 *
 * The followings are the available columns in table 'agendamiento.porcentajes':
 * @property integer $idporcentaje
 * @property integer $idenmedico
 * @property integer $identipoagenda
 * @property integer $porcentaje
 * @property integer $valor
 * @property boolean $estado
 * @property integer $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Usuario $creadopor
 * @property Medico $idenmedico
 * @property Usuario $modificadopor
 * @property Tipoagenda $identipoagenda
 */

class Porcentajes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.porcentajes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idenmedico, identipoagenda, porcentaje, valor, valorlunes, valormartes, valormiercoles, valorjueves, valorviernes,creadopor, modificadopor', 'numerical', 'integerOnly'=>true),
			array('estado, fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idporcentaje, idenmedico, identipoagenda, porcentaje, valor, estado, creadopor, modificadopor, fechacreado, fechamodificado,nombredoc,agenda', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'creadopor' => array(self::BELONGS_TO, 'Usuario', 'creadopor'),
			'medico' => array(self::BELONGS_TO, 'Medico', 'idenmedico'),
			'modificadopor' => array(self::BELONGS_TO, 'Usuario', 'modificadopor'),
			'tipoagenda' => array(self::BELONGS_TO, 'Tipoagenda', 'identipoagenda'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idporcentaje' => 'Idporcentaje',
			'idenmedico' => 'Profesional de la Salud',
			'identipoagenda' => 'Tipo de Agenda',
			'porcentaje' => 'Porcentaje %',
			'valor' => 'Valor',
			'estado' => 'Estado',
			'creadopor' => 'Creado Por',
			'modificadopor' => 'Modificado Por',
			'fechacreado' => 'Fecha Creado',
			'fechamodificado' => 'Fecha Modificado',
                    'valorlunes' => 'Turnos Lunes',
			'valormartes' => 'Turnos Martes',
			'valormiercoles' => 'Turnos Miercoles',
			'valorjueves' => 'Turnos Jueves',
			'valorviernes' => 'Turnos Viernes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */

	public $nombredoc;
	public $agenda;
	public $id;
	public function search($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.


		$criteria=new CDbCriteria;


		$criteria->compare('idporcentaje',$this->idporcentaje);
		$criteria->compare('idenmedico',$id);
		$criteria->compare('UPPER(t.identipoagenda)',strtoupper($this->identipoagenda));
		$criteria->compare('porcentaje',$this->porcentaje);
		$criteria->compare('valor',$this->valor);
		//$criteria->compare('t.estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor);
		$criteria->compare('modificadopor',$this->modificadopor);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);

		$criteria->with = array ('medico','tipoagenda');
        $criteria->addSearchCondition("medico.nombre ||' '|| medico.apellido", $this->nombredoc, true);
        $criteria->addSearchCondition("tipoagenda.tipoagenda", $this->agenda, true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Porcentajes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
