<?php

/**
 * This is the model class for table "agendamiento.dia".
 *
 * The followings are the available columns in table 'agendamiento.dia':
 * @property integer $iddia
 * @property integer $idmedico
 
 * @property string $dia
 * @property boolean $estado
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 * @property string $horainicio
 * @property string $horafin
 *
 * The followings are the available model relations:
 * @property Medico $idmedico
 */
class Dia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.dia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idmedico, dia, horainicio, horafin', 'required'),
			array('idmedico', 'numerical', 'integerOnly'=>true),
			array('dia', 'length', 'max'=>10),
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('estado, fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('iddia, idmedico, dia, estado, creadopor, modificadopor, fechacreado, fechamodificado, horainicio, horafin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idmedico' => array(self::BELONGS_TO, 'Medico', 'idmedico'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'iddia' => 'Iddia',
			'idmedico' => 'Profesional de la Salud',
			
			'dia' => 'Dia',
			'estado' => 'Estado',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
			'horainicio' => 'Horainicio',
			'horafin' => 'Horafin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('iddia',$this->iddia);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('numhora',$this->numhora);
		$criteria->compare('dia',$this->dia,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
