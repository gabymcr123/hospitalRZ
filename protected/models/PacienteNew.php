<?php

/**
 * This is the model class for table "agendamiento.paciente".
 *
 * The followings are the available columns in table 'agendamiento.paciente':
 * @property integer $id
 * @property string $numero_archivo
 * @property string $cedula
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $historia_clinica_temporal
 * @property string $nombre_padre
 * @property string $apellido_padre
 * @property string $nombre_madre
 * @property string $apellido_madre
 * @property string $fecha_nacimiento
 * @property string $lugar_nacimiento
 * @property string $nacionalidad
 * @property integer $sexo
 * @property integer $grupo_cultural
 * @property integer $estado_civil
 * @property string $telefono
 * @property string $celular
 * @property boolean $discapacitado
 * @property string $discapacidad_predomina
 * @property string $instruccion
 * @property string $ocupacion
 * @property string $empresa
 * @property integer $tipo_seguro
 * @property string $fecha_admision
 * @property string $provincia
 * @property string $canton
 * @property string $parroquia
 * @property string $barrio
 * @property string $zona
 * @property string $direccion
 * @property string $direccion2
 * @property string $emergencia_llamar
 * @property string $nombre_refenrecia
 * @property string $parentesco
 * @property string $direccion_parentesco
 * @property string $telefono_parentesco
 * @property string $usuario_creador
 * @property string $usuario_modificador
 * @property string $fecha_creado
 * @property string $fecha_modificado
 * @property integer $estado
 * @property integer $nmod
 * @property boolean $calta
 * @property string $date02
 * @property string $res01
 * @property string $prm_01
 * @property double $na
 *
 * The followings are the available model relations:
 * @property Agenda[] $agendas
 * @property Caso[] $casos
 */
class PacienteNew extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.paciente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sexo, grupo_cultural, estado_civil, tipo_seguro, estado, nmod', 'numerical', 'integerOnly'=>true),
			array('na', 'numerical'),
			array('cedula, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, historia_clinica_temporal, nombre_padre, apellido_padre, nombre_madre, apellido_madre, lugar_nacimiento, nacionalidad, telefono, celular, discapacidad_predomina, instruccion, ocupacion, empresa, provincia, canton, parroquia, barrio, zona, direccion, direccion2, emergencia_llamar, nombre_refenrecia, parentesco, direccion_parentesco, telefono_parentesco, usuario_creador, usuario_modificador, prm_01', 'length', 'max'=>100),
			array('res01', 'length', 'max'=>6),
			array('numero_archivo, fecha_nacimiento, discapacitado, fecha_admision, fecha_creado, fecha_modificado, calta, date02', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero_archivo, cedula, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, historia_clinica_temporal, nombre_padre, apellido_padre, nombre_madre, apellido_madre, fecha_nacimiento, lugar_nacimiento, nacionalidad, sexo, grupo_cultural, estado_civil, telefono, celular, discapacitado, discapacidad_predomina, instruccion, ocupacion, empresa, tipo_seguro, fecha_admision, provincia, canton, parroquia, barrio, zona, direccion, direccion2, emergencia_llamar, nombre_refenrecia, parentesco, direccion_parentesco, telefono_parentesco, usuario_creador, usuario_modificador, fecha_creado, fecha_modificado, estado, nmod, calta, date02, res01, prm_01, na', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agendas' => array(self::HAS_MANY, 'Agenda', 'idpaciente'),
			'casos' => array(self::HAS_MANY, 'Caso', 'idpaciente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero_archivo' => 'Numero Archivo',
			'cedula' => 'Cedula',
			'primer_apellido' => 'Primer Apellido',
			'segundo_apellido' => 'Segundo Apellido',
			'primer_nombre' => 'Primer Nombre',
			'segundo_nombre' => 'Segundo Nombre',
			'historia_clinica_temporal' => 'Historia Clinica Temporal',
			'nombre_padre' => 'Nombre Padre',
			'apellido_padre' => 'Apellido Padre',
			'nombre_madre' => 'Nombre Madre',
			'apellido_madre' => 'Apellido Madre',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'lugar_nacimiento' => 'Lugar Nacimiento',
			'nacionalidad' => 'Nacionalidad',
			'sexo' => 'Sexo',
			'grupo_cultural' => 'Grupo Cultural',
			'estado_civil' => 'Estado Civil',
			'telefono' => 'Telefono',
			'celular' => 'Celular',
			'discapacitado' => 'Discapacitado',
			'discapacidad_predomina' => 'Discapacidad Predomina',
			'instruccion' => 'Instruccion',
			'ocupacion' => 'Ocupacion',
			'empresa' => 'Empresa',
			'tipo_seguro' => 'Tipo Seguro',
			'fecha_admision' => 'Fecha Admision',
			'provincia' => 'Provincia',
			'canton' => 'Canton',
			'parroquia' => 'Parroquia',
			'barrio' => 'Barrio',
			'zona' => 'Zona',
			'direccion' => 'Direccion',
			'direccion2' => 'Direccion2',
			'emergencia_llamar' => 'Emergencia Llamar',
			'nombre_refenrecia' => 'Nombre Refenrecia',
			'parentesco' => 'Parentesco',
			'direccion_parentesco' => 'Direccion Parentesco',
			'telefono_parentesco' => 'Telefono Parentesco',
			'usuario_creador' => 'Usuario Creador',
			'usuario_modificador' => 'Usuario Modificador',
			'fecha_creado' => 'Fecha Creado',
			'fecha_modificado' => 'Fecha Modificado',
			'estado' => 'Estado',
			'nmod' => 'Nmod',
			'calta' => 'Calta',
			'date02' => 'Date02',
			'res01' => 'Res01',
			'prm_01' => 'Prm 01',
			'na' => 'Na',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero_archivo',$this->numero_archivo,true);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('primer_apellido',$this->primer_apellido,true);
		$criteria->compare('segundo_apellido',$this->segundo_apellido,true);
		$criteria->compare('primer_nombre',$this->primer_nombre,true);
		$criteria->compare('segundo_nombre',$this->segundo_nombre,true);
		$criteria->compare('historia_clinica_temporal',$this->historia_clinica_temporal,true);
		$criteria->compare('nombre_padre',$this->nombre_padre,true);
		$criteria->compare('apellido_padre',$this->apellido_padre,true);
		$criteria->compare('nombre_madre',$this->nombre_madre,true);
		$criteria->compare('apellido_madre',$this->apellido_madre,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('lugar_nacimiento',$this->lugar_nacimiento,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('sexo',$this->sexo);
		$criteria->compare('grupo_cultural',$this->grupo_cultural);
		$criteria->compare('estado_civil',$this->estado_civil);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('celular',$this->celular,true);
		$criteria->compare('discapacitado',$this->discapacitado);
		$criteria->compare('discapacidad_predomina',$this->discapacidad_predomina,true);
		$criteria->compare('instruccion',$this->instruccion,true);
		$criteria->compare('ocupacion',$this->ocupacion,true);
		$criteria->compare('empresa',$this->empresa,true);
		$criteria->compare('tipo_seguro',$this->tipo_seguro);
		$criteria->compare('fecha_admision',$this->fecha_admision,true);
		$criteria->compare('provincia',$this->provincia,true);
		$criteria->compare('canton',$this->canton,true);
		$criteria->compare('parroquia',$this->parroquia,true);
		$criteria->compare('barrio',$this->barrio,true);
		$criteria->compare('zona',$this->zona,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('direccion2',$this->direccion2,true);
		$criteria->compare('emergencia_llamar',$this->emergencia_llamar,true);
		$criteria->compare('nombre_refenrecia',$this->nombre_refenrecia,true);
		$criteria->compare('parentesco',$this->parentesco,true);
		$criteria->compare('direccion_parentesco',$this->direccion_parentesco,true);
		$criteria->compare('telefono_parentesco',$this->telefono_parentesco,true);
		$criteria->compare('usuario_creador',$this->usuario_creador,true);
		$criteria->compare('usuario_modificador',$this->usuario_modificador,true);
		$criteria->compare('fecha_creado',$this->fecha_creado,true);
		$criteria->compare('fecha_modificado',$this->fecha_modificado,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('nmod',$this->nmod);
		$criteria->compare('calta',$this->calta);
		$criteria->compare('date02',$this->date02,true);
		$criteria->compare('res01',$this->res01,true);
		$criteria->compare('prm_01',$this->prm_01,true);
		$criteria->compare('na',$this->na);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PacienteNew the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
