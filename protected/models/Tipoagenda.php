<?php

/**
 * This is the model class for table "agendamiento.tipoagenda".
 *
 * The followings are the available columns in table 'agendamiento.tipoagenda':
 * @property integer $idtipoagenda
 * @property string $tipoagenda
 * @property boolean $estado
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 * @property integer $idrol
 *
 * The followings are the available model relations:
 * @property Rolusuario $idrol
 * @property Agenda[] $agendas
 * @property Porcentajes[] $porcentajes
 * @property Agendaextra[] $agendaextras
 */
class Tipoagenda extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.tipoagenda';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipoagenda', 'required'),
			array('idrol', 'numerical', 'integerOnly'=>true),
			array('tipoagenda', 'length', 'max'=>30),
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('estado, fechacreado, fechamodificado', 'safe'),
                        array('tipoagenda', 'unique','message'=>'El tipo de agenda ya existe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idtipoagenda, tipoagenda, estado, creadopor, modificadopor, fechacreado, fechamodificado, idrol', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idroles' => array(self::BELONGS_TO, 'Rolusuario', 'idrol'),
			'agendas' => array(self::HAS_MANY, 'Agenda', 'idtipoagenda'),
			'porcentajes' => array(self::HAS_MANY, 'Porcentajes', 'identipoagenda'),
			'agendaextras' => array(self::HAS_MANY, 'Agendaextra', 'idtipo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idtipoagenda' => 'Idtipoagenda',
			'tipoagenda' => 'Tipo De Agenda',
			'estado' => 'Estado',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
			'idrol' => 'Rol De Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.idtipoagenda',$this->idtipoagenda);
		$criteria->compare('UPPER(tipoagenda)',strtoupper($this->tipoagenda),true);
		//$criteria->compare('estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order = 't.idtipoagenda ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tipoagenda the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
