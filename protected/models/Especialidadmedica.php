<?php

/**
 * This is the model class for table "agendamiento.especialidadmedica".
 *
 * The followings are the available columns in table 'agendamiento.especialidadmedica':
 * @property integer $idespecialidad
 * @property integer $idserviciomedico
 * @property string $especialidad
 * @property integer $valorcita
 * @property boolean $estado
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Medico[] $medicos
 * @property Serviciomedico $idserviciomedico
 * @property Porcentaje[] $porcentajes
 */
class Especialidadmedica extends CActiveRecord
{
    public $serviciomedico;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.especialidadmedica';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idserviciomedico,intervalo, especialidad', 'required'),
			array('idserviciomedico', 'numerical', 'integerOnly'=>true),
			array('especialidad', 'length', 'max'=>30),
                        array('especialidad','unique', 'message' => 'Esta especialidad ya existe'),	
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('estado, fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idespecialidad,intervalo, idserviciomedico, especialidad, estado, creadopor, modificadopor, fechacreado, fechamodificado,serviciomedico', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'medicos' => array(self::HAS_MANY, 'Medico', 'idespecialidad'),
                        'idserviciomedico' => array(self::BELONGS_TO, 'Serviciomedico', 'idserviciomedico'),
			'porcentajes' => array(self::HAS_MANY, 'Porcentaje', 'idespecialidad'),
                    'idserviciomedico1' => array(self::BELONGS_TO, 'Serviciomedico', 'idserviciomedico'),
			
                        
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idespecialidad' => 'Especialidad',
			'idserviciomedico' => 'Servicio Médico',
			'intervalo'=>'Intervalo',
			
			'estado' => 'Estado',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idespecialidad',$this->idespecialidad);
		$criteria->compare('t.idserviciomedico',$this->idserviciomedico);
                $criteria->with = array ('idserviciomedico1');
                $criteria->addSearchCondition('UPPER(idserviciomedico1.serviciomedico)', strtoupper($this->serviciomedico), true);
		
               
		$criteria->compare('UPPER(especialidad)',  strtoupper($this->especialidad),true);
		

		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
                //$criteria->compare('t.estado',true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order = 'especialidad ASC';
                return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Especialidadmedica the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
