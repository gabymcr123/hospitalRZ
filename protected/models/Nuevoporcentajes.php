<?php

/**
 * This is the model class for table "agendamiento.nuevoporcentajes".
 *
 * The followings are the available columns in table 'agendamiento.nuevoporcentajes':
 * @property integer $idnuevo
 * @property integer $idmedico
 * @property string $fechainicio
 * @property string $fechafin
 * @property integer $valorreferencia
 * @property integer $valorsubcecuente
 * @property string $creadopor
 * @property string $fechacreado
 *
 * The followings are the available model relations:
 * @property Medico $idmedico
 */
class Nuevoporcentajes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.nuevoporcentajes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idmedico, valorreferencia, valorsubcecuente', 'numerical', 'integerOnly'=>true),
			array('creadopor', 'length', 'max'=>30),
			array('fechainicio, fechafin, fechacreado', 'safe'),
                        array('idmedico, valorreferencia, valorsubcecuente, fechainicio, fechafin,','required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idnuevo, idmedico, fechainicio, fechafin, valorreferencia, valorsubcecuente, creadopor, fechacreado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idmedico' => array(self::BELONGS_TO, 'Medico', 'idmedico'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idnuevo' => 'Idnuevo',
			'idmedico' => 'Profesional de la salud',
			'fechainicio' => 'Fecha de inicio',
			'fechafin' => 'Fecha de fin',
			'valorreferencia' => 'Porcentaje referencia',
			'valorsubcecuente' => 'Porcentaje subcecuente',
			'creadopor' => 'Creadopor',
			'fechacreado' => 'Fechacreado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idnuevo',$this->idnuevo);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('fechainicio',$this->fechainicio,true);
		$criteria->compare('fechafin',$this->fechafin,true);
		$criteria->compare('valorreferencia',$this->valorreferencia);
		$criteria->compare('valorsubcecuente',$this->valorsubcecuente);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
                $criteria->order = 'fechacreado Desc';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Nuevoporcentajes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
