﻿<?php

/**
 * This is the model class for table "agendamiento.serviciomedico".
 *
 * The followings are the available columns in table 'agendamiento.serviciomedico':
 * @property integer $idserviciomedico
 * @property string $serviciomedico
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Especialidadmedica[] $especialidadmedicas
 */
class Serviciomedico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.serviciomedico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('serviciomedico', 'required'),
			array('serviciomedico', 'length', 'max'=>30),
			array('creadopor, modificadopor', 'length', 'max'=>50),
                        array('serviciomedico','unique', 'message' => 'Este servicio médico ya existe'),
			array('fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idserviciomedico, serviciomedico, creadopor, modificadopor, fechacreado, fechamodificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'especialidadmedicas' => array(self::HAS_MANY, 'Especialidadmedica', 'idserviciomedico'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idserviciomedico' => 'Idserviciomedico',
			'serviciomedico' => 'Servicio médico',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idserviciomedico',$this->idserviciomedico);
		$criteria->compare('UPPER(serviciomedico)',strtoupper($this->serviciomedico),true);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
                //$criteria->compare('t.estado',true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order = 't.serviciomedico ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Serviciomedico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
