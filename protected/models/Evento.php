<?php

/**
 * This is the model class for table "agendamiento.evento".
 *
 * The followings are the available columns in table 'agendamiento.evento':
 * @property integer $idevento
 * @property integer $idconcepto
 * @property string $descripcion
 * @property string $fechainicio
 * @property string $fechafin
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 * @property integer $idmedico
 * @property boolean $hora
 *
 * The followings are the available model relations:
 * @property Medico[] $medicos
 * @property Concepto $idconcepto

 */
class Evento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.evento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idconcepto, fechainicio, fechafin,idmedico', 'required'),
			array('idconcepto, idmedico', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>30),
                        
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('hora,fechacreado, fechamodificado, estado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idevento, idconcepto, descripcion, fechainicio, fechafin, creadopor, modificadopor, fechacreado, fechamodificado, idmedico, nombredoc,concepto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idconcepto1' => array(self::BELONGS_TO, 'Concepto', 'idconcepto'),
                        'medico' => array(self::BELONGS_TO, 'Medico', 'idmedico'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idevento' => 'Idevento',
			'idconcepto' => 'Concepto',
			'descripcion' => 'Descripción',
			'fechainicio' => 'Fecha Inicio',
			'fechafin' => 'Fecha Fin',
			'creadopor' => 'Creadopor',
                        'hora' => 'Turnos ',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
                        'idmedico' => 'Profesional de la salud',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
        public $nombredoc;
		public $concepto;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idevento',$this->idevento);
		$criteria->compare('idconcepto',$this->idconcepto);
		$criteria->with = array ('idconcepto1','medico');
                $criteria->addSearchCondition('UPPER(idconcepto1.concepto)', strtoupper($this->concepto), true);
		$criteria->compare('UPPER(descripcion)',strtoupper($this->descripcion),true);
		$criteria->compare('fechainicio',$this->fechainicio);
		$criteria->compare('fechafin',$this->fechafin);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
                //$criteria->compare('t.estado',true);
                $criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->compare('idmedico',$this->idmedico,true);
                $criteria->addSearchCondition('UPPER(medico.nombre || medico.apellido)', strtoupper($this->nombredoc), true);
                $criteria->order = 'idevento DESC';
                        
                return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Evento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
