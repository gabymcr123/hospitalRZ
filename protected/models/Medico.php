<?php

/**
 * This is the model class for table "agendamiento.medico".
 *
 * The followings are the available columns in table 'agendamiento.medico':
 * @property integer $idmedico
 * @property integer $idespecialidad
 * @property string $nombre
 * @property string $apellido
 * @property string $cedula
 * @property string $telefono
 * @property boolean $estado
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 * @property integer $idusuario
 * @property integer $intervalo
 *
 * The followings are the available model relations:
 * @property Evento[] $eventos
 * @property Especialidadmedica $idespecialidad
 * @property Usuario $idusuario
 * @property Dia[] $dias
 * @property Agenda[] $agendas
 * @property Porcentajes[] $porcentajes
 * @property Agendaextra[] $agendaextras
 */
class Medico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.medico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idespecialidad, nombre, apellido, cedula, intervalo', 'required'),
			array('idespecialidad, idusuario, intervalo', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido', 'length', 'max'=>35),
			array('cedula, telefono', 'length', 'max'=>10),
                        array ('intervalo','length', 'max'=>2),
                        array('cedula','unique', 'message' => 'Esta cedula ya existe'),
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('estado, telefono,fechacreado, fechamodificado,titulo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idmedico, idespecialidad, nombre, apellido, cedula, telefono, estado, creadopor, modificadopor, fechacreado, fechamodificado, idusuario, especialidad,intervalo,nombreusu', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'eventos' => array(self::HAS_MANY, 'Evento', 'idmedico'),
			'idespecialidad1' => array(self::BELONGS_TO, 'Especialidadmedica', 'idespecialidad'),
			'pkusuario' => array(self::BELONGS_TO, 'Usuario', 'idusuario'),
                        'pkidusuario' => array(self::BELONGS_TO, 'Usuario', 'idusuario'),
			'dias' => array(self::HAS_MANY, 'Dia', 'idmedico'),
			'agendas' => array(self::HAS_MANY, 'Agenda', 'idmedico'),
			'porcentajes' => array(self::HAS_MANY, 'Porcentajes', 'idenmedico'),
			'agendaextras' => array(self::HAS_MANY, 'Agendaextra', 'idmedico'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idmedico' => 'Profesional de la Salud',
			'idespecialidad' => 'Especialidad',
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'cedula' => 'Cedula',
			'telefono' => 'Telefono',
			'estado' => 'Estado',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
			'idusuario' => 'Idusuario',
			'intervalo' => 'Intervalo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditidusuarioions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
        public $especialidad;
        public $nombreusu;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('idespecialidad',$this->idespecialidad);
		$criteria->with = array ('idespecialidad1','pkidusuario');
                $criteria->addSearchCondition('UPPER(idespecialidad1.especialidad)', strtoupper($this->especialidad), true);		
		$criteria->compare('UPPER(t.nombre)',strtoupper($this->nombre),true);
		$criteria->compare('UPPER(t.apellido)',strtoupper($this->apellido),true);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('telefono',$this->telefono,true);
		//$criteria->compare('t.estado',true);
		$criteria->compare('t.creadopor',$this->creadopor,true);
		$criteria->compare('t.modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
		$criteria->compare('idusuario',$this->idusuario);
                $criteria->compare('intervalo',$this->intervalo);
                $criteria->compare('pkidusuario.nombre',$this->nombreusu);
                $criteria->order = 't.apellido ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Medico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        public function getNombreCompleto()
    {
        return $this->nombre.' '.$this->apellido;
    }
       public function getFullname()
    {
        return $this->apellido.' '.$this->nombre;
    }
    public function getFullnameEspe()
    {
        return $this->idespecialidad1->especialidad.' ( '.$this->apellido.' '.$this->nombre.' )';
    }
    

        
   
}
