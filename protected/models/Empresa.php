<?php

/**
 * This is the model class for table "agendamiento.empresa".
 *
 * The followings are the available columns in table 'agendamiento.empresa':
 * @property integer $idempresa
 * @property string $nombre
 * @property string $razonsocial
 * @property string $direccion
 * @property string $lleno1
 * @property string $lleno2
 * @property string $creadopor
 * @property string $modificado
 * @property string $fechacreado
 * @property string $fechamodificado
 */
class Empresa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.empresa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre', 'required'),
			array('razonsocial', 'length', 'max'=>30),
			array('direccion, creadopor, modificado', 'length', 'max'=>50),
			array('lleno1, lleno2, fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idempresa, nombre, razonsocial, direccion, lleno1, lleno2, creadopor, modificado, fechacreado, fechamodificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idempresa' => 'Idempresa',
			'nombre' => 'Nombre',
			'razonsocial' => 'Razonsocial',
			'direccion' => 'Direccion',
			'lleno1' => 'Lleno1',
			'lleno2' => 'Lleno2',
			'creadopor' => 'Creadopor',
			'modificado' => 'Modificado',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idempresa',$this->idempresa);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('razonsocial',$this->razonsocial,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('lleno1',$this->lleno1,true);
		$criteria->compare('lleno2',$this->lleno2,true);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificado',$this->modificado,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order = 'nombre ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Empresa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
