<?php

/**
 * This is the model class for table "agendamiento.agendaextra".
 *
 * The followings are the available columns in table 'agendamiento.agendaextra':
 * @property integer $idextra
 * @property integer $idmedico
 * @property integer $idtipo
 * @property string $responsable
 * @property string $fecha
 * @property integer $numturno
 * @property boolean $estado
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Medico $idmedico
 * @property Tipoagenda $idtipo
 */
class Agendaextra extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.agendaextra';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idmedico, idtipo, fecha, numturno', 'required'),
			array('responsable,idmedico, idtipo, numturno', 'numerical', 'integerOnly'=>true),
                    
			
			array(' fecha,creadopor, modificadopor', 'length', 'max'=>50),
			array('estado, fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idextra, idmedico, idtipo, responsable, fecha, numturno, estado, creadopor, modificadopor, fechacreado, fechamodificado,medico,tipoagenda,responsable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'medico1' => array(self::BELONGS_TO, 'Medico', 'idmedico'),
			'tipo1' => array(self::BELONGS_TO, 'Tipoagenda', 'idtipo'),
			'usuario1' => array(self::BELONGS_TO, 'Usuario', 'responsable'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idextra' => 'Idextra',
			'idmedico' => 'Profesional de la Salud',
			'idtipo' => 'Tipo de Agenda',
			'responsable' => 'Responsable',
			'fecha' => 'Fecha',
			'numturno' => 'Número de Turnos',
			'estado' => 'Estado',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
        public $medico;
        public $tipoagenda;
        public $responsable;
        public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.idextra',$this->idextra);
		$criteria->compare('t.idmedico',$this->idmedico);
		$criteria->compare('t.idtipo',$this->idtipo);
		$criteria->compare('fecha',$this->fecha);
		$criteria->compare('t.numturno',$this->numturno);
		//$criteria->compare('t.estado',$this->estado);
		$criteria->compare('t.creadopor',$this->creadopor,true);
		$criteria->compare('t.modificadopor',$this->modificadopor,true);
		$criteria->compare('t.fechacreado',$this->fechacreado,true);
		$criteria->compare('t.fechamodificado',$this->fechamodificado,true);
                $criteria->with = array ('medico1','tipo1','usuario1');
                $criteria->addSearchCondition("UPPER(medico1.nombre ||' '||medico1.apellido)",strtoupper($this->medico), true);
                $criteria->addSearchCondition("UPPER(tipo1.tipoagenda)",strtoupper($this->tipoagenda), true);
                $criteria->addSearchCondition("UPPER(usuario1.nombre ||' '||usuario1.apellido)",strtoupper($this->responsable), true);
                $criteria->order = 'fecha DESC';
                
                $sql = 'select rolusuario.rol from agendamiento.usuario inner join agendamiento.rolusuario on usuario.idrol = rolusuario.idrol where idusuario ='.Yii::app()->user->id;
                $tipo = Yii::app()->db->createCommand($sql)->queryScalar();
                $idusuario = Yii::app()->user->id;
                if($tipo =="MEDICO")
                {
                    $criteria->compare('medico1.idusuario',$idusuario);
                }                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Agendaextra the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
}
