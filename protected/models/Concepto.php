<?php

/**
 * This is the model class for table "agendamiento.concepto".
 *
 * The followings are the available columns in table 'agendamiento.concepto':
 * @property integer $idconcepto
 * @property string $concepto
 * @property string $fechainicio(eliminado)
 * @property string $fechafin(eliminado)
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Evento[] $eventos
 */
class Concepto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.concepto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('concepto', 'required'),
			array('concepto, creadopor, modificadopor', 'length', 'max'=>50),
			
                        array('fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idconcepto, concepto, creadopor, modificadopor, fechacreado, fechamodificado', 'safe', 'on'=>'search'),
                        //array('idconcepto, concepto, fechainicio, fechafin, creadopor, modificadopor, fechacreado, fechamodificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'eventos' => array(self::HAS_MANY, 'Evento', 'idconcepto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idconcepto' => 'Idconcepto',
			'concepto' => 'Concepto',
			//'fechainicio' => 'Fechainicio',
			//'fechafin' => 'Fechafin',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idconcepto',$this->idconcepto);
		$criteria->compare('UPPER(concepto)',strtoupper($this->concepto),true);
		//$criteria->compare('fechainicio',$this->fechainicio,true);
		//$criteria->compare('fechafin',$this->fechafin,true);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                //$criteria->compare('t.estado',true);
                $criteria->order = 'concepto ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the spec
         * ified AR class.this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Concepto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
