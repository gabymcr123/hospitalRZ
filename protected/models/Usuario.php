<?php

/**
 * This is the model class for table "agendamiento.usuario".
 *
 * The followings are the available columns in table 'agendamiento.usuario':
 * @property integer $idusuario
 * @property integer $idrol
 * @property string $nombre
 * @property string $apellido
 * @property string $usuario
 * @property string $password
 * @property boolean $estado
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Medico[] $medicos
 * @property Rolusuario $idrol
 */
class Usuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
         
	 */
                    public $repeat_password;
	public function tableName()
	{
		return 'agendamiento.usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idrol, nombre, apellido, usuario, password ,estado', 'required'),
			array('idrol', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido, usuario, password', 'length', 'max'=>30),
                        array('usuario','unique', 'message' => 'Este usuario ya existe'),
			array('creadopor, modificadopor', 'length', 'max'=>50),
                        array('password','length','min'=>4),
                        array('repeat_password','ComprobarPassword'),
                        array("fechacreado, fechamodificado", "safe"),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idusuario, idrol, nombre, apellido, usuario, password, estado, creadopor, modificadopor, fechacreado, fechamodificado,rol', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'medicos' => array(self::HAS_MANY, 'Medico', 'idusuario'),
			'idrol1' => array(self::BELONGS_TO, 'Rolusuario', 'idrol'),
			'agendaextras' => array(self::HAS_MANY, 'Agendaextra', 'responsable'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idusuario' => 'Idusuario',
			'idrol' => 'Rol',
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'usuario' => 'Usuario',
			'password' => 'Contraseña',
			'estado' => 'Estado',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
                                                            'repeat_password' => 'Repetir Contraseña'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
                    public $rol;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idusuario',$this->idusuario);
                                        $criteria->addCondition("t.idrol != 2");
		$criteria->with = array ('idrol1');
                                        $criteria->addSearchCondition('idrol1.rol', $this->rol, true);
		$criteria->compare('UPPER(nombre)',strtoupper($this->nombre),true);
		$criteria->compare('UPPER(apellido)',strtoupper($this->apellido),true);
		$criteria->compare('UPPER(usuario)',strtoupper($this->usuario),true);
		$criteria->compare('password',$this->password,true);
//		$criteria->compare('t.estado',true);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order = 'usuario ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchAgendador()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idusuario',$this->idusuario);
                                        $criteria->compare('idrol',1);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('apellido',$this->apellido,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('password',$this->password,true);
//		$criteria->compare('t.estado',true);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order = 'usuario ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getNombreCompleto()
        {
            return $this->nombre.' '.$this->apellido;
        }
        
         public function ComprobarPassword($attribute,$params){
                        $var=$this->repeat_password;
                        $con2 = $this->password;

                        if($var == $con2){

                        }else{
                            $this->addError($attribute,'Contraseñas no coinciden');
                        }

                    }
}
