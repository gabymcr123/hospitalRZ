<?php

/**
 * This is the model class for table "agendamiento.subcentro".
 *
 * The followings are the available columns in table 'agendamiento.subcentro':
 * @property integer $idsubcentro
 * @property string $subcentro
 * @property string $provincia
 * @property string $canton
 * @property string $parroquia
 * @property string $zona
 * @property string $distrito
 * @property string $circuito
 * @property string $estado
 * @property string $institucion_sistema
 * @property string $tipo
 * @property string $seleccionada
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Agenda[] $agendas
 */
class Subcentro extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.subcentro';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('institucion_sistema, tipo, seleccionada', 'length', 'max'=>10),
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('subcentro, provincia, canton, parroquia, zona, distrito, circuito, estado, fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idsubcentro, subcentro, provincia, canton, parroquia, zona, distrito, circuito, estado, institucion_sistema, tipo, seleccionada, creadopor, modificadopor, fechacreado, fechamodificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agendas' => array(self::HAS_MANY, 'Agenda', 'idsubcentro'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idsubcentro' => 'Idsubcentro',
			'subcentro' => 'Subcentro',
			'provincia' => 'Provincia',
			'canton' => 'Canton',
			'parroquia' => 'Parroquia',
			'zona' => 'Zona',
			'distrito' => 'Distrito',
			'circuito' => 'Circuito',
			'estado' => 'Estado',
			'institucion_sistema' => 'Institucion Sistema',
			'tipo' => 'Tipo',
			'seleccionada' => 'Seleccionada',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('subcentro',$this->subcentro,true);
		$criteria->compare('provincia',$this->provincia,true);
		$criteria->compare('canton',$this->canton,true);
		$criteria->compare('parroquia',$this->parroquia,true);
		$criteria->compare('zona',$this->zona,true);
		$criteria->compare('distrito',$this->distrito,true);
		$criteria->compare('circuito',$this->circuito,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('institucion_sistema',$this->institucion_sistema,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('seleccionada',$this->seleccionada,true);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subcentro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
