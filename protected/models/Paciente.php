﻿<?php

/**
 * This is the model class for table "agendamiento.paciente".
 *
 * The followings are the available columns in table 'agendamiento.paciente':
 * @property integer $idpaciente
 * @property string $nombre
 * @property string $apellido
 * @property string $cedula
 * @property string $telefono
 * @property string $historialclinico
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 * @property boolean $estado
 *
 * The followings are the available model relations:
 * @property Agenda[] $agendas
 */
class Paciente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.paciente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('nombre, apellido', 'length', 'max'=>30),
			array('cedula', 'length', 'max'=>13),
                        array('historialclinico','length', 'max'=>20),
                        array('historialclinico', 'unique','message'=>'El historial clínico ya  existe'),
			array('telefono,historialclinico', 'length', 'max'=>11),
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('fechacreado, fechamodificado, estado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idpaciente, nombre, apellido, cedula, telefono, historialclinico, creadopor, modificadopor, fechacreado, fechamodificado, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agendas' => array(self::HAS_MANY, 'Agenda', 'idpaciente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idpaciente' => 'Paciente',
			'nombre' => 'Nombres',
			'apellido' => 'Apellidos',
			'cedula' => 'Cédula',
			'telefono' => 'Teléfono',
			'historialclinico' => 'Número historial clínico',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
			'estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idpaciente',$this->idpaciente);
		$criteria->compare("UPPER(nombre||' '||APELLIDO)", strtoupper($this->nombre),true);
		$criteria->compare('UPPER(apellido)',strtoupper($this->apellido),true);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('UPPER(historialclinico)',strtoupper($this->historialclinico),true);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
		//$criteria->compare('estado',true);
                $criteria->order = 'idpaciente DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paciente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getNombreCompleto()
        {
            return $this->nombre.' '.$this->apellido;
        }
}
