<?php

/**
 * This is the model class for table "agendamiento.permisos".
 *
 * The followings are the available columns in table 'agendamiento.permisos':
 * @property integer $idpermisos
 * @property integer $idtipoagenda
 * @property integer $idmedico
 * @property boolean $autorizacion
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Tipoagenda $idtipoagenda
 * @property Medico $idmedico
 */
class Permisos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.permisos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idtipoagenda, idmedico', 'required'),
			array('idtipoagenda, idmedico', 'numerical', 'integerOnly'=>true),
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('autorizacion, fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idpermisos, idtipoagenda, idmedico, autorizacion, creadopor, modificadopor, fechacreado, fechamodificado,medico,tipoagenda', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idtipoagenda1' => array(self::BELONGS_TO, 'Tipoagenda', 'idtipoagenda'),
			'idmedico1' => array(self::BELONGS_TO, 'Medico', 'idmedico'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idpermisos' => 'Idpermisos',
			'idtipoagenda' => 'Tipo de Agenda',
			'idmedico' => 'Profesional de la Salud',
			'autorizacion' => 'Autorización',
			'creadopor' => 'Creado por',
			'modificadopor' => 'Modificado por',
			'fechacreado' => 'Fecha creado',
			'fechamodificado' => 'Fecha modificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
         public $medico;
        public $tipoagenda;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idpermisos',$this->idpermisos);
		$criteria->compare('t.idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('autorizacion',$this->autorizacion);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                 $criteria->with = array ('idmedico1','idtipoagenda1');
                $criteria->addSearchCondition("UPPER(idmedico1.nombre ||' '||idmedico1.apellido)",strtoupper($this->medico), true);
                $criteria->addSearchCondition("UPPER(idtipoagenda1.tipoagenda)",strtoupper($this->tipoagenda), true);
                
                $sql = 'select rolusuario.rol from agendamiento.usuario inner join agendamiento.rolusuario on usuario.idrol = rolusuario.idrol where idusuario ='.Yii::app()->user->id;
                $tipo = Yii::app()->db->createCommand($sql)->queryScalar();
                $idusuario = Yii::app()->user->id;
                if($tipo =="MEDICO")
                {
                    $criteria->compare('idmedico1.idusuario',$idusuario);
                }                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Permisos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
