
<?php

/**
 * This is the model class for table "agendamiento.agenda".
 *
 * The followings are the available columns in table 'agendamiento.agenda':
 * @property integer $idagenda
 * @property integer $idtipoagenda
 * @property integer $idpaciente
 * @property integer $idmedico
 * @property integer $idsubcentro
 * @property string $nombre
 * @property integer $numeroturno
 * @property string $fecha
 * @property string $horainicio
 * @property string $horafin
 * @property boolean $estado
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 * @property boolean $atendido
 *
 * The followings are the available model relations:
 * @property Medico $idmedico
 * @property Paciente $idpaciente
 * @property Subcentro $idsubcentro
 * @property Tipoagenda $idtipoagenda
 */
class Agenda extends CActiveRecord
{
    public $sql;
    public $arrayDataProvider;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.agenda';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
	return array(
        array('idtipoagenda, idpaciente, idmedico, fecha, nombre, numeroturno, horainicio, horafin', 'required'),
        array('idtipoagenda, idpaciente, idsubcentro, numeroturno, telefono,celular', 'numerical', 'integerOnly'=>true),
        array('nombre, creadopor,fecha, modificadopor,fechacreado', 'length', 'max'=>50),
        array('atendido', 'boolean'),
        array('estado, fechacreado,fechamodificado,telefono,celular', 'safe'),
        // The following rule is used by search().
	// @todo Please remove those attributes that should not be searched.
        array('idagenda, idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, numeroturno, fecha, horainicio, horafin, estado, creadopor, modificadopor, fechacreado,apellido, fechamodificado, nombredoc, telefono, celular,nombresub,nombretipoag,nombrepac,historial,atendido, titulo, fx,canton,cedu', 'safe', 'on'=>'search'),
        array('idagenda, idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, numeroturno, fecha, horainicio, horafin, estado, creadopor, modificadopor, fechacreado, fechamodificado, agenda, paciente, doctor, f1, f2', 'safe', 'on'=>'busqueda1'),
        array('idagenda, idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, numeroturno, fecha, horainicio, horafin, estado, creadopor, modificadopor, fechacreado, fechamodificado, agenda1,f3, f4', 'safe', 'on'=>'busqueda2'),
        array('idagenda, idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, numeroturno, fecha, horainicio, horafin, estado, creadopor, modificadopor, fechacreado, fechamodificado, f3, f4, centro', 'safe', 'on'=>'busqueda3'),
        array('atendido', 'safe'),
        array('idagenda, idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, numeroturno, fecha, horainicio, horafin, estado, creadopor, modificadopor, fechacreado, fechamodificado, agenda1,f3, f4', 'safe', 'on'=>'busqueda4'),
        array('idagenda, idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, numeroturno, fecha, horainicio, horafin, estado, creadopor, modificadopor, fechacreado, fechamodificado, fs,centro, atendido, nuevafecha, idespecialidad', 'safe', 'on'=>'busqueda5'),
        array('idagenda, idtipoagenda, idpaciente, idmedico, idsubcentro, nombre, numeroturno, fecha, horainicio, horafin, estado, creadopor, modificadopor, fechacreado, fechamodificado, fs,centro, atendido, nuevafecha, especialidad, cantidad', 'safe', 'on'=>'busquedaEspecialidad'),
        array('especialidad, cantidad', 'safe'), 
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pkmedico' => array(self::BELONGS_TO, 'Medico', 'idmedico'),
			'pkpaciente' => array(self::BELONGS_TO, 'Paciente', 'idpaciente'),
			'pksubcentro' => array(self::BELONGS_TO, 'Subcentro', 'idsubcentro'),
			'pktipoagenda' => array(self::BELONGS_TO, 'Tipoagenda', 'idtipoagenda'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idagenda' => 'Idagenda',
			'idtipoagenda' => 'Tipo de Agenda',
			'idpaciente' => 'Paciente',
			'idmedico' => 'Profesional de la Salud',
			'idsubcentro' => 'Centro de Salud',
			'nombre' => 'Nombre Del Paciente',
			'numeroturno' => 'Numero de Turno',
			'fecha' => 'Fecha de atención',
                        'telefono' => 'Teléfono',
			'celular' => 'Celular',
			'horainicio' => 'Hora Atención',
			'horafin' => 'Hora Fin',
			'estado' => 'Estado',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fecha Agendada',
			'fechamodificado' => 'Fechamodificado',
                        'atendido'=> 'Atendido',
                        
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
        public $nombredoc;
        public $nombresub;
        public $nombrepac;
        public $nombretipoag;
        public $apellido;
        public $historial;
        public $fx;
        public $cedu;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('t.idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idpaciente',$this->idpaciente);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('LOWER(t.nombre)',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
		$criteria->compare('t.fecha',$this->fecha);
                if((isset($this->fx) && trim($this->fx) != "")) { 
                  $criteria->compare('t.fecha',$this->fx);  
                } 
                $criteria->compare('telefono',$this->telefono);
		$criteria->compare('celular',$this->celular);
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('t.estado',$this->estado);
		$criteria->compare('t.creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('t.fechacreado',$this->fechacreado);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);    
                $criteria->with = array('pkmedico','pksubcentro','pkpaciente','pktipoagenda');
                
                $criteria->addSearchCondition("UPPER(pkmedico.nombre ||' '||pkmedico.apellido)",strtoupper($this->nombredoc), true);
                $criteria->addSearchCondition("UPPER(pkpaciente.historialclinico)",strtoupper($this->historial), true);
                $criteria->addSearchCondition("UPPER(pkpaciente.cedula)",strtoupper($this->cedu), true);
                $criteria->addSearchCondition('UPPER(pkmedico.idespecialidad1->especialidad)',strtoupper($this->especialidad), true);
  
                $criteria->addSearchCondition('UPPER(pksubcentro.subcentro)',strtoupper($this->nombresub), true);
                $criteria->addSearchCondition("UPPER(pkpaciente.nombre ||' '|| pkpaciente.apellido)",strtoupper($this->nombrepac));
                $criteria->addSearchCondition('UPPER(pktipoagenda.tipoagenda)',strtoupper($this->nombretipoag), true);
                $criteria->order = 'idagenda DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
       
        public function searchmedico($v)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
                $date = date_format(date_create(), 'Y-m-d');
		$criteria=new CDbCriteria;

		$criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idpaciente',$this->idpaciente);
		$criteria->compare('t.idmedico',$v);
		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('LOWER(t.nombre)',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
		$criteria->compare('fecha',$date);
                $criteria->compare('telefono',$this->telefono);
		$criteria->compare('celular',$this->celular);
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('t.estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->compare('atendido',$this->atendido,true);
                $criteria->with = array('pkmedico','pksubcentro','pkpaciente','pktipoagenda');
                
                $criteria->addSearchCondition("LOWER(pkmedico.nombre ||' '||pkmedico.apellido)",$this->nombredoc, true);
  
                $criteria->addSearchCondition('LOWER(pksubcentro.subcentro)',$this->nombresub, true);
                $criteria->addSearchCondition('LOWER(pkpaciente.nombre)',$this->nombrepac, true);
                $criteria->addSearchCondition('LOWER(pktipoagenda.tipoagenda)',$this->nombretipoag, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public $f1;
  public $f2;
        public $doctor;
        public $paciente;
        public $agenda;
        public function busqueda1()
	{
		 $criteria = new CDbCriteria;
                $criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idpaciente',$this->idpaciente);
		$criteria->compare('t.idmedico',$this->idmedico);
		$criteria->compare('idsubcentro',$this->idsubcentro);
                $criteria->with = array ('pksubcentro','pkmedico');
                $criteria->addSearchCondition('pksubcentro.subcentro', $this->centro, true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
                $criteria->compare('fecha',$this->fecha,true);
		if((isset($this->f1) && trim($this->f1) != "") && (isset($this->f2) && trim($this->f2) != "")){ 
                $criteria->addBetweenCondition('fecha', ''.$this->f1.'', ''.$this->f2.'');
                }
                $criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('celular',$this->celular, true);
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order= 'fecha,numeroturno ASC';
                $session=new CHttpSession;
                            $session->open();
                            $session['reporte_1']=$criteria;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public $f3;
        public $f4;
        public $agenda1;
        
        
        //para reporte 2
        public function busqueda2(){
                $criteria = new CDbCriteria;
                $criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('t.idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idpaciente',$this->idpaciente);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
		$criteria->with = array ('pktipoagenda');
                $criteria->addSearchCondition('pktipoagenda.tipoagenda', $this->agenda1, true);
                //if($f1!="" or $f2!=""){
		$criteria->compare('fecha',$this->fecha,true);
		//}
                if((isset($this->f3) && trim($this->f3) != "") && (isset($this->f4) && trim($this->f4) != "")){ 
                $criteria->addBetweenCondition('fecha', ''.$this->f3.'', ''.$this->f4.'');
                } //else {
                    //if((isset($this->f1) && trim($this->f1) != "") || (isset($this->f2) && trim($this->f2) == "")){
                      //$criteria->addBetweenCondition('fecha', ''.$this->f1.'');
                    //}
                //}
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $session=new CHttpSession;
                            $session->open();

                            $session['reporte_2']=$criteria;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
        public $centro;
        //para reporte 3
        public function busqueda3(){
                $criteria = new CDbCriteria;
                $criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idpaciente',$this->idpaciente);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('idsubcentro',$this->idsubcentro);
                $criteria->with = array ('pksubcentro','pkmedico');
                $criteria->addSearchCondition('pksubcentro.subcentro', $this->centro, true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
		$criteria->compare('fecha',$this->f3);
                $criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('celular',$this->celular, true);
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order= 'pkmedico.apellido,numeroturno ASC';
                $session=new CHttpSession;
                            $session->open();
                            $session['reporte_3']=$criteria;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
        //reporte 4
      public $f3_1;
        public $f4_1;
        public $agenda1_1;
        public function busqueda4()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
                $criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('t.idtipoagenda',1);
		$criteria->compare('idpaciente',$this->idpaciente);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
	$criteria->with = array ('pktipoagenda');
                $criteria->addSearchCondition('pktipoagenda.tipoagenda', $this->agenda1, true);
                $criteria->condition = "t.fechacreado >= '".$this->f3."' and fecha <= '".$this->f4."' and t.idtipoagenda=1";
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->order='idmedico ASC';
                $session=new CHttpSession;
                            $session->open();

                            $session['reporte_4']=$criteria;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public $idespecialidad;
        //reporte5
        public function busqueda5()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idpaciente',$this->idpaciente);
                $criteria->with = array ('pkpaciente','pkmedico','pktipoagenda');
                $criteria->addSearchCondition('pkpaciente.nombre', $this->paciente, true);
		$criteria->compare('t.idmedico',$this->idmedico);
                $criteria->addSearchCondition('pkmedico.nombre', $this->doctor, true);
		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
                //$criteria->with = array ('especialidad');
                //$criteria->addSearchCondition('especialidad.especialidad', $this->especialidades, true);
                $criteria->addSearchCondition('pktipoagenda.tipoagenda', $this->agenda, true);
                //if($f1!="" or $f2!=""){
		$criteria->compare('t.fecha',$this->fecha);
		//}
                //codigo para sumar o restar fecha
                $this->nuevafecha = strtotime ( '+14 day' , strtotime ( $this->fs ) );
                $this->nuevafecha = date ( 'd-m-Y' , $this->nuevafecha );
                if((isset($this->fs) && trim($this->fs) != "")) { 
                $criteria->addBetweenCondition('fecha', ''.$this->fs.'', ''.$this->nuevafecha.'');
                } 
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->compare('t.atendido',1);
                $session=new CHttpSession;
                            $session->open();

                            $session['reporte_5']=$criteria;  //Esto para guardar la criteria en la sesión actual para usarlo posteriormente.
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function busquedaPacienteDia(){
               $criteria=new CDbCriteria;

		$criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('t.idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idpaciente',$this->idpaciente);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('LOWER(t.nombre)',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
		$criteria->compare('t.fecha',$this->fecha);
                $criteria->compare('telefono',$this->telefono);
		$criteria->compare('celular',$this->celular);
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('t.estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('t.fechacreado',$this->fechacreado);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);    
                $criteria->with = array('pkmedico','pksubcentro','pkpaciente','pktipoagenda');
                
                $criteria->addSearchCondition("UPPER(pkmedico.nombre ||' '||pkmedico.apellido)",strtoupper($this->nombredoc), true);
                $criteria->addSearchCondition('UPPER(pkmedico.idespecialidad1->especialidad)',strtoupper($this->especialidad), true);
  
                $criteria->addSearchCondition('UPPER(pksubcentro.subcentro)',strtoupper($this->nombresub), true);
                $criteria->addSearchCondition("UPPER(pkpaciente.nombre ||' '|| pkpaciente.apellido)",strtoupper($this->nombrepac));
                $criteria->addSearchCondition('UPPER(pktipoagenda.tipoagenda)',strtoupper($this->nombretipoag), true);
                $criteria->order = 'idagenda DESC';
                $session=new CHttpSession;
                            $session->open();

                            $session['reporte_dia']=$criteria;  //Esto para guardar la criteria en la sesión actual para usarlo posteriormente.
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        //reporte6
        public $especialidad;
        public $cantidad;
        public function busquedaEspecialidad()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
                $criteria->compare('idagenda',$this->idagenda);
		$criteria->compare('idtipoagenda',$this->idtipoagenda);
		$criteria->compare('idpaciente',$this->idpaciente);
                $criteria->with = array ('pkpaciente');
                $criteria->addSearchCondition('pkpaciente.nombre', $this->paciente, true);
                $sql= Yii::app()->db->createCommand("select idmedico from agendamiento.medico where nombre||' '||apellido = '".$this->especialidad."'")->queryScalar();
		$criteria->compare('idmedico',$sql);
                $criteria->with = array('pkmedico');
                $criteria->addSearchCondition('pkmedico.nombre', $this->doctor, true);
		$criteria->compare('idsubcentro',$this->idsubcentro);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('numeroturno',$this->numeroturno);
                //$criteria->with = array ('especialidad');
                //$criteria->addSearchCondition('especialidad.especialidad', $this->especialidades, true);
                $criteria->with = array ('pktipoagenda');
                $criteria->addSearchCondition('pktipoagenda.tipoagenda', $this->agenda, true);
                //if($f1!="" or $f2!=""){
		$criteria->compare('fecha',$this->fecha,true);
		//}
                //codigo para sumar o restar fecha
                $this->nuevafecha = strtotime ( '+14 day' , strtotime ( $this->fs ) );
                $this->nuevafecha = date ( 'd-m-Y' , $this->nuevafecha );
                if((isset($this->fs) && trim($this->fs) != "")) { 
                $criteria->addBetweenCondition('fecha', ''.$this->fs.'', ''.$this->nuevafecha.'');
                } 
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('t.fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);
                $criteria->compare('t.atendido',1);
                
                $session=new CHttpSession;
                            $session->open();

                            $session['reporte_especialidad']=$criteria;  //Esto para guardar la criteria en la sesión actual para usarlo posteriormente.


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Agenda the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
         public function getNombreCompleto()
    {
        return $this->nombre.' '.$this->apellido;
    }
}

