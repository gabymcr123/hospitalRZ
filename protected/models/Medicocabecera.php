<?php

/**
 * This is the model class for table "agendamiento.medicocabecera".
 *
 * The followings are the available columns in table 'agendamiento.medicocabecera':
 * @property integer $idcabecera
 * @property integer $idmedico
 * @property string $horainicio
 * @property string $horafin
 * @property string $intervaloconsulta
 * @property string $creadopor
 * @property string $modificadopor
 * @property string $fechacreado
 * @property string $fechamodificado
 *
 * The followings are the available model relations:
 * @property Medico $idmedico
 */
class Medicocabecera extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agendamiento.medicocabecera';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idmedico, horainicio, horafin, intervaloconsulta', 'required'),
			array('idmedico', 'numerical', 'integerOnly'=>true),
			array('creadopor, modificadopor', 'length', 'max'=>50),
			array('fechacreado, fechamodificado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idcabecera, idmedico, horainicio, horafin, intervaloconsulta, creadopor, modificadopor, fechacreado, fechamodificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idmedico' => array(self::BELONGS_TO, 'Medico', 'idmedico'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idcabecera' => 'Idcabecera',
			'idmedico' => 'Idmedico',
			'horainicio' => 'Horainicio',
			'horafin' => 'Horafin',
			'intervaloconsulta' => 'Intervaloconsulta',
			'creadopor' => 'Creadopor',
			'modificadopor' => 'Modificadopor',
			'fechacreado' => 'Fechacreado',
			'fechamodificado' => 'Fechamodificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idcabecera',$this->idcabecera);
		$criteria->compare('idmedico',$this->idmedico);
		$criteria->compare('horainicio',$this->horainicio,true);
		$criteria->compare('horafin',$this->horafin,true);
		$criteria->compare('intervaloconsulta',$this->intervaloconsulta,true);
		$criteria->compare('creadopor',$this->creadopor,true);
		$criteria->compare('modificadopor',$this->modificadopor,true);
		$criteria->compare('fechacreado',$this->fechacreado,true);
		$criteria->compare('fechamodificado',$this->fechamodificado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Medicocabecera the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
