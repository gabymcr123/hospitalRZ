<?php


class AgendaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
        public $ayuda;
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
           
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','calendario','obtenermedicos','reagendarMed','valoresagenda','reagendar','Autocompletar','Autocompletar2','ayuda','tipoagenda', 'obtenerhorario', 'obtenermedico','indicadoresGPR','reporteexcelespecialidadantidad'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('listareagendar','listareagendar2','listareagendados2','listareagendados','create','update','obtenerHora','enviar','ReporteDoctorPacAgend','Especialidad', 'prueba','listaespecialidad', 'GenerarPdf', 'Reporte2', 'GenerarPdf2', 'ReportePacientesAgendados', 'GenerarPdfPacientesAgendados','Reporte4', 'GenerarPdf4','Reporte5', 'GenerarPdf5' ,'lista','GenerarExcelPacientesAgendados','GenerarExcelDoctor','GenerarExcelAgenda','GenerarExcelPaci15Dias','GenerarExcelDoctorPaci15Dias','reagendarMedico','obtenerPacientesReagendar','autocompletarcentro','disponibilidad','consultahistorial','autocompletarprofesional','Pacientesdeldia','pdfpacientedia','reporteindicadoresgprExcel','adminmedico','horarioinformacion'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),            
			),
                    array('deny',
                        'actions'=> array('create'),
                        'users'=>array('*'),
                        
                    ),
		);
	}

	public function actionView($id)
	{   
            Yii::app()->db->createCommand("delete from agendamiento.turnotemporal where usuario ='".Yii::app()->user->name."';")->execute();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
       	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            Yii::app()->db->createCommand("delete from agendamiento.turnotemporal where usuario ='".Yii::app()->user->name."';")->execute();
            $model=new Agenda;
            $this->performAjaxValidation($model);
            
                if(isset($_POST['referencia']))
                    $ref = $_POST['referencia'];
                
		
		// Uncomment the following line if AJAX validation is needed
		if(isset($_POST['Agenda']))
		{       $centro = $_POST['subcentro'];
                $date = date_format(date_create(), 'Y-m-d H:i:s');
                $model->attributes=$_POST['Agenda'];
                if($model->idpaciente== null or $model->idpaciente== '' or $model->idpaciente ==' '){
                    $paciente = new Paciente;
                    $nombresarr = explode(" ",$model->nombre);
                    $total = count($nombresarr);
                    $mitad = ceil($total/2);
                    foreach($nombresarr as $key => $value){
                        if($key < $mitad) $paciente->nombre = trim($paciente->nombre.' '.$value);
                        else $paciente->apellido = trim($paciente->apellido.' '.$value);
                    }
                    $paciente->telefono = $model->telefono;
                    $paciente->celular = $model->celular;
                    $paciente->fechacreado = $date;
                    $paciente->fechamodificado = $date;
                    $hc= $_POST['historial'];
                    
                    if (strlen($hc)<9){
                          $paciente->historialclinico = $hc;
                    }else {
                        $paciente->cedula = $hc;
                    }
                  
                    if($paciente->save())
                        $model->idpaciente= $paciente->idpaciente;
                }
                if($model->idsubcentro== null or $model->idsubcentro== ''){
                    $subcentro = Yii::app()->db->createCommand("select idsubcentro from agendamiento.subcentro where subcentro='".trim($centro)."';")->queryScalar();
                    if(isset($subcentro) and $subcentro!=''){
                        $model->idsubcentro = $subcentro;
                    }else{
                        $model->idsubcentro = 0;
                    }
                }
                if(!isset($model->numeroturno)|| $model->numeroturno==""){$model->numeroturno=1;}
                $val = Yii::app()->db->createCommand("select inicio, fin from agendamiento.horapaciente(".$model->idmedico.",'".$model->fecha."', ".$model->numeroturno.");")->queryRow();
                $model->horainicio = $val['inicio'];
                if($model->idtipoagenda == '5' or $model->idtipoagenda == '6'){
                    if(isset($ref)){
                    $agenda = $this->loadModel($ref);
                    $agenda->save();
                    $model->idreferencia = $ref;
                    }
                    $model->reagendado = 1;
                    
                }else if($model->idtipoagenda == '3' or $model->idtipoagenda == '4'){
                    $model->numeroturno = 0;
                    $model->horainicio = Yii::app()->db->createCommand('select horafin from agendamiento.dia where idmedico='.$model->idmedico.';')->queryScalar();
                }
                $model->fechacreado = $date;
                $model->fechamodificado = $date;
                $model->creadopor = Yii::app()->user->name;
                $model->horafin = $val['fin'];
                
		if($model->save())
                    $this->redirect(array('view','id'=>$model->idagenda));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agenda']))
		{     
            $date = date_format(date_create(), 'Y-m-d H:i:s');
			$model->attributes=$_POST['Agenda'];
            $model->fechacreado = $date;
            $model->fechamodificado = $date;
			if($model->save())
				$this->redirect(array('view','id'=>$model->idagenda));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Agenda');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
        
        
        
        public function actionlistareagendar()
	{
		$dataProvider=new CActiveDataProvider('Agenda');
		$this->render('listareagendar',array(
			'dataProvider'=>$dataProvider,
		));
	}
        
        public function actionlistareagendar2()
	{
		$dataProvider=new CActiveDataProvider('Agenda');
		$this->render('listareagendar2',array(
			'dataProvider'=>$dataProvider,
		));
	}
         public function actionEspecialidad(){
             $dataProvider=new CActiveDataProvider('Agenda');
		$this->render('especialidad',array(
			'dataProvider'=>$dataProvider,
		));
         }
        

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
            Yii::app()->db->createCommand("delete from agendamiento.turnotemporal where usuario ='".Yii::app()->user->name."';")->execute();
		$model=new Agenda('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Agenda']))
			$model->attributes=$_GET['Agenda'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}
        public function actionAdminmedico()
	{
                $user = Yii::app()->user->Name;
		$model=new Agenda('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Agenda']))
			$model->attributes=$_GET['Agenda'];
		$this->render('adminmedico',array(
			'model'=>$model,'user'=>$user
		));
	}

	public function loadModel($id)
	{
		$model=Agenda::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La pagina solicitada no existe.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Agenda $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agenda-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionObtenermedicos($opcion){
                $resp = Medico::model()->findAllByAttributes(array('idespecialidad'=>$opcion,'estado'=>true));
                header("Content-type: application/json");
                echo CJSON::encode($resp);
        }
        
        public function actionValoresagenda(){
            /* Cargar los valores para poder agenda cuandp presiona el + en ele calendario*/
            $id = $_GET['id'];/* obtenermos el idmedico o especialidad*/
            $fecha = $_GET['fecha'];/*obtenemos fecha*/
            //sentecia que deber ser el procedimiento a llamar le dare id y fecha y deber debolverme esos datos
            $row = Yii::app()->db->createCommand("select * from agendamiento.valoresmedico(".$id.",'".$fecha."');")->queryRow();
            $count = $row['contador'];/*cantidad existente en agenda*/
            $limite = $row['valorcita'];/*cantidad limite a agendar*/
            $nombres = $row['nombres'];/*el nombre completo del medico*/
            Yii::app()->db->createCommand("delete from agendamiento.turnotemporal where usuario ='".Yii::app()->user->name."';")->execute();
            Yii::app()->db->createCommand("insert into agendamiento.turnotemporal values (".$id.",".$count.",'".Yii::app()->user->name."','".$fecha."');")->execute();
            echo ($id.'_'.$nombres.'_'.$count.'_'.$limite);/*Devuelvo el resultado en un String*/
            /*Al recibirlo lo convierto en Array*/
        }
        public function actionTipoagenda(){
            /*Cuando Selecion otro tipo de agendamiento*/
            $ismedico = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 2) ? true : false ;
            if(isset($_GET['pasar'])){
            $pasar = $_GET['pasar'];/**/
            }else{
            $pasar = "no";
            }
            $id = $_GET['id'];/*id tipo de agenda*/
            $medico = $_GET['idme'];
            $fecha = $_GET['fecha'];
            $autorizacion = Yii::app()->db->createCommand('select autorizacion from agendamiento.permisos where idmedico ='.$medico.'and idtipoagenda = '.$id.';')->queryScalar();
            if($autorizacion or $pasar!="si"){
            if($id>2 && $id <5){
            $row = Yii::app()->db->createCommand("select * from agendamiento.obtenerinifinextra(".$id.",".$medico.",'".$fecha."')")->queryRow();
            $ini = $row['ini'];
            $fin = $row['fin'];
                if(!isset($fin)):
                    $fin = 0;
                endif;
                if($ismedico):
                    $fin = $ini+1;
                endif;
            }else{
             if($id==5)$id=1;
             $row = Yii::app()->db->createCommand("select * from agendamiento.obtenerinifin(".$id.",".$medico.",'".$fecha."')")->queryRow(); 
             $ini = $row['ini'];
             $fin = $row['fin'];
            }
            }else{
                $ini='.';
                $fin='.';
            }
             echo $ini.'_'.$fin;
        }
    public function actionDisponibilidad(){
        $id = $_GET['id'];/* Tipo */
        $fecha = $_GET['fecha'];//Fecha a reagendar
        $paciente= $_GET['paciente'];
        $medico= $_GET['especialidad'];
        if($id=='5'){
            $id=1;
        }else if($id=='6'){
            $id=2;
        }
        $resul = Yii::app()->db->createCommand("select fechacreadop, fechap, medicop, idagend from agendamiento.fechasagenda(".$medico.",'".$fecha."',".$paciente.",".$id.");")->queryRow();
        if(($resul['fechap']!='' && $resul['fechacreadop']!='') and (strtotime($fecha)>=strtotime($resul['fechap']))){
           $dateini = $resul['fechacreadop'];
           $datefin = $resul['fechap']; 
           $medicoviejo = $resul['medicop']; 
           $agenda = $resul['idagend']; 
           $nuevafecha = strtotime ( '-15 day' , strtotime ( $fecha ) ) ;
           if($nuevafecha<=strtotime($datefin))
               print '<strong>Fecha anterior:</strong> '.$datefin.' <strong> - Profesional</strong>: '.$medicoviejo.'<input type="text" value="'.$agenda.'" name="referencia" style="display:none;">';
           else
               print '<strong>Atención: </strong>Paciente no tiene cita a reagendar';
        }else{
           echo '<strong>Atención: </strong>Paciente no tiene cita a reagendar';
        }
        
    }
    
        public function actionReagendar(){
            $this->render('reagendar');
        }
        public function actionReagendarMedico(){

            $this->render('reagendarMedico');

        }
        
               public function actionAutocompletar() {
        $criteria = new CDbCriteria;
	$criteria->compare("LOWER(nombre||' '||apellido)", strtolower($_GET['term']), true);
//        $criteria->compare('LOWER(apellido)', strtolower($_GET['term']), true,'OR');
 	$criteria->order = 'nombre';
 	$criteria->limit = 10; 
        $arr = array();
 	$data = Paciente::model()->findAll($criteria);
 	if (!empty($data))
 	{
  	foreach ($data as $item) 
  	{
   		$arr[] = array(
                'id' => $item->idpaciente,
                'value' => trim($item->nombre ." ". $item->apellido) ,
                'telefono' => $item->telefono,
                'celular' => $item->celular,
                'historial' => $item->historialclinico,
   		);
        }
 	}
 	else
 	{
  		$arr[] = array(
   		'id' => '',
   		'value' => '',
                'telefono' => '',
                'celular' => '',
                'hitorial' => '',
  		);
 	}
 	echo CJSON::encode($arr);
        Yii::app()->end();
}
  public function actionAutocompletar2() {
        $criteria = new CDbCriteria;
	$criteria->compare('LOWER(nombre)', strtolower($_GET['term']), true, 'OR');
        $criteria->compare('LOWER(apellido)', strtolower($_GET['term']), true,'OR');
 	$criteria->order = 'nombre';
 	$criteria->limit = 10; 
        
 	$data = Medico::model()->findAll($criteria);
 	if (!empty($data))
 	{
  	$arr = array();
  	foreach ($data as $item) 
  	{
   		$arr[] = array(
                'id' => $item->idmedico,
                'value' => $item->nombre ." ". $item->apellido ,
   		);
        }
 	}
 	else
 	{
  		$arr = array();
  		$arr[] = array(
   		'id' => '',
   		'value' => 'Sin resultado',
                'telefono' => '',
  		);
 	}
 	echo CJSON::encode($arr);
        Yii::app()->end();
}       
    public function actionAutocompletarcentro(){
        $criteria = new CDbCriteria;
	$criteria->compare('LOWER(subcentro)', strtolower($_GET['term']), true);
 	$criteria->order = 'subcentro';
 	$criteria->limit = 10; 
        
 	$data = Subcentro::model()->findAll($criteria);
 	if (!empty($data))
 	{
  	$centros = array();
  	foreach ($data as $item) 
  	{
   		$centros[] = array(
                'id' => $item->idsubcentro,
                'value' => $item->subcentro ,
                'canton' => $item->canton ,
   		);
        }
 	}
 	else
 	{
  		$centros = array();
  		$centros[] = array(
   		'id' => '',
   		'value' => '',
                'nombre' => '' ,
  		);
 	}
 	echo CJSON::encode($centros);
        Yii::app()->end();
    }
    public function actionAutocompletarprofesional() {
        $criteria = new CDbCriteria;
	$criteria->compare("LOWER(nombre) ||' '||LOWER(apellido)", strtolower($_GET['term']), true);
 	$criteria->order = 'nombre';
 	$criteria->limit = 8; 
 	$data = Medico::model()->findAll($criteria);
 	if (!empty($data))
 	{
  	$arr = array();
  	foreach ($data as $item) 
  	{
   		$arr[] = array(
                    'id' => $item->idmedico,
                    'value' => $item->nombre ." ". $item->apellido ,
                    'especialidad' => $item->idespecialidad,
   		);
    }
 	}
 	else
 	{
  		$arr = array();
  		$arr[] = array(
   		'id' => '',
   		'value' => '',
                'especialidad' => '',
  		);
 	}
 	echo CJSON::encode($arr);
        Yii::app()->end();
}
    public function actionLista($fecha,$accion,$especialidad,$doctor){
            $doctor = $_GET['doctor'];
            $especialidad = $_GET['especialidad'];
            $eventos= array();
                $sql ="";
                if($doctor!=""){
                    echo '<label>Motivos</label><br>';
                    $sql = "select * from agendamiento.eventomedico(".$doctor.",'".$fecha."');";
                }else{
                    echo '<label>Doctores</label><br>';
                    $sql = "select * from agendamiento.eventoespecialidad(".$especialidad.",'".$fecha."');";
                }
                $query = Yii::app()->db->createCommand($sql)->query();
                
                foreach($query as $fila) {
                    $ini = $fila['fechainiciop'];
                    $fin = $fila['fechafinp'];
                    $det = $fila['detalle'];
                    $des = $fila['descripcion'];
                    if($doctor!=""){
                        echo $det.': '.$des.'<br>';
                    }
                    else{
                        $doc = $fila['nombres'];
                        echo '<b>'.$doc.'</b> por motivo de: '.$det.'<br>';
                    }
                }
    }
        public function actionCalendario($mes,$anio,$accion,$especialidad,$doctor){
                $info=(isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 4) ? true : false ;
                $isadmin = Yii::app()->db->createCommand('select idrol from agendamiento.usuario where idusuario ='.Yii::app()->user->Id)->queryScalar()=='3' ? true:false;
                $doc = false;
                $esp = false;
                $lun = false; $mar = false; $mie = false; $jue = false; $vie = false;
                $doctor = $_GET['doctor'];
                $especialidad = $_GET['especialidad'];
                if($doctor!=""){
                    /*dias de la semana si trabaja o no*/
                    $doc=true;
                    $dia = Yii::app()->db->createCommand('select estado from agendamiento.dia where idmedico ='.$doctor.' order by iddia;')->queryColumn();
                    $lun = $dia[0];
                    $mar= $dia[1];
                    $mie= $dia[2];
                    $jue= $dia[3];
                    $vie = $dia[4];
                }
                $fecha_calendario=array();
                if ($_GET["mes"]=="" || $_GET["anio"]=="")
                {
                        $fecha_calendario[1]=intval(date("m"));
                        if ($fecha_calendario[1]<10) $fecha_calendario[1]="0".$fecha_calendario[1];
                        $fecha_calendario[0]=date("Y");
                }
                else
                {
                        $fecha_calendario[1]=intval($_GET["mes"]);
                        if ($fecha_calendario[1]<10) $fecha_calendario[1]="0".$fecha_calendario[1];
                        else $fecha_calendario[1]=$fecha_calendario[1];
                        $fecha_calendario[0]=$_GET["anio"];
                }
                $fecha_calendario[2]="01";

                /* obtenemos el dia de la semana del 1 del mes actual */
                $primeromes=date("N",mktime(0,0,0,$fecha_calendario[1],1,$fecha_calendario[0]));

                /* comprobamos si el a�o es bisiesto y creamos array de d�as */
                if (($fecha_calendario[0] % 4 == 0) && (($fecha_calendario[0] % 100 != 0) || ($fecha_calendario[0] % 400 == 0))) 
                    $dias=array("","31","29","31","30","31","30","31","31","30","31","30","31");
                else $dias=array("","31","28","31","30","31","30","31","31","30","31","30","31");

                //CONSULTANDO EVENTOS
                $eventos= array();
                if($doc){
                $sql = "select * from agendamiento.fechaeventomedico(".$doctor.",".$fecha_calendario[1].",".$fecha_calendario[0].");";
                $query = Yii::app()->db->createCommand($sql)->query();
                foreach($query as $fila) {
                    $ini = $fila['fechainiciop'];
                    $fin = $fila['fechafinp'];
                    $eventos[$ini] = $fin;
                    while($ini < $fin){
                            $nuevafecha = strtotime ( '+1 day' , strtotime ( $ini ) ) ;
                            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                            $ini = $nuevafecha;
                            $eventos[$ini] = $fin;
                    }		
                }
                }
                //FIN DE CONSULTA EVENTO

                $meses=array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

                /* calculamos los días de la semana anterior al día 1 del mes en curso */
                $diasantes=$primeromes-1;

                /* los días totales de la tabla siempre serán máximo 42 (7 días x 6 filas máximo) */
                $diasdespues=42;

                /* calculamos las filas de la tabla */
                $tope=$dias[intval($fecha_calendario[1])]+$diasantes;
                if ($tope%7!=0) $totalfilas=intval(($tope/7)+1);
                else $totalfilas=intval(($tope/7));

                /* empezamos a pintar la tabla */
                echo "<h2 id='titulocalendario' rel = '".$fecha_calendario[0]."-".$fecha_calendario[1]."' >Calendario:  ".$meses[intval($fecha_calendario[1])]." del ".$fecha_calendario[0]." <abbr title='S&oacute;lo se pueden agregar eventos en d&iacute;as h&aacute;biles y en fechas futuras (o la fecha actual).'></abbr></h2>";
                if (isset($mostrar)) echo $mostrar;

                echo "<table class='calendario' cellspacing='0' cellpadding='0'>";
                        echo "<tr><th>Lunes</th><th>Martes</th><th>Mi&eacute;rcoles</th><th>Jueves</th><th>Viernes</th><th>S&aacute;bado</th><th>Domingo</th></tr><tr>";
                        /* inicializamos filas de la tabla */
                        $tr=0;
                        $dia=1;
                        $userdoct= Yii::app()->db->createCommand('select idrol from agendamiento.usuario where idusuario='.Yii::app()->user->id)->queryScalar();
                        if($userdoct != 2){
                            $fechalimite= Yii::app()->db->createCommand('select limiteagenda from agendamiento.datosgenerales;')->queryScalar();
                            $fechalimiteref= Yii::app()->db->createCommand('select limitereferencia from agendamiento.datosgenerales;')->queryScalar();
                        }
                        else{
                            $fechalimite= Yii::app()->db->createCommand('select limiteagendadoctor from agendamiento.datosgenerales;')->queryScalar();
                            $fechalimiteref= $fechalimite;
                        }
                        for ($i=1;$i<=$diasdespues;$i++)
                        {
                                if ($tr < $totalfilas)
                                {
                                        if ($i>=$primeromes && $i<=$tope)
                                        {       $ban=false;
                                                echo "<td class='";
                                                // creamos fecha completa 
                                                if ($dia<10) $dia_actual="0".$dia; 
                                                else $dia_actual=$dia;
                                                
                                                $fecha_completa = $fecha_calendario[0]."-".$fecha_calendario[1]."-".$dia_actual;
                                                
                                                $diaSemana = date("l", strtotime($fecha_completa));
                                                if($lun and ($doc or $esp) and $diaSemana == 'Monday' ){
                                                    echo 'atencion ';
                                                    $ban =true;
                                                }
                                                else if($mar and ($doc or $esp) and $diaSemana == 'Tuesday'){
                                                    echo 'atencion ';
                                                    $ban =true;
                                                }else 
                                                if($mie and ($doc or $esp) and $diaSemana == 'Wednesday'){
                                                    $ban =true;
                                                    echo 'atencion ';
                                                }else 
                                                if($jue and ($doc or $esp) and $diaSemana == 'Thursday'){
                                                    echo 'atencion ';
                                                    $ban =true;
                                                }else 
                                                if($vie and ($doc or $esp) and $diaSemana == 'Friday'){
                                                    echo 'atencion ';
                                                    $ban =true;
                                                }
                                                
                                                //EVENTOS GENERALES
                                                if (isset($eventos[$fecha_completa])) {
                                                if (intval($eventos[$fecha_completa])>0)
                                                {       
                                                    if($doc){ 
                                                        echo 'evento ';
                                                        $ban=false;
                                                    }
                                                    $hayevento=$eventos[$fecha_completa];
                                                        
                                                }
                                                }
                                                else $hayevento=0;
                                                //FIN DE EVENTOS

                                                // si es hoy coloreamos la celda
                                                if (date("Y-m-d")==$fecha_completa) echo("hoy");
                                                echo("'>");

                                                // recorremos el array de eventos para mostrar los eventos del d�a de hoy 
                                                //if ($hayevento>0) echo "<a href='#' data-evento='#evento".$dia_actual."' class='modal' rel='".$fecha_completa."' title='Hay ".$hayevento." eventos'>".$dia."</a>";
                                                if ($hayevento>0) echo "<a href='#' data-evento='#evento".$dia_actual."' class='falta' rel='".$fecha_completa."' title='Hay ".$hayevento." eventos'><span class='dia_calendario'>$dia </span></a>";
                                                else echo("<span class='dia_calendario'>$dia </span>");

                                                // agregamos enlace a nuevo evento si la fecha no ha pasado 
                                                $timer = explode(" ",$fecha_completa);
                                                $fecha2 = explode("-",$timer[0]);
                                                $fechex = $fecha2[2]."/".$fecha2[1]."/".$fecha2[0];

                                                $cortamos=explode("-",$fecha_completa);
                                                $pdia=$cortamos[2];
                                                $pmes=$cortamos[1];
                                                $pano=$cortamos[0];
                                                $fue=date("w",mktime(0,0,0,$pmes,$pdia,$pano));
                                                $resul = null;
                                                if (intval($fue)==0 || intval($fue)==6)
                                                    $resul = true;
                                                else
                                                    $resul = false;
                                                //imprime los signo mas para gregar evento
                                                if(!$info){
                                                if (((date("Y-m-d")<=$fecha_completa and $fecha_completa<=$fechalimite) or $isadmin) && $resul==false && $ban ) {
                                                    $classref = '';
                                                    if($fecha_completa>=$fechalimiteref){
                                                        $classref = 'limitereferencia';
                                                    }
                                                    $classadd = 'add agregar_evento';
                                                    if($doc){
                                                        $datosdia= Yii::app()->db->createCommand("select * from agendamiento.agendallenadatos(".$doctor.",'".$fecha_completa."');")->queryRow();
                                                        if($datosdia['lleno'])
                                                            $classadd = 'add agregar_evento_lleno';
                                                        if($fecha_completa>=$fechalimiteref){
                                                            $classref = 'limitereferencia';
                                                            $classadd = $classadd.' limiref';
                                                        }
                                                       print '<table class=""><tr><th>R</th><th>S</th><th>E</th></tr><tr><td class="'.$classref.'">'.$datosdia['refe'].'</td><td>'.$datosdia['sub'].'</td><td>'.$datosdia['ext'].'</td></tr></table>';    
                                                    }
                                                    echo "<a href='#' data-evento='#nuevo_evento' title='Agregar un Evento el ".$fechex."' class='".$classadd."' data-toggle='modal' data-target='#myModal' rel='".$fecha_completa."'>&nbsp;</a>";
                                                }
                                                }
                                                echo "</td>";
                                                $dia+=1;
                                        }
                                        else echo "<td class='desactivada'>&nbsp;</td>";
                                        if ($i==7 || $i==14 || $i==21 || $i==28 || $i==35 || $i==42) {echo "<tr>";$tr+=1;}
                                }
                        }
                        echo "</table>";
                        
                        $mesanterior=date("Y-m-d",mktime(0,0,0,$fecha_calendario[1]-1,01,$fecha_calendario[0]));
                        $messiguiente=date("Y-m-d",mktime(0,0,0,$fecha_calendario[1]+1,01,$fecha_calendario[0]));
                        echo "<p class='toggle'>&laquo; <a href='#' rel='$mesanterior' class='anterior'>Mes Anterior</a> - <a href='#' class='siguiente' rel='$messiguiente'>Mes Siguiente</a> &raquo;</p>";
                        return;
        }
        
        public function actionObtenerhora(){
             
            
            $medico = $_GET['medico'];
            $fecha = $_GET['fecha'];
            
            $idm = Yii::app()->db->createCommand("select idmedico from agendamiento.medico where (nombre || ' ' || apellido ) ='".$medico."'")->queryScalar();
            
            if($idm== null and $fecha == null){
            $idm=1;
            $fecha = '1900-11-18';
            }
            $sql =  Yii::app()->db->createCommand("select * from agendamiento.referencia(".$idm.",'".$fecha."')")->query();
            
            $tabla = Yii::app()->db->createCommand("select * from agendamiento.eventomedico(".$idm.",'".$fecha."')")->query();
            echo"<center>";
            echo "Eventos del médico";
            echo "<div class='datagrid'>";
            echo "<table>";
            echo "<thead><tr><th>Medico</th><th>Fecha</th><th>Fecha</th><th>Detalle</th></tr><thead>";
            foreach ($tabla as $rowt) {
                echo "<tr><td>".$rowt['idmedicop']." </td><td>".$rowt['fechainiciop']."</td><td>".$rowt['fechafinp']."</td><td>".$rowt['detalle']."</td></tr>";
            }
            echo "</table>";
            echo "</div>";
            echo "<hr>";
            echo "Pacientes de ese día";
            echo "<div align= \"center\" id=\"datos\">"; 
            echo "<div class='datagrid'>";
            echo "<table>";     
            echo "<thead><tr><th>Nombre</th><th>Télefono </th><th>Tipo</th><th>Fecha</th><th>Seleccionar</th></tr><thead>";
            foreach ($sql as $row) {   
            echo "<tr><td align=center>".$row['pacientep']." </td><td align=center>".$row['telefonop']."</td><td align=center>".$row['tipo']."</td><td align=center>".$row['fechap']."</td><td align=center>".CHtml::Checkbox('seleccionar', false, array('value'=>$row['idpaci'],'class'=>'chbutton', 
            'onclick'=>"if(this.checked == true) agregar('".$row['idpaci']."'); "
            . " else borrar('".$row['idpaci']."');"))."</td></tr>";
            }
            echo"<tr><th>Seleccionar todos ".CHtml::Checkbox('todos', false, array('value'=>false, 
            'onclick'=>"if(this.checked == true) verdadero(); "
            . " else falso();"))."</th></tr>";
            echo "</table>";
            echo "</div>";
            echo "</div>";
            echo"</center>";
            
        }
        
        public function actionObtenerPacientesReagendar(){
         $idusu = Yii::app()->user->Id;
            $resp = Medico::model()->find('idusuario='.$idusu);
            $medico = $resp->idmedico;
            $fecha = $_GET['fecha'];
            
            $pacientes = Yii::app()->db->createCommand("select * from agendamiento.subsecuente(".$medico.",'".$fecha."')")->query();
            $eventos = Yii::app()->db->createCommand("select * from agendamiento.eventomedico(".$medico.",'".$fecha."')")->query();
            echo"<center>";
            echo "Eventos del médico";
            echo "<div class='datagrid'>";
            echo "<table>";
            echo "<thead><tr><th>Medico</th><th>Fecha</th><th>Fecha</th><th>Detalle</th></tr><thead>";
            $ban = true;
            foreach ($eventos as $rowt) {
                $ban=false;
                echo "<tr><td>".$rowt['idmedicop']." </td><td>".$rowt['fechainiciop']."</td><td>".$rowt['fechafinp']."</td><td>".$rowt['detalle']."</td></tr>";
            }
            if($ban){
                echo "<tr><td>No tiene eventos</td><td></td><td></td><td></td></tr>";
            }
            echo "</table>";
            echo "</div>";
            echo "<hr>";
            echo "Pacientes de ese día";
            echo "<div class='datagrid'>";
            echo "<table>";
            echo "<thead><tr><th>Nombre</th><th>Télefono</th><th>Tipo</th><th>Fecha</th><th>Atención</th><th>Seleccionar</th></tr><thead>";
            $ban = true;
            foreach ($pacientes as $row) {
                 $ban=false;
            echo "<tr>"
                . "<td>".$row['pacientep']." </td>"
                . "<td>".$row['telefonop']."</td>"
                . "<td>".$row['tipo']."</td>"
                . "<td>".$row['fechap']."</td>"
                . "<td>No Atendido</td>"
                . "</tr>";
            
            }
            if($ban){
                echo "<tr><td>No tiene pacientes ese día</td><td></td><td></td><td></td><td></td></tr>";
            }
            echo "</table>";
            echo "</div>";
            echo"</center>";
           
        }
        
        public function actionEnviar(){
                    $medico = $_GET['medico'];
                    $medico2 = $_GET['medico2'];
                    $fecha = $_GET['fecha'];
                    $fechaReag = $_GET['fecha2'];
                    $val1 = $_GET['valor1'];
                    $val2 = $_GET['valor2'];
                    //array que se manda al seleccionar checkbox
                    $datos=array($_GET['datos']);
                    //numero de elementos del array
                    $contador = count($datos, COUNT_RECURSIVE)-1;
        $date = date_format(date_create(), 'Y-m-d H:i:s');

                    $usu = Yii::app()->user->name;
                    $es = true;
                    $idm = Yii::app()->db->createCommand("select idmedico from agendamiento.medico where (nombre || ' ' || apellido ) ='".$medico."'")->queryScalar();
                    $idm2 = Yii::app()->db->createCommand("select idmedico from agendamiento.medico where (nombre || ' ' || apellido ) ='".$medico2."'")->queryScalar();
                    $cont =1;
                    $vuel = 0;
                      
                    //$fecha2 = "";
                    $sql = Yii::app()->db->createCommand("select * from agendamiento.agenda where agenda.idmedico = ".$idm." and agenda.fecha ='".$fecha."' order by agenda.idagenda")->query();
                    if ($val1 == 0 AND $val2 ==0){
                    foreach ($sql as $row) {                      
                        $vuel++;
                        $tipoagenda = 5;
                        $idpac = $row['idpaciente'];
                        $idmed = $row['idmedico'];
                        $idsub = $row['idsubcentro'];
                        $nombre = $row['nombre'];
                        $tele = $row['telefono'];
                        $cel =$row['celular'];
                        $idagenda = $row ['idagenda'];
                        
                        $fechaa = $fecha;
            
                       $fecha2 =  date('d-m-Y',strtotime('+'.$cont.' days', strtotime($fechaa)));
                        $dia=date("w", strtotime($fecha2));
                        
                        if($dia=="0"){
    
                        $fecha2 = date('d-m-Y',strtotime('+1 days', strtotime($fecha2)));
                        $cont++;
    
                        }else if ($dia=="6"){
                        $fecha2 = date('d-m-Y',strtotime('+2 days', strtotime($fecha2)));
                                    $cont +=2;
                        }
                        if($vuel==2){
                            $vuel = 0;
                            $cont++;
                        }    
                        //checkbox
                        //recorrer array de idpacientes
                        for($j=0;$j<=$contador-1;$j++){
                        foreach ($datos as $key){
                            if($key[$j]==$idpac){
                                $sqll= Yii::app()->db->createCommand(
                                "insert into agendamiento.agenda(idtipoagenda,idpaciente,idmedico,idsubcentro,nombre,fecha,estado,creadopor,modificadopor,fechacreado,fechamodificado,telefono,celular,idreferencia) "
                                . "VALUES(".$tipoagenda.",".$idpac.",".$idmed.",".$idsub.",'".$nombre."','".$fecha2."','".$es."','".$usu."','".$usu."','".$date."','".$date."','".$tele."','".$cel."',".$idagenda.")")->execute();
                                Yii::app()->db->createCommand("update agendamiento.agenda set reagendado = 1 where idagenda =".$idagenda)->execute();
                                
                                //$hola2= $hola2.$key[$j]." ";
                                
                            }
                        }
                        
                        }
                            
                               }
                    
                       }else if($val1 == 1 AND $val2 ==0){
                           $cont--;
                            foreach ($sql as $row) {                      
                        $vuel++;
                        $tipoagenda = 5;
                        $idpac = $row['idpaciente'];
                        $idmed = $row['idmedico'];
                        $idsub = $row['idsubcentro'];
                        $nombre = $row['nombre'];
                        
                        
                        $fechcre=$date;
                        $fechamod = $date;
                        $crepo = $usu;
                        $modpo = $usu;
                        $tele = $row['telefono'];;
                        $cel =$row['celular'];;
                        $idagenda = $row ['idagenda'];
                        
                        $fechaa = $fechaReag;
                        $fecha2 =  date('d-m-Y',strtotime('+'.$cont.' days', strtotime($fechaa)));
                        $dia=date("w", strtotime($fecha2));
                        
                        if($dia=="0"){
    
                        $fecha2 = date('d-m-Y',strtotime('+1 days', strtotime($fecha2)));
                        $cont++;
    
                        }else if ($dia=="6"){
                        $fecha2 = date('d-m-Y',strtotime('+2 days', strtotime($fecha2)));
                                    $cont +=2;
                        }
                        
                        
                        if($vuel==2){
                            $vuel = 0;
                            $cont++;
                        }
                         //checkbox
                        //recorrer array de idpacientes
                        for($j=0;$j<=$contador-1;$j++){
                        foreach ($datos as $key){
                            if($key[$j]==$idpac){
                               $sqll= Yii::app()->db->createCommand(
                               "insert into agendamiento.agenda(idtipoagenda,idpaciente,idmedico,idsubcentro,nombre,fecha,estado,creadopor,modificadopor,fechacreado,fechamodificado,telefono,celular,idreferencia) "
                               . "VALUES(".$tipoagenda.",".$idpac.",".$idmed.",".$idsub.",'".$nombre."','".$fecha2."','".$es."','".$usu."','".$usu."','".$date."','".$date."','".$tele."','".$cel."',".$idagenda.")")->execute();
                                Yii::app()->db->createCommand("update agendamiento.agenda set reagendado = 1 where idagenda =".$idagenda)->execute();
 
                               }
                        }
                        
                        }
                           
                         } 
                    }else if($val1 == 1 AND $val2 ==1){
                        
                          $cont--;
                            foreach ($sql as $row) {                      
                        $vuel++;
                        $tipoagenda = 5;
                        $idpac = $row['idpaciente'];
                        $idmed = $row['idmedico'];
                        $idsub = $row['idsubcentro'];
                        $nombre = $row['nombre'];
                        
                        
                        $fechcre=$date;
                        $fechamod = $date;
                        $crepo = $usu;
                        $modpo = $usu;
                        $tele = $row['telefono'];;
                        $cel =$row['celular'];;
                        $idagenda = $row ['idagenda'];
                        
                        $fechaa = $fechaReag;
                        $fecha2 =  date('d-m-Y',strtotime('+'.$cont.' days', strtotime($fechaa)));
                        $dia=date("w", strtotime($fecha2));
                        
                        if($dia=="0"){
    
                        $fecha2 = date('d-m-Y',strtotime('+1 days', strtotime($fecha2)));
                        $cont++;
    
                        }else if ($dia=="6"){
                        $fecha2 = date('d-m-Y',strtotime('+2 days', strtotime($fecha2)));
                                    $cont +=2;
                        }
                        
                        
                        if($vuel==2){
                            $vuel = 0;
                            $cont++;
                        }
                        
                        //checkbox
                        //recorrer array de idpacientes
                        for($j=0;$j<=$contador-1;$j++){
                        foreach ($datos as $key){
                            if($key[$j]==$idpac){
                               $sqll= Yii::app()->db->createCommand(
                               "insert into agendamiento.agenda(idtipoagenda,idpaciente,idmedico,idsubcentro,nombre,fecha,estado,creadopor,modificadopor,fechacreado,fechamodificado,telefono,celular,idreferencia) "
                               . "VALUES(".$tipoagenda.",".$idpac.",".$idm2.",".$idsub.",'".$nombre."','".$fecha2."','".$es."','".$usu."','".$usu."','".$date."','".$date."','".$tele."','".$cel."',".$idagenda.")")->execute();
                               Yii::app()->db->createCommand("update agendamiento.agenda set reagendado = 1 where idagenda =".$idagenda)->execute();
                       
                               }
                        }
                        
                        }
                         
                           
                         } 
                        
                    }else if ($val1 == 0 AND $val2 ==1){
                          $vuel++;
                        $tipoagenda = 5;
                        $idpac = $row['idpaciente'];
                        $idmed = $row['idmedico'];
                        $idsub = $row['idsubcentro'];
                        $nombre = $row['nombre'];
                        $tele = $row['telefono'];;
                        $cel =$row['celular'];;
                        $idagenda = $row ['idagenda'];
                        
                        $fechaa = $fecha;
            
                       $fecha2 =  date('d-m-Y',strtotime('+'.$cont.' days', strtotime($fechaa)));
                        $dia=date("w", strtotime($fecha2));
                        
                        if($dia=="0"){
    
                        $fecha2 = date('d-m-Y',strtotime('+1 days', strtotime($fecha2)));
                        $cont++;
    
                        }else if ($dia=="6"){
                        $fecha2 = date('d-m-Y',strtotime('+2 days', strtotime($fecha2)));
                                    $cont +=2;
                        }
                        if($vuel==2){
                            $vuel = 0;
                            $cont++;
                        }   
                        
                        //checkbox
                        //recorrer array de idpacientes
                        for($j=0;$j<=$contador-1;$j++){
                        foreach ($datos as $keys){
                            if($keys[$j]==$idpac){
                               $sqll= Yii::app()->db->createCommand(
                               "insert into agendamiento.agenda(idtipoagenda,idpaciente,idmedico,idsubcentro,nombre,fecha,estado,creadopor,modificadopor,fechacreado,fechamodificado,telefono,celular,idreferencia) "
                               . "VALUES(".$tipoagenda.",".$idpac.",".$idm2.",".$idsub.",'".$nombre."','".$fecha2."','".$es."','".$usu."','".$usu."','".$date."','".$date."','".$tele."','".$cel."',".$idagenda.")")->execute();
                               Yii::app()->db->createCommand("update agendamiento.agenda set reagendado = 1 where idagenda =".$idagenda)->execute();
                    
                               }
                        }
                        
                        }
                            
                       }
                        if($sqll > 0 ){
                            echo "Datos insertados";
                        }else {
                            
                        }
                        
                        
                                
                    }  
                    
                     public function actionReagendarMed(){
                     $idusu = Yii::app()->user->Id;
                     $resp = Medico::model()->find('idusuario='.$idusu);
                    $medico = $resp->idmedico;
                    $fecha = $_GET['fecha'];
                    $fechaReag = $_GET['fecha2'];
                    $date = date_format(date_create(), 'Y-m-d H:i:s');
                    $usu = Yii::app()->user->name;
                    $es = true;
                    
                    $cont =1;
                    $vuel = 0;
                    
                    //$fecha2 = "";
                    $sql = Yii::app()->db->createCommand("select * from agendamiento.agenda where idtipoagenda = 2 and reagendado = 0 and agenda.idmedico = ".$medico." and agenda.fecha ='".$fecha."' order by agenda.idagenda")->query();
                    if ($fechaReag==""){
                    foreach ($sql as $row) {                      
                        $vuel++;
                        $tipoagenda = 6;
                        
                        $idpac = $row['idpaciente'];
                        $idmed = $row['idmedico'];
                        $idsub = $row['idsubcentro'];
                        $nombre = $row['nombre'];
                        $tele = $row['telefono'];;
                        $cel =$row['celular'];;
                        $idagenda = $row ['idagenda'];
                        
                        $fechaa = $fecha;
            
                       $fecha2 =  date('d-m-Y',strtotime('+'.$cont.' days', strtotime($fechaa)));
                        $dia=date("w", strtotime($fecha2));
                        
                        if($dia=="0"){
    
                        $fecha2 = date('d-m-Y',strtotime('+1 days', strtotime($fecha2)));
                        $cont++;
    
                        }else if ($dia=="6"){
                        $fecha2 = date('d-m-Y',strtotime('+2 days', strtotime($fecha2)));
                                    $cont +=2;
                        }
                        if($vuel==2){
                            $vuel = 0;
                            $cont++;
                        }    
                            
                       $sqll= Yii::app()->db->createCommand(
"insert into agendamiento.agenda(idtipoagenda,idpaciente,idmedico,idsubcentro,nombre,fecha,estado,creadopor,modificadopor,fechacreado,fechamodificado,telefono,celular,idreferencia) "
                        . "VALUES(".$tipoagenda.",".$idpac.",".$medico.",".$idsub.",'".$nombre."','".$fecha2."','".$es."','".$usu."','".$usu."','".$date."','".$date."','".$tele."','".$cel."',".$idagenda.")")->execute();
                        Yii::app()->db->createCommand("update agendamiento.agenda set reagendado = 1 where idagenda =".$idagenda)->execute(); 
                    }
                    
                       }else{
                           $cont--;
                            foreach ($sql as $row) {                      
                        $vuel++;
                        $tipoagenda = 6;
                        $idpac = $row['idpaciente'];
                        $idmed = $row['idmedico'];
                        $idsub = $row['idsubcentro'];
                        $nombre = $row['nombre'];
                        $idagenda = $row['idagenda'];
                        
                        $fechcre=$date;
                        $fechamod = $date;
                        $crepo = $usu;
                        $modpo = $usu;
                        $tele = $row['telefono'];;
                        $cel =$row['celular'];;
                        $idagenda = $row ['idagenda'];
                        
                        $fechaa = $fechaReag;
                        $fecha2 =  date('d-m-Y',strtotime('+'.$cont.' days', strtotime($fechaa)));
                        $dia=date("w", strtotime($fecha2));
                        
                        if($dia=="0"){
    
                        $fecha2 = date('d-m-Y',strtotime('+1 days', strtotime($fecha2)));
                        $cont++;
    
                        }else if ($dia=="6"){
                        $fecha2 = date('d-m-Y',strtotime('+2 days', strtotime($fecha2)));
                                    $cont +=2;
                        }
                        
                        
                        if($vuel==2){
                            $vuel = 0;
                            $cont++;
                        }
                        
                           
                         $sqll= Yii::app()->db->createCommand(
"insert into agendamiento.agenda(idtipoagenda,idpaciente,idmedico,idsubcentro,nombre,fecha,estado,creadopor,modificadopor,fechacreado,fechamodificado,telefono,celular,idreferencia) "
                       . "VALUES(".$tipoagenda.",".$idpac.",".$medico.",".$idsub.",'".$nombre."','".$fecha2."','".$es."','".$usu."','".$usu."','".$date."','".$date."','".$tele."','".$cel."',".$idagenda.")")->execute();
                          Yii::app()->db->createCommand("update agendamiento.agenda set reagendado = 1 where idagenda =".$idagenda)->execute(); 
                       } 
                    }
                    
                        
                        if($sqll > 0 ){
                            echo "Datos insertados";
                        }
                        
                        
                                 
                    }
                    
                    
                    
                    
                    //funcion para busqueda
        public function actionReporteDoctorPacAgend(){
                                      
                $model=new Agenda('busqueda1');
		$model->unsetAttributes();  // clear any default values
                if(isset($_GET['Agenda']))
			$model->attributes=$_GET['Agenda'];
                                
		$this->render('reporDoctorPacAgend',array(
			'model'=>$model,
		));
        }
        
        public function actionReporte2(){
                if(isset($_GET['fecha_inicial']) and isset($_GET['fecha_final'])){
                $f3 = $_GET['fecha_inicial'];
                $f4 = $_GET['fecha_final'];                  
                }
                $model=new Agenda('busqueda2');
		$model->unsetAttributes();  // clear any default values
                if(isset($_GET['Agenda']))
			$model->attributes=$_GET['Agenda'];
                if(isset($_GET['fecha_inicial']) and isset($_GET['fecha_final'])){
                
                $this->render('reporte2',array(
			'model'=>$model,'fecha_inicial'=>$f3,'fecha_final'=>$f4,
		));    
                }
                
		$this->render('reporTipoAgenda',array(
			'model'=>$model,
		));
        }
        //reporte 3
        public function actionReportePacientesAgendados(){
                                 
                $model=new Agenda('busqueda3');
		$model->unsetAttributes();  // clear any default values
                if(isset($_GET['Agenda']))
		$model->attributes=$_GET['Agenda'];
     
		$this->render('reporPacientesAgendados',array(
			'model'=>$model,
		));
        }
             
        public function actionReporte4(){
                if(isset($_GET['fecha_inicial']) and isset($_GET['fecha_final'])){
                $f1 = $_GET['fecha_inicial'];
                $f2 = $_GET['fecha_final'];                  
                }
                            
                $model=new Agenda('busqueda2');
		$model->unsetAttributes();  // clear any default values
                if(isset($_GET['Agenda']))
			$model->attributes=$_GET['Agenda'];
                if(isset($_GET['fecha_inicial']) and isset($_GET['fecha_final'])){
                
                $this->render('reporte4',array(
			'model'=>$model,'fecha_inicial'=>$f1,'fecha_final'=>$f2,
		));    
                }
                
		$this->render('reporPaci15Dias',array(
			'model'=>$model,
		));
        }
        //
        public function actionPacientesdeldia(){
                $model=new Agenda('busquedaPacienteDia');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Agenda']))
			$model->attributes=$_GET['Agenda'];
		$this->render('reportepacientesdia',array(
			'model'=>$model,
		));
        }
        public function actionReporte5(){
                if(isset($_GET['fecha_inicial']) and isset($_GET['fecha_final'])){
                $f1 = $_GET['fecha_inicial'];
                $f2 = $_GET['fecha_final'];                  
                }
                         
                $model=new Agenda('busqueda5');
		$model->unsetAttributes();  // clear any default values
                if(isset($_GET['Agenda']))
			$model->attributes=$_GET['Agenda'];
                if(isset($_GET['fecha_inicial']) and isset($_GET['fecha_final'])){
                
                $this->render('reporte5',array(
			'model'=>$model,'fecha_inicial'=>$f1,'fecha_final'=>$f2,
		));    
                }
                
		$this->render('reporDoctor15Dias',array(
			'model'=>$model,
		));
        }   
        //reporte 6
        public function actionreportemedico(){
            $dataprovider = new CactiveDataProvider('Agenda');
            $this->render('reporte_medico', array(
            'model'=>$dataprovider,
            ));
        }   
        
        //generar pdf 1
      public function actionGenerarPdfDoctorPacAgend()
      {
          $val = $_GET['val'];
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_1']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_1']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
          $this->renderPartial('ReportDoctorPacAgend', array('model'=>$model,'val'=>$val), true);
         
         
       }
       //generar excel doctor
        public function actionGenerarExcelDoctor(){
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_1']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_1']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
          $contenido = $this->renderPartial("ExcelDoctor", array('model'=>$model,), true);
          Yii::app()->request->sendFile('Reporte_Doctor'.date('YmdHis').'.xls', $contenido);
          exit;
       }
       //generar pdf2
       public function actionGenerarPdf2()
      {
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_2']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_2']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
         $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
         $mPDF1->useOnlyCoreFonts = true;
         $mPDF1->SetTitle("Reporte");
         $mPDF1->SetAuthor("Hospital");
         //$mPDF1->SetWatermarkText("Pacientes");
         $mPDF1->showWatermarkText = true;
         $mPDF1->watermark_font = 'DejaVuSansCondensed';
         $mPDF1->watermarkTextAlpha = 0.1;
         $mPDF1->SetDisplayMode('fullpage');
         $mPDF1->WriteHTML($this->renderPartial('ReportAgenda', array('model'=>$model), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
         $mPDF1->Output('Reporte_Pacientes_TipoAgenda'.date('YmdHis'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;
       }
       //generar excel tipo agenda
        public function actionGenerarExcelAgenda(){
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_2']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_2']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
          $contenido = $this->renderPartial("ExcelAgenda", array('model'=>$model,), true);
          Yii::app()->request->sendFile('Reporte_Agenda'.date('YmdHis').'.xls', $contenido);
          exit;
       }
       //generar pdf 3
        public function actionGenerarPdfPacientesAgendados()
      {
            $val = $_GET['val'];
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_3']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_3']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
          $this->renderPartial('ReportPacientesAgendados', array('model'=>$model,'val'=>$val), true);
       }
       //generar excel pacientes
       public function actionGenerarExcelPacientesAgendados(){
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_3']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_3']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
          $contenido = $this->renderPartial("ExcelPacientesAgendados", array('model'=>$model,), true);
          Yii::app()->request->sendFile('Reporte_PacientesAgendados'.date('YmdHis').'.xls', $contenido);
          exit;
       }
       
       //generar pdf 4 
      public function actionGenerarPdf4()
      {
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_4']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_4']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
         $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
         $mPDF1->useOnlyCoreFonts = true;
         $mPDF1->SetTitle("Reporte");
         $mPDF1->SetAuthor("Hospital");
         //$mPDF1->SetWatermarkText("Pacientes");
         $mPDF1->showWatermarkText = true;
         $mPDF1->watermark_font = 'DejaVuSansCondensed';
         $mPDF1->watermarkTextAlpha = 0.1;
         $mPDF1->SetDisplayMode('fullpage');
         $mPDF1->WriteHTML($this->renderPartial('ReportPaci15Dias', array('model'=>$model), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
         $mPDF1->Output('Reporte_Pacientes_15Dias'.date('YmdHis'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;
       }
       //generar excel pacientes ultimos 15 dias
       public function actionGenerarExcelPaci15Dias(){
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_4']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_4']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
          $contenido = $this->renderPartial("ExcelPaci15Dias", array('model'=>$model,), true);
          Yii::app()->request->sendFile('Reporte_Paci15Dias'.date('YmdHis').'.xls', $contenido);
          exit;
       }
       
       public function actionlistareagendados()
       {
        //$tabla = Yii::app()->db->createCommand("select * from agendamiento.reagendadosreferencias")->query();
             $dateinicio = $_GET['fechainicio'];
             $datefinal = $_GET['fechafinal'];
             $medico = $_GET['medico'];
           $tabla = Yii::app()->db->createCommand("select * from agendamiento.reagendadosreferencia(".$medico.",'".$dateinicio."','".$datefinal."')")->query();
     
            echo "<div class='datagrid'>";
            echo "<table>";
            echo "<thead><tr><th>Paciente</th><th>Fecha de la Cita</th><th>Fecha de Reagendamiento</th><th>Profesional de la salud</th><th>Profesional de la Salud Asignado </th></tr><thead>";
            foreach ($tabla as $rowt) {
                echo "<tr><td>".$rowt['pac']." </td><td>".$rowt['fecita']."</td><td>".$rowt['fecre']."</td><td>".$rowt['medci']."</td><td>".$rowt['medre']."</td></tr>";
            }
            echo "</table>";
            echo "</div>";
            echo "<hr>";
       }
       
       public function actionlistareagendados2()
       {
        //$tabla = Yii::app()->db->createCommand("select * from agendamiento.reagendadosreferencias")->query();
             $dateinicio2 = $_GET['fechainicio2'];
             $datefinal2 = $_GET['fechafinal2'];
             $usuario = Yii::app()->user->id;
             // $tabla = Yii::app()->db->createCommand("select * from agendamiento.reagendadosreferencia('".$dateinicio2."','".$datefinal2."')")->query();
     
           $tabla2 = Yii::app()->db->createCommand("select * from agendamiento.reagendadossubsecuente(4,'2014-12-12','2014-12-30')")->query();
     
            echo "<div class='datagrid'>";
            echo "<table>";
            echo "<thead><tr><th>Paciente</th><th>Fecha de la Cita</th><th>Fecha de Reagendamiento</th><th>Profesional de la Salud</th><th><thead>";
            foreach ($tabla2 as $rowt) {
                echo "<tr><td>".$rowt['pac']." </td><td>".$rowt['fecita']."</td><td>".$rowt['fecre']."</td><td>".$rowt['med']."</td></tr>";
            }
            echo "</table>";
            echo "</div>";
            echo "<hr>";
            
            
            
       }
       
       public function actionPdfpacientedia(){
           $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_dia']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_dia']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
         $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
         $mPDF1->useOnlyCoreFonts = true;
         $mPDF1->SetTitle("Reporte");
         $mPDF1->SetAuthor("Hospital");
         //$mPDF1->SetWatermarkText("Pacientes");
         $mPDF1->showWatermarkText = true;
         $mPDF1->watermark_font = 'DejaVuSansCondensed';
         $mPDF1->watermarkTextAlpha = 0.1;
         $mPDF1->SetDisplayMode('fullpage');
         $mPDF1->WriteHTML($this->renderPartial('ReportPacientesDia', array('model'=>$model), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
         $mPDF1->Output('ReportPacientesDia'.date('YmdHis'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;
       }
       
       //generar pdf 5
      public function actionGenerarPdf5()
      {
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_5']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_5']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
         $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
         $mPDF1->useOnlyCoreFonts = true;
         $mPDF1->SetTitle("Reporte");
         $mPDF1->SetAuthor("Hospital");
         //$mPDF1->SetWatermarkText("Pacientes");
         $mPDF1->showWatermarkText = true;
         $mPDF1->watermark_font = 'DejaVuSansCondensed';
         $mPDF1->watermarkTextAlpha = 0.1;
         $mPDF1->SetDisplayMode('fullpage');
         $mPDF1->WriteHTML($this->renderPartial('ReportDoctorPaci15Dias', array('model'=>$model), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
         $mPDF1->Output('Reporte_Pacientes_15Dias_Doctor'.date('YmdHis'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;
       }
       //generar excel pacientes ultimos 15 dias por doctor
       public function actionGenerarExcelDoctorPaci15Dias(){
          $session=new CHttpSession;
          $session->open();
          if(isset($session['reporte_5']))
          //Si hay datos filtrados entonces usar la criteria guardada en la sesion (esto lo guardamos en la funcion search() del modelo)
          {
          $model=Agenda::model()->findAll($session['reporte_5']);
          }
          else
          //Si no hay datos filtrados exportar todo
          {
          $model =Agenda::model()->findAll(); //Consulta para buscar todos los registros
          }
          $contenido = $this->renderPartial("ExcelDoctorPaci15Dias", array('model'=>$model,), true);
          Yii::app()->request->sendFile('Reporte_DoctorPaci15Dias'.date('YmdHis').'.xls', $contenido);
          exit;
       }
            
       public function actionReporteexcelespecialidadantidad(){
           if(isset($_GET['f1']) and isset($_GET['f2'])and isset($_GET['espe'])):
            $dateinicio = $_GET['f1'];
             $datefinal = $_GET['f2'];
             $especialidad = $_GET['espe'];
             else:
                 $dateinicio = date('d-m-Y');
             $datefinal = date('d-m-Y');
             $especialidad = 19;
                 
             endif;
             
            
         $tabla = Yii::app()->db->createCommand("select * from agendamiento.pacientespecialidad(".$especialidad.",'".$dateinicio."','".$datefinal."')")->query();
         $contenido = $this->renderPartial("ReporteEspe", array('model'=>$tabla,), true);
          Yii::app()->request->sendFile('Reporte_Espe'.date('YmdHis').'.xls', $contenido);
          exit;
         
       }

       public $fechaini ;
       public $fechafi ;
       public $especi ;
        //especialidad
        public function actionlistaespecialidad()
       {
             $dateinicio = $_GET['fechainicio'];
             $datefinal = $_GET['fechafinal'];
             $especialidad = $_GET['especialidad1'];
             $this->fechaini=$dateinicio;
             $this->fechafi=$datefinal;
             $this->especi= $especialidad;
             $tabla = Yii::app()->db->createCommand("select * from agendamiento.pacientespecialidad(".$especialidad.",'".$dateinicio."','".$datefinal."')")->query();
             
             echo "<div class='datagrid'>";
             echo "<table>";
             echo "<thead><tr><th>Profesional de la salud </th><th>Cantidad de pacientes</th></tr><thead>";
             foreach ($tabla as $rowt) {
                echo "<tr><td>".$rowt['pacientes']." </td><td>".$rowt['numpacientes']."</td></tr>";
             }
             echo "</table>";
             echo "</div>";
             echo "<hr>";
             //$this->actionPrueba($dateinicio,$datefinal,$especialidad);
             
             
       }
       public function actionHorarioinformacion($doctor){
           $model = Dia::model()->findAll("idmedico=$doctor and estado = true order by iddia");
           $textTable = '<table>';
           $textTable .= '<thead><tr><th>Día</th><th>Hora Inicio atención</th><th>Hora Fin atención</th></tr></thead><tbody>';
           foreach ($model as $row) {
               $textTable .= "<tr>";
               $textTable .= "<td>".$row["dia"]."</td>";
               $textTable .= "<td>".$row["horainicio"]."</td>";
               $textTable .= "<td>".$row["horafin"]."</td>";
               $textTable .= "</tr>";
           }
           $textTable .= '</tbody></table>';
           print $textTable;
       }
       public function actionObtenerhorario($idmedico, $fecha){
          $turnos = Yii::app()->db->createCommand("select agendamiento.valorturnototal(".$idmedico.",'".$fecha."');")->queryScalar();
          $turnosocupados = Yii::app()->db->createCommand("select numeroturno from agendamiento.agenda where idmedico =".$idmedico." and fecha = '".$fecha."';")->queryColumn();
          $resul = "<label>Turno:</label><br>";
          $activar=true;
          for($i=1;$i<=$turnos;$i++){
            $hora = Yii::app()->db->createCommand("select inicio from agendamiento.horapaciente(".$idmedico.",'".$fecha."',".$i.");")->queryScalar();
            $hora = substr($hora, 0, -3);
            $existe = false;
            foreach ($turnosocupados as $key => $value) {if($value==$i) {$existe = true;}}
            
            if($existe){
                $resul = $resul.'<label name="hora" class="btn btn-info btn-sm" disabled="disabled">
                <input type="radio" name="Agenda[numeroturno]" value="'.$i.'" />'.$hora.'</label>';
            }
            else if($activar){
                $resul = $resul.'<label name="hora" class="btn btn-default btn-sm active" >
                <input type="radio" name="Agenda[numeroturno]" value="'.$i.'" />'.$hora.'</label>';
                $activar=false;
            }
            else{
                $resul = $resul.'<label name="hora" class="btn btn-default btn-sm" >
                <input type="radio" name="Agenda[numeroturno]" value="'.$i.'" />'.$hora.'</label>';
            }
          }
          print $resul;
       }
    
       public function actionObtenermedico($idespecialidad){
           $resp = Medico::model()->findAllByAttributes(array('idespecialidad'=>$idespecialidad,'estado'=>true));
           header("Content-type: application/json");
           echo CJSON::encode($resp);
        }
        public function actionConsultahistorial($historial){
            if(strlen($historial)<10):
                $resp = Paciente::model()->findAllByAttributes(array('historialclinico'=>$historial,'estado'=>true));
            else:
                $resp = Paciente::model()->findAllByAttributes(array('cedula'=>$historial,'estado'=>true));
            endif;
            if(!$resp):
                $link = mssql_connect('10.68.32.49', 'uleam', 'HRZuleam');
                mssql_select_db('SISHOSP', $link);
                if(strlen($historial)<10):
                    $result = mssql_query("exec dbo.SHOSP_SEARCH_INDICE '$historial',' ',' ',' ','3',1;");
                else:
                    $result = mssql_query("exec dbo.SHOSP_SEARCH_INDICE '$historial',' ',' ',' ','4',1;");
                endif;
                while ($row = mssql_fetch_array($result)) {
                    $resp[0]['apellido'] = trim($row['APE01']).' '.trim($row['APE02']);
                    $resp[0]['nombre'] = trim($row['NAME01']);
                    $resp[0]['telefono'] = '';
                    $resp[0]['celular'] = '';
                    $resp[0]['idpaciente'] = '';
                }
            endif;
            header("Content-type: application/json");
            echo CJSON::encode($resp);
           
        }
        function datosnodoctor(){
            /* crear la busqueda de doctores*/
            print '<table>';
            print '<tr>';
            print '<td style="width:120px;"><strong>';
            print  CHtml::label('Especialidad','especialidad');
            print '</strong></td>';
             $datos = CHtml::listData(Especialidadmedica::model()->findAll('estado = true order by especialidad'),'idespecialidad','especialidad');
            print '<td>';
            print CHtml::dropDownList('especialidad',0,$datos, array('empty'=>'-Seleccione Especialidad-','class'=>'span5 form-control'));
            print '</td>';
            print '<td>';
            print CHtml::label('Profesional de la salud','profesional');
            print '</td>';
            print '<td>';
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'name'=>'profesional_salud',
                    //'model'=>$model,
                    'source'=>$this->createUrl('agenda/autocompletarprofesional'),
                    'options'=>array
                    (
                        'showAnim'=>'fold',
                        'select'=>"js:function(event, ui) 
                        {
                            $('#especialidad').val(ui.item.especialidad);
                            $('#especialidad').change();
                            busq= true;
                            ID=ui.item.id;
                        }",
                    ),
                    'htmlOptions'=>array
                    (
                        'size'=>35,
                        'maxlength'=>50,
                        'placeholder'=>'Profesional de la salud',
                        'title'=>'Indique el Profesional',
                        'style'=>'position:initial;',
                        'class'=>'span5 form-control',
                        'onkeyup'=>'this.value=this.value.toUpperCase()',
                    ),
                    ));
            print '</td> </tr>     </table>';
            print '<div class="doctores"><h1><small>Profesionales</small></h1><div id="cssmenu"></div></div>';
        }
     function datosdoctor($ident){
         /* cargar los datos del doctor*/
        $medico = Medico::model()->find('idmedico = '.$ident);
        print  '<div class="doctores">';
        print '<h2><small>';
        print $medico->titulo.' '.$medico->nombre.' '.$medico->apellido;
        print '</small></h2>';
        print '<label>Identificación: </label>'.$medico->cedula .'<br>';
        print '<label>Especialidad: </label>'.$medico->idespecialidad1->especialidad.'<br>';
        print '<label>Teléfono: </label>'.$medico->telefono.'<br>';
        print '<label>Tiempo por Consulta: </label>'.$medico->intervalo.' minutos<br>';
        print '<div id="cssmenu"></div>';
        print '</div>';
     }
     
     public function actionReporteindicadoresgpr(){
         if(isset($_GET['anio']) and isset($_GET['mes'])):
$anio = $_GET['anio'];
$mes = $_GET['mes'];

else:
 $mes = date('m');
 $anio = date('Y');

endif;

 $fechaini = "$anio-$mes-01";

$fechafin = "$anio-$mes-".date('t', strtotime($fechaini));
        
          $sqlespe = Yii::app()->db->createCommand("select especialidad,
(select count(*)
from agendamiento.agenda 
join agendamiento.medico on medico.idmedico = agenda.idmedico 
join agendamiento.especialidadmedica on especialidadmedica.idespecialidad = medico.idespecialidad
where fecha between '".$fechaini."' 
and '".$fechafin."' and( idtipoagenda = 1 or idtipoagenda =3 or idtipoagenda=5)
and (agenda.fecha-agenda.fechacreado) <=15 and especialidadmedica.idespecialidad = tabla.idespecialidad) as total

 from agendamiento.especialidadmedica as tabla order by especialidad")->queryAll();
          $sqlespe2 = Yii::app()->db->createCommand("select especialidad,
(select count(*)
from agendamiento.agenda 
join agendamiento.medico on medico.idmedico = agenda.idmedico 
join agendamiento.especialidadmedica on especialidadmedica.idespecialidad = medico.idespecialidad
where fecha between '".$fechaini."' 
and '".$fechafin."' and( idtipoagenda = 1 or idtipoagenda =3 or idtipoagenda=5)
and (agenda.fecha-agenda.fechacreado) >15 and especialidadmedica.idespecialidad = tabla.idespecialidad) as total

 from agendamiento.especialidadmedica as tabla order by especialidad")->queryAll();
         $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
         $mPDF1->useOnlyCoreFonts = true;
         $mPDF1->SetTitle("Reporte");
         $mPDF1->SetAuthor("Hospital");
         //$mPDF1->SetWatermarkText("Pacientes");
         $mPDF1->showWatermarkText = true;
         $mPDF1->watermark_font = 'DejaVuSansCondensed';
         $mPDF1->watermarkTextAlpha = 0.1;
         $mPDF1->SetDisplayMode('fullpage');
         $mPDF1->WriteHTML($this->renderPartial('Reportepdfgrp', array('mes'=>$mes,'anio'=>$anio,'sqlespe'=>$sqlespe,'sqlespe2'=>$sqlespe2), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
         $mPDF1->Output('ReporteIndicadoresGRP'.date('YmdHis'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;
     }
     
     public function actionReporteindicadoresgprExcel(){
         if(isset($_GET['anio']) and isset($_GET['mes'])):
$anio = $_GET['anio'];
$mes = $_GET['mes'];

else:
 $mes = date('m');
 $anio = date('Y');

endif;

 $fechaini = "$anio-$mes-01";

$fechafin = "$anio-$mes-".date('t', strtotime($fechaini));
        
          $sqlespe = Yii::app()->db->createCommand("select especialidad,
(select count(*)
from agendamiento.agenda 
join agendamiento.medico on medico.idmedico = agenda.idmedico 
join agendamiento.especialidadmedica on especialidadmedica.idespecialidad = medico.idespecialidad
where fecha between '".$fechaini."' 
and '".$fechafin."' and( idtipoagenda = 1 or idtipoagenda =3 or idtipoagenda=5)
and (agenda.fecha-agenda.fechacreado) <=15 and especialidadmedica.idespecialidad = tabla.idespecialidad) as total

 from agendamiento.especialidadmedica as tabla order by especialidad")->queryAll();
          $sqlespe2 = Yii::app()->db->createCommand("select especialidad,
(select count(*)
from agendamiento.agenda 
join agendamiento.medico on medico.idmedico = agenda.idmedico 
join agendamiento.especialidadmedica on especialidadmedica.idespecialidad = medico.idespecialidad
where fecha between '".$fechaini."' 
and '".$fechafin."' and( idtipoagenda = 1 or idtipoagenda =3 or idtipoagenda=5)
and (agenda.fecha-agenda.fechacreado) >15 and especialidadmedica.idespecialidad = tabla.idespecialidad) as total

 from agendamiento.especialidadmedica as tabla order by especialidad")->queryAll();
        
         
         $contenido = $this->renderPartial("Reportepdfgrp_excel", array('mes'=>$mes,'anio'=>$anio,'sqlespe'=>$sqlespe,'sqlespe2'=>$sqlespe2), true);
          Yii::app()->request->sendFile('Reporte_gpr'.date('YmdHis').'.xls', $contenido);
          exit;
     }
     
     public function actionIndicadoresGPR(){
         
if(isset($_GET['anio']) and isset($_GET['mes'])):
$anio = $_GET['anio'];
$mes = $_GET['mes'];

else:
 $mes = date('m');
 $anio = date('Y');

endif;

 $fechaini = "$anio-$mes-01";

$fechafin = "$anio-$mes-".date('t', strtotime($fechaini));
        
          $sqlespe = Yii::app()->db->createCommand("select especialidad,
(select count(*)
from agendamiento.agenda 
join agendamiento.medico on medico.idmedico = agenda.idmedico 
join agendamiento.especialidadmedica on especialidadmedica.idespecialidad = medico.idespecialidad
where fecha between '".$fechaini."' 
and '".$fechafin."' and( idtipoagenda = 1 or idtipoagenda =3 or idtipoagenda=5)
and (agenda.fecha-agenda.fechacreado) <=15 and especialidadmedica.idespecialidad = tabla.idespecialidad) as total

 from agendamiento.especialidadmedica as tabla order by especialidad")->queryAll();
          $sqlespe2 = Yii::app()->db->createCommand("select especialidad,
(select count(*)
from agendamiento.agenda 
join agendamiento.medico on medico.idmedico = agenda.idmedico 
join agendamiento.especialidadmedica on especialidadmedica.idespecialidad = medico.idespecialidad
where fecha between '".$fechaini."' 
and '".$fechafin."' and( idtipoagenda = 1 or idtipoagenda =3 or idtipoagenda=5)
and (agenda.fecha-agenda.fechacreado) >15 and especialidadmedica.idespecialidad = tabla.idespecialidad) as total

 from agendamiento.especialidadmedica as tabla order by especialidad")->queryAll();
         $this->render('reportegpr',array('mes'=>$mes,'anio'=>$anio,'sqlespe'=>$sqlespe,'sqlespe2'=>$sqlespe2));
     }
}   
