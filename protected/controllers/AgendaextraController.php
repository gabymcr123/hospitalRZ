<?php

class AgendaextraController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('Autocompletar'),
'users'=>array('*'),
),

array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete','create','update','index','view'),
'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete','create','update','index','view'),
'users'=>CHtml::listData(Usuario::model()->findAll("idrol=2"),"usuario","usuario"),
),   
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Agendaextra;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Agendaextra']))
{
    $usu = Yii::app()->user->id;
    $model->responsable=$usu;
    $date = date_format(date_create(), 'Y-m-d H:i:s');
    $model->fechacreado = $date;
    $model->fechamodificado = $date;
    $model->creadopor = $usu;
    $model->modificadopor = $usu;
$model->attributes=$_POST['Agendaextra'];
if($model->save())
$this->redirect(array('view','id'=>$model->idextra));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Agendaextra']))
{
$model->attributes=$_POST['Agendaextra'];
$usu = Yii::app()->user->name;
$date = date_format(date_create(), 'Y-m-d H:i:s');
$model->fechamodificado = $date;
$model->modificadopor = $usu;
if($model->save())
$this->redirect(array('view','id'=>$model->idextra));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Agendaextra');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Agendaextra('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Agendaextra']))
$model->attributes=$_GET['Agendaextra'];

$this->render('admin',array(
'model'=>$model,
));
}


public function actionAutocompletar() {
        $criteria = new CDbCriteria;
	$criteria->compare('LOWER(nombre)', strtolower($_GET['term']), true, 'OR');
        $criteria->compare('LOWER(apellido)', strtolower($_GET['term']), true,'OR');
 	$criteria->order = 'nombre';
 	$criteria->limit = 30; 
        
 	$data = Medico::model()->findAll($criteria);
 	if (!empty($data))
 	{
  	$arr = array();
  	foreach ($data as $item) 
  	{
   		$arr[] = array(
                'id' => $item->idmedico,
                'value' => $item->nombre ." ". $item->apellido ,
   		);
    }
 	}
 	else
 	{
  		$arr = array();
  		$arr[] = array(
   		'id' => '',
   		'value' => 'Sin resultado',
                'telefono' => '',
  		);
 	}
 	echo CJSON::encode($arr);
        Yii::app()->end();
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Agendaextra::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}


/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='agendaextra-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}


}
