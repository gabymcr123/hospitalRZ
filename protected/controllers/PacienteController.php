<?php

class PacienteController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view','admin','Autocompletar'),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('delete',),
'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Paciente;

// Uncomment the following line if AJAX validation is needed
$this->performAjaxValidation($model);

if(isset($_POST['Paciente']))
{
$model->attributes=$_POST['Paciente'];
$usu = Yii::app()->user->name;
$date = date_format(date_create(), 'Y-m-d H:i:s');
$model->fechacreado = $date;
$model->fechamodificado = $date;
$model->creadopor = $usu;
$model->modificadopor = $usu;

if($model->save())
$this->redirect(array('view','id'=>$model->idpaciente));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
$this->performAjaxValidation($model);

if(isset($_POST['Paciente']))
{
$model->attributes=$_POST['Paciente'];
$usu = Yii::app()->user->name;
$date = date_format(date_create(), 'Y-m-d H:i:s');
$model->fechamodificado = $date;
$model->modificadopor = $usu;
if($model->save())
$this->redirect(array('view','id'=>$model->idpaciente));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
    if(Yii::app()->request->isPostRequest)
                {
                // we only allow deletion via POST request
                $sql = 'update agendamiento.paciente set estado=false where idpaciente='.$id;
                Yii::app()->db->createCommand($sql)->execute();
                //$this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

                 if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                 }
                 else
                         throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

}

/**
* Lists all models.
*/
public function actionIndex()
{
$model =new Paciente;
$dataProvider=$model->search();
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Paciente('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Paciente']))
$model->attributes=$_GET['Paciente'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Paciente::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='paciente-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}




public function actionAutocompletar() {
        $criteria = new CDbCriteria;
	$criteria->compare('LOWER(nombre)', strtolower($_GET['term']), true, 'OR');
        $criteria->compare('LOWER(apellido)', strtolower($_GET['term']), true,'OR');
 	$criteria->order = 'nombre';
 	$criteria->limit = 30; 
        
 	$data = Paciente::model()->findAll($criteria);
 	if (!empty($data))
 	{
  	$arr = array();
  	foreach ($data as $item) 
  	{
   		$arr[] = array(
    	'id' => $item->idpaciente,
    	'value' => $item->nombre ." ". $item->apellido ,
   		);
    }
 	}
 	else
 	{
  		$arr = array();
  		$arr[] = array(
   		'id' => '',
   		'value' => 'No se han encontrado resultados para su búsqueda',
  		);
 	}
 	echo CJSON::encode($arr);
        Yii::app()->end();
}
}
