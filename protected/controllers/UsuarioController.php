<?php

class UsuarioController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('update_1','admin_agendador','cambiolimite'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('create','update','admin','delete'),
'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Usuario;
// Uncomment the following line if AJAX validation is needed
 $this->performAjaxValidation($model);

if(isset($_POST['Usuario']))
{
$model->attributes=$_POST['Usuario'];

if($model->save())
    $this->redirect(array('view','id'=>$model->idusuario));

}
$date = date_format(date_create(), 'Y-m-d H:i:s');
$usu = Yii::app()->user->name;
$model->fechacreado = $date;
$model->fechamodificado = $date;
$model->creadopor = $usu;
$model ->modificadopor = $usu;
$model->estado = true;

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
Yii::app()->db->createCommand("delete from agendamiento.turnotemporal where usuario ='".Yii::app()->user->name."';")->execute();
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
$this->performAjaxValidation($model);

if(isset($_POST['Usuario']))
{
$model->attributes=$_POST['Usuario'];
if($model->save())
$this->redirect(array('view','id'=>$model->idusuario));
}

$this->render('update',array(
'model'=>$model,
));
}

public function actionUpdate_1($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
$this->performAjaxValidation($model);

if(isset($_POST['Usuario']))
{
$model->attributes=$_POST['Usuario'];
if($model->save())
$this->redirect(array('view','id'=>$model->idusuario));
}

$this->render('update_1',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($pk)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request

    
    $value = Yii::app()->db->createCommand('select estado from agendamiento.usuario where idusuario='.$pk)->queryScalar();
    if($value=='true'){
        $sql = 'update agendamiento.usuario set estado = false where idusuario='.$pk;
    }else{
        $sql = 'update agendamiento.usuario set estado = true where idusuario='.$pk;
    }
    
    Yii::app()->db->createCommand($sql)->execute();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Usuario');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Usuario('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Usuario']))
$model->attributes=$_GET['Usuario'];

$this->render('admin',array(
'model'=>$model,
));
}

public function actionAdmin_agendador()
{
$model=new Usuario('searchAgendador');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Usuario']))
$model->attributes=$_GET['Usuario'];

$this->render('admin_agendador',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Usuario::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}
 
/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='usuario-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

public function actionCambiolimite($valor, $agenda){
    
    if($valor!=null or $valor !=''){
        if ($agenda =='si') {
            $count= Yii::app()->db->createCommand("update agendamiento.datosgenerales set limiteagenda ='".$valor."';")->execute();
        }else if ($agenda =='no'){
            $count= Yii::app()->db->createCommand("update agendamiento.datosgenerales set limiteagendadoctor ='".$valor."';")->execute();
        } else if ($agenda =='ref'){
            $count= Yii::app()->db->createCommand("update agendamiento.datosgenerales set limitereferencia ='".$valor."';")->execute();
        }
        if($count>0){
            print $valor;
        }else{
            print 'no';
        }
    }
    else{
        print 'no';
    }
}
}
