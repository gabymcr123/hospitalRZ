<?php

class DiaController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update'),
'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Dia;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Dia']))
{
$model->attributes=$_POST['Dia'];
if($model->save())
$this->redirect(array('view','id'=>$model->iddia));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($idmedico)
{
    
$iddia = Yii::app()->db->createCommand('select iddia from agendamiento.dia where idmedico = '.$idmedico.' order by iddia')->queryColumn();
$model_1 = Dia::model()->findByPk($iddia[0]);
$model2 = Dia::model()->findByPk($iddia[1]);
$model3 = Dia::model()->findByPk($iddia[2]);
$model4 = Dia::model()->findByPk($iddia[3]);
$model5 = Dia::model()->findByPk($iddia[4]);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

    if(!empty($_POST))
{
   
$model_1->attributes=$_POST['Dia'][1];
$model2->attributes=$_POST['Dia'][2];
$model3->attributes=$_POST['Dia'][3];
$model4->attributes=$_POST['Dia'][4];      
$model5->attributes=$_POST['Dia'][5];
$model_1->save();
$model2->save();
$model3->save();
$model4->save();
$model5->save();
Yii::app()->db->createCommand('select agendamiento.porcentajesturnos('.$model_1->idmedico.');')->execute();
                      $this->redirect(array('medico/view','id'=>$idmedico));
                  

}
$model_1->idmedico = $idmedico;
$this->render('update',array(
'model_1'=>$model_1,'model2'=>$model2,'model3'=>$model3,'model4'=>$model4,'model5'=>$model5,'idmedico'=>$idmedico
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Dia');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Dia('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Dia']))
$model->attributes=$_GET['Dia'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Dia::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='dia-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
