<?php

class NuevoporcentajesController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Nuevoporcentajes;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Nuevoporcentajes']))
{
    $espe = $_POST['listespecialidad'];
    $gene= $_POST['vgeneral'];
$model->attributes=$_POST['Nuevoporcentajes'];
$model->fechacreado = date('Y-m-d');
$model->creadopor = Yii::app()->user->name;
if($espe=="" && $gene==""){
    if($model->save())
        $this->redirect(array('view','id'=>$model->idnuevo));
        //$this->redirect(array('admin','model'=>new Nuevoporcentajes));
}else {
    if($gene==""){
    $resul =Yii::app()->db->createcommand('select idmedico from agendamiento.medico where estado = true and idespecialidad ='.$espe)->queryColumn();
    }else {
    $resul =Yii::app()->db->createcommand('select idmedico from agendamiento.medico where estado = true')->queryColumn();    
    }
    foreach ($resul as $key => $value){
       $model->idmedico = $value;
       $modelnuevo = $model;
       $modelnuevo->save(); 
    }
    $data = new Nuevoporcentajes;
    $this->redirect('admin',array('model'=>$data));
}
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Nuevoporcentajes']))
{
$model->attributes=$_POST['Nuevoporcentajes'];
if($model->save())
$this->redirect(array('view','id'=>$model->idnuevo));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
//$dataProvider=new CActiveDataProvider('Nuevoporcentajes');
$dataProvider = new Nuevoporcentajes;
$this->render('admin',array(
'model'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Nuevoporcentajes('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Nuevoporcentajes']))
$model->attributes=$_GET['Nuevoporcentajes'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Nuevoporcentajes::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='nuevoporcentajes-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
