<?php

class MedicoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','obtenerintervalo','obtenerespecialidad','prueba'),
				'users'=>array('*'),
			
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update'),
				'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),            
			
			),
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','create','update'),
				'users'=>CHtml::listData(Usuario::model()->findAll("idrol=2"),"usuario","usuario"),            
			
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                                        $modelc = new Usuario;
                                        $model=new Medico;
                                   

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);
                                        
		if(isset($_POST['Medico']))
		{
                        
		$model->attributes=$_POST['Medico'];
                
                                     //   $modelDi->attributes=$_POST['Dia'];
                                        $date = date_format(date_create(), 'Y-m-d H:i:s');
                                        $usu = Yii::app()->user->name;
                                        $model->fechacreado = $date;
                                        $model->fechamodificado = $date;
                                        $model ->modificadopor = $usu;
                                        $model->creadopor = $usu;
                                        $model->estado = true;
                                        $modelc->fechacreado = $date;
                                        $modelc->fechamodificado = $date;
                                        $modelc->creadopor = $usu;
                                        $modelc ->modificadopor = $usu;
                                        $modelc->estado = true;
                                        $modelc->idrol = 2;
                                        $modelc->attributes=$_POST['Usuario'];
                                        
                                        if($modelc->save()){
                                        //$model->idusuario=Yii::app()->db->createCommand("select idusuario from agendamiento.usuario where usuario ='".$modelc->usuario."';")->queryScalar();
                                        $model->idusuario =$modelc->idusuario; 
                                        
                                        $model->nombre = $modelc->nombre;
                                        $model->apellido = $modelc->apellido;
                                        if($model->save()){
                                        //$modelDi->idmedico =Yii::app()->db->createCommand("select idusuario from agendamiento.usuario where usuario ='".$modelc->usuario."';")->queryScalar();
                                                        
                                    $this->redirect(array('dia/update','idmedico'=>$model->idmedico));
                
                }}
		}
                                      //  $modelDi->intervaloconsulta = 15;   
                
		$this->render('create',array(
			'model'=>$model,'modelc'=>$modelc
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $iddi = Yii::app()->db->createCommand("select iddia from agendamiento.dia where idmedico =".$id)->queryScalar();
                  $modelDi=  Dia::model()->findByPk($iddi);
                     $idu = Yii::app()->db->createCommand("select idusuario from agendamiento.medico where idmedico =".$id)->queryScalar();
                   $modelU = Usuario::model()->findByPk($idu);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Medico']))
		{
                                        $date = date_format(date_create(), 'Y-m-d H:i:s');
                                        $usu = Yii::app()->user->name;
		$model->attributes=$_POST['Medico'];
                                       // $modelDi->attributes=$_POST['Dia'];
                                        $modelU->attributes=$_POST['Usuario'];
                                        $model->fechamodificado = $date;
                                        $model ->modificadopor = $usu;
                                        $modelU->fechamodificado = $date;
                                        $modelU ->modificadopor = $usu;
                                        if($modelU->save()){
                                            	if($model->save()){
                                                    $modelDi->fechamodificado = $date;
                                    //    $modelDi ->modificadopor = $usu;
                                            //        if($modelDi->save()){
                                                Yii::app()->db->createCommand('select agendamiento.porcentajesturnos('.$model->idmedico.');')->execute();
                                                $this->redirect(array('view','id'=>$model->idmedico));
                                                  //  }
                                                }
                                        }
		
		}
                $modelU->repeat_password = $modelU->password;
		$this->render('update',array(
			'model'=>$model,'modelDi'=>$modelDi,'modelU'=>$modelU
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($pk)
{
            if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
                $value = Yii::app()->db->createCommand('select estado from agendamiento.medico where idmedico='.$pk)->queryScalar();
                $idusu= Yii::app()->db->createCommand('select idusuario from agendamiento.medico where idmedico ='.$pk)->queryScalar();
                if($value=='true'){
                    $sql1 = 'update agendamiento.usuario set estado = False where idusuario='.$idusu;
                    $sql = 'update agendamiento.medico set estado = False where idmedico='.$pk;
                    $sql2 = 'update agendamiento.dia set estado = False where idmedico='.$pk;
                }
                else{
                    $sql1 = 'update agendamiento.usuario set estado = true where idusuario='.$idusu;
                    $sql = 'update agendamiento.medico set estado = true where idmedico='.$pk;
                    $sql2 = 'update agendamiento.dia set estado = true where idmedico='.$pk;
                }
                Yii::app()->db->createCommand($sql1)->execute();
                Yii::app()->db->createCommand($sql)->execute();
                Yii::app()->db->createCommand($sql2)->execute();
                
		//$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
	
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                                        $model = new Medico;
                                        
		$dataProvider= $model->search();
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Medico('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Medico']))
			$model->attributes=$_GET['Medico'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Medico the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Medico::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Medico $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='medico-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
         public function actionObtenerintervalo(){
             $opcion= $_GET['idespecialidad'];
                $resp = Especialidadmedica::model()->find('idespecialidad='.$opcion);
                
                echo($resp->intervalo);
        }
        public function actionObtenerespecialidad($opcion){
            $idespecialidad =4;
            $resp = Especialidadmedica::model()->findAllByAttributes(array('idserviciomedico'=>$opcion,'estado'=>true));
            header("Content-type: application/json");
            echo CJSON::encode($resp);
        }
        
        
        public function actionPrueba() {
        $sql = "update agendamiento.agenda set atendido = true";
        Yii::app()->db->createCommand($sql)->execute();
}
}
