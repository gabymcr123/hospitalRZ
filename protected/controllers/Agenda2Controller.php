<?php

class Agenda2Controller extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('admin','create','update','atender','calendario'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('delete'),
'users'=>CHtml::listData(Usuario::model()->findAll("idrol=3"),"usuario","usuario"),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{   
    $model=new Agenda;
            $this->performAjaxValidation($model);
            
                if(isset($_POST['referencia']))
                    $ref = $_POST['referencia'];
                
		
		// Uncomment the following line if AJAX validation is needed
		if(isset($_POST['Agenda']))
		{       
                //$centro = $_POST['subcentro'];
                $date = date_format(date_create(), 'Y-m-d H:i:s');
                $model->attributes=$_POST['Agenda'];
                if(!isset($model->numeroturno)|| $model->numeroturno==""){$model->numeroturno=1;}
                $val = Yii::app()->db->createCommand("select inicio, fin from agendamiento.horapaciente(".$model->idmedico.",'".$model->fecha."', ".$model->numeroturno.");")->queryRow();
                $model->horainicio = $val['inicio'];
                if($model->idtipoagenda == '5' or $model->idtipoagenda == '6'){
                    if(isset($ref)){
                    $agenda = $this->loadModel($ref);
                    $agenda->save();
                    $model->idreferencia = $ref;
                    }
                    $model->reagendado = 1;
                    
                }else if($model->idtipoagenda == '3' or $model->idtipoagenda == '4'){
                    $model->numeroturno = 0;
                    $model->horainicio = Yii::app()->db->createCommand('select horafin from agendamiento.dia where idmedico='.$model->idmedico.';')->queryScalar();
                }
                $model->fechacreado = $date;
                $model->fechamodificado = $date;
                $model->creadopor = Yii::app()->user->name;
                $model->horafin = $val['fin'];
                $diasdesemanas = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'); 
                $mesanio = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'); 
                $arrfecha = explode('-',  $model->fecha);
                $html ='';
		if($model->save()):
                    $model->numeroturno = $model->numeroturno==0 ? 'Sin Numero de turno (Extra)': $model->numeroturno;
                    $html.='<h4 style="color:green;">Agenda creada correctamente</h4>';
                    $html.= '<label>DETALLE DE CITA:</label><table class="table detail-view">';
                    $html.= '<tr><th>Historial</th><td>'.$model->pkpaciente->historialclinico.'</td></tr>';
                    $html.= '<tr><th>Paciente</th><td>'.$model->pkpaciente->apellido.' '.$model->pkpaciente->nombre.'</td></tr>';
                    //$html.= '<tr><th>Profesional de la salud</th><td>'.$model->pkmedico->Fullname.'</td></tr>';
                    $html.= '<tr><th>Tipo</th><td>'.$model->pktipoagenda->tipoagenda.'</td></tr>';
                    $html.= '<tr><th>Fecha Atención</th><td>'.$diasdesemanas[date('N', strtotime($model->fecha))].', '.$arrfecha[2].'-'.$mesanio[(int)$arrfecha[1]].'-'.$arrfecha[0].'</td></tr>';
                    $html.= '<tr><th>Hora de Atención</th><td>'.$model->horainicio.'</td></tr>';
                    $html.= '<tr><th>Numero de Turno</th><td>'.$model->numeroturno.'</td></tr>';
                    $html.= '<tr><th>Teléfono</th><td>'.$model->telefono.'</td></tr>';
                    $html.= '<tr><th>Celular</th><td>'.$model->celular.'</td></tr>';
                    $html.= '</table>';
                    print $html;
                endif;
		}

//            $this->render('create',array(
//            'model'=>$model
//            ));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Agenda2']))
{
$model->attributes=$_POST['Agenda2'];
if($model->save())
$this->redirect(array('agenda2/admin'));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Agenda2');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
    if(ISSET($_GET['f'])):
        $f = $_GET['f'];
    else:
        $f = date_format(date_create(), 'Y-m-d');
    endif;
    
    $modelo=new Agenda();
    $model=new Agenda2();
    $model->unsetAttributes();  // clear any default values
    if(isset($_GET['Agenda2']))
    $model->attributes=$_GET['Agenda2'];

    $this->render('admin',array(
    'model'=>$model,'modelo'=>$modelo,'f'=>$f
    ));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Agenda2::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='agenda2-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
public function actionAtender($pk){
    $re = Yii::app()->db->createCommand('select atendido from agendamiento.agenda where idagenda ='.$pk)->queryScalar();
    if($re){
        Yii::app()->db->createCommand('update agendamiento.agenda set atendido=false where idagenda ='.$pk)->execute();
    }else{
        Yii::app()->db->createCommand('update agendamiento.agenda set atendido=true where idagenda ='.$pk)->execute();
    }
}
public function actionCalendario($mes,$anio,$accion,$especialidad,$doctor){
                $info=(isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 4) ? true : false ;
                $isadmin = Yii::app()->db->createCommand('select idrol from agendamiento.usuario where idusuario ='.Yii::app()->user->Id)->queryScalar()=='3' ? true:false;
                $doc = false;
                $esp = false;
                $lun = false; $mar = false; $mie = false; $jue = false; $vie = false;
                $doctor = $_GET['doctor'];
                $especialidad = $_GET['especialidad'];
                if($doctor!=""){
                    /*dias de la semana si trabaja o no*/
                    $doc=true;
                    $dia = Yii::app()->db->createCommand('select estado from agendamiento.dia where idmedico ='.$doctor.' order by iddia;')->queryColumn();
                    $lun = $dia[0];
                    $mar= $dia[1];
                    $mie= $dia[2];
                    $jue= $dia[3];
                    $vie = $dia[4];
                }
                $fecha_calendario=array();
                if ($_GET["mes"]=="" || $_GET["anio"]=="")
                {
                        $fecha_calendario[1]=intval(date("m"));
                        if ($fecha_calendario[1]<10) $fecha_calendario[1]="0".$fecha_calendario[1];
                        $fecha_calendario[0]=date("Y");
                }
                else
                {
                        $fecha_calendario[1]=intval($_GET["mes"]);
                        if ($fecha_calendario[1]<10) $fecha_calendario[1]="0".$fecha_calendario[1];
                        else $fecha_calendario[1]=$fecha_calendario[1];
                        $fecha_calendario[0]=$_GET["anio"];
                }
                $fecha_calendario[2]="01";

                /* obtenemos el dia de la semana del 1 del mes actual */
                $primeromes=date("N",mktime(0,0,0,$fecha_calendario[1],1,$fecha_calendario[0]));

                /* comprobamos si el a�o es bisiesto y creamos array de d�as */
                if (($fecha_calendario[0] % 4 == 0) && (($fecha_calendario[0] % 100 != 0) || ($fecha_calendario[0] % 400 == 0))) 
                    $dias=array("","31","29","31","30","31","30","31","31","30","31","30","31");
                else $dias=array("","31","28","31","30","31","30","31","31","30","31","30","31");

                //CONSULTANDO EVENTOS
                $eventos= array();
                if($doc){
                $sql = "select * from agendamiento.fechaeventomedico(".$doctor.",".$fecha_calendario[1].",".$fecha_calendario[0].");";
                $query = Yii::app()->db->createCommand($sql)->query();
                foreach($query as $fila) {
					$ini = $fila['fechainiciop'];
					$fin = $fila['fechafinp'];
                                        $eventos[$ini] = $fin;
					while($ini < $fin){
						$nuevafecha = strtotime ( '+1 day' , strtotime ( $ini ) ) ;
						$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
						$ini = $nuevafecha;
						$eventos[$ini] = $fin;
					}		
                }
                }
                //FIN DE CONSULTA EVENTO

                $meses=array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

                /* calculamos los días de la semana anterior al día 1 del mes en curso */
                $diasantes=$primeromes-1;

                /* los días totales de la tabla siempre serán máximo 42 (7 días x 6 filas máximo) */
                $diasdespues=42;

                /* calculamos las filas de la tabla */
                $tope=$dias[intval($fecha_calendario[1])]+$diasantes;
                if ($tope%7!=0) $totalfilas=intval(($tope/7)+1);
                else $totalfilas=intval(($tope/7));

                /* empezamos a pintar la tabla */
                echo "<h2 id='titulocalendario' rel = '".$fecha_calendario[0]."-".$fecha_calendario[1]."' >Calendario:  ".$meses[intval($fecha_calendario[1])]." del ".$fecha_calendario[0]." <abbr title='S&oacute;lo se pueden agregar eventos en d&iacute;as h&aacute;biles y en fechas futuras (o la fecha actual).'></abbr></h2>";
                if (isset($mostrar)) echo $mostrar;

                echo "<table class='calendario' cellspacing='0' cellpadding='0'>";
                        echo "<tr><th>Lunes</th><th>Martes</th><th>Mi&eacute;rcoles</th><th>Jueves</th><th>Viernes</th><th>S&aacute;bado</th><th>Domingo</th></tr><tr>";
                        /* inicializamos filas de la tabla */
                        $tr=0;
                        $dia=1;

                        for ($i=1;$i<=$diasdespues;$i++)
                        {
                                if ($tr < $totalfilas)
                                {
                                        if ($i>=$primeromes && $i<=$tope)
                                        {       $ban=false;
                                                echo "<td class='";
                                                // creamos fecha completa 
                                                if ($dia<10) $dia_actual="0".$dia; 
                                                else $dia_actual=$dia;
                                                
                                                $fecha_completa = $fecha_calendario[0]."-".$fecha_calendario[1]."-".$dia_actual;
                                                
                                                $diaSemana = date("l", strtotime($fecha_completa));
                                                if($lun and ($doc or $esp) and $diaSemana == 'Monday' ){
                                                    echo 'atencion ';
                                                    $ban =true;
                                                }
                                                else if($mar and ($doc or $esp) and $diaSemana == 'Tuesday'){
                                                    echo 'atencion ';
                                                    $ban =true;
                                                }else 
                                                if($mie and ($doc or $esp) and $diaSemana == 'Wednesday'){
                                                    $ban =true;
                                                    echo 'atencion ';
                                                }else 
                                                if($jue and ($doc or $esp) and $diaSemana == 'Thursday'){
                                                    echo 'atencion ';
                                                    $ban =true;
                                                }else 
                                                if($vie and ($doc or $esp) and $diaSemana == 'Friday'){
                                                    echo 'atencion ';
                                                    $ban =true;
                                                }
                                                $cortamos=explode("-",$fecha_completa);
                                                $pdia=$cortamos[2];
                                                $pmes=$cortamos[1];
                                                $pano=$cortamos[0];
                                                $fue=date("w",mktime(0,0,0,$pmes,$pdia,$pano));
                                                $resul = null;
                                                if (intval($fue)==0 || intval($fue)==6):
                                                    $resul = true;
                                                else:
                                                    $resul = false;
                                                endif;
                                                if(!$resul):
                                                $classadd = '';
                                                $datosdia= Yii::app()->db->createCommand("select * from agendamiento.agendallenadatos(".$doctor.",'".$fecha_completa."');")->queryRow();
                                                    if($datosdia['sub']<=0):
                                                        $classadd = 'eventoespe ';
                                                    endif;
                                                        print $classadd.' ';
                                                endif;
                                                
                                                //EVENTOS GENERALES
                                                if (isset($eventos[$fecha_completa])) {
                                                if (intval($eventos[$fecha_completa])>0)
                                                {       
                                                    if($esp){
                                                        $sql = "select * from agendamiento.elegirmedico(".$especialidad.",'".$fecha_completa."');";
                                                        $id = Yii::app()->db->createCommand($sql)->queryScalar();
                                                        if(isset($id)){
                                                            echo 'eventoespe ';
                                                            //$ban=false;
                                                        }
                                                        else {
                                                            echo 'evento ';
                                                            $ban=false;
                                                        }
                                                    }
                                                    else if($doc){ 
                                                        echo 'evento ';
                                                        $ban=false;
                                                        
                                                    }
                                                    $hayevento=$eventos[$fecha_completa];
                                                        
                                                }
                                                }
                                                else $hayevento=0;
                                                //FIN DE EVENTOS

                                                // si es hoy coloreamos la celda
                                                if (date("Y-m-d")==$fecha_completa) echo("hoy");
                                                echo("'>");
                                                if(!$resul){
                                                    print '<table class=""><tr><td style="font-size:11px;font-weight:normal;padding:0px;color:blue;">'.$datosdia['sub'].'</td></tr></table>';
                                                }
                                                // recorremos el array de eventos para mostrar los eventos del d�a de hoy 
                                                //if ($hayevento>0) echo "<a href='#' data-evento='#evento".$dia_actual."' class='modal' rel='".$fecha_completa."' title='Hay ".$hayevento." eventos'>".$dia."</a>";
                                                if ($hayevento>0) echo "<a href='#' data-evento='#evento".$dia_actual."' class='falta' rel='".$fecha_completa."' title='Hay ".$hayevento." eventos'><span class='dia_calendario'>$dia </span></a>";
                                                else echo("<span class='dia_calendario'>$dia </span>");

                                                // agregamos enlace a nuevo evento si la fecha no ha pasado 
                                                $timer = explode(" ",$fecha_completa);
                                                $fecha2 = explode("-",$timer[0]);
                                                $fechex = $fecha2[2]."/".$fecha2[1]."/".$fecha2[0];

                                                $cortamos=explode("-",$fecha_completa);
                                                $pdia=$cortamos[2];
                                                $pmes=$cortamos[1];
                                                $pano=$cortamos[0];
                                                $fue=date("w",mktime(0,0,0,$pmes,$pdia,$pano));
                                                $resul = null;
                                                if (intval($fue)==0 || intval($fue)==6)
                                                    $resul = true;
                                                else
                                                    $resul = false;
                                                //imprime los signo mas para gregar evento
                                                $userdoct= Yii::app()->db->createCommand('select idrol from agendamiento.usuario where idusuario='.Yii::app()->user->id)->queryScalar();
                                                if($userdoct != 2){
                                                    $fechalimite= Yii::app()->db->createCommand('select limiteagenda from agendamiento.datosgenerales;')->queryScalar();
                                                    $fechalimiteref= Yii::app()->db->createCommand('select limitereferencia from agendamiento.datosgenerales;')->queryScalar();
                                                }
                                                else{
                                                    $fechalimite= Yii::app()->db->createCommand('select limiteagendadoctor from agendamiento.datosgenerales;')->queryScalar();
                                                    $fechalimiteref= $fechalimite;
                                                }
                                                if(!$info){
                                                    if (((date("Y-m-d")<=$fecha_completa and $fecha_completa<=$fechalimite) or $isadmin) && $resul==false && $ban ) {
                                                        print '<input type="radio" name="Dia" value="'.$fecha_completa.'" class="fecha" onclick="selectfecha(this.value); " />';
                                                    }
                                                }
                                                echo "</td>";
                                                $dia+=1;
                                        }
                                        else echo "<td class='desactivada'>&nbsp;</td>";
                                        if ($i==7 || $i==14 || $i==21 || $i==28 || $i==35 || $i==42) {echo "<tr>";$tr+=1;}
                                }
                        }
                        echo "</table>";
                        
                        $mesanterior=date("Y-m-d",mktime(0,0,0,$fecha_calendario[1]-1,01,$fecha_calendario[0]));
                        $messiguiente=date("Y-m-d",mktime(0,0,0,$fecha_calendario[1]+1,01,$fecha_calendario[0]));
                        echo "<p class='toggle'>&laquo; <a href='#' rel='$mesanterior' class='anterior'>Mes Anterior</a> - <a href='#' class='siguiente' rel='$messiguiente'>Mes Siguiente</a> &raquo;</p>";
                        return;
        }
        
    public function actionTipoagenda(){
        
        $id = $_GET['id'];/*id tipo de agenda*/
        $medico = $_GET['idme'];
        $fecha = $_GET['fecha'];
        if($id>2 && $id <5){
        $row = Yii::app()->db->createCommand("select * from agendamiento.obtenerinifinextra(".$id.",".$medico.",'".$fecha."')")->queryRow();
        $ini = $row['ini'];
        $fin = $row['fin'];
            if(!isset($fin))
                $fin = 0;
            }else{
            if($id==5)$id=1;
            $row = Yii::app()->db->createCommand("select * from agendamiento.obtenerinifin(".$id.",".$medico.",'".$fecha."')")->queryRow(); 
            $ini = $row['ini'];
            $fin = $row['fin'];  
            echo $ini.'_'.$fin;
    }
    }
}
